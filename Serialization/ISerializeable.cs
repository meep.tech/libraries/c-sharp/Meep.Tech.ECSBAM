﻿using Newtonsoft.Json.Linq;
using Meep.Tech.Data.Serialization;
using System.Collections.Generic;
using System;
using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;

namespace Meep.Tech.Data.Serialization {

  /// <summary>
  /// A serializeable data item:
  /// </summary>
  public interface ISerializeable {

    /// <summary>
    /// Cached model initializers for models without types.
    /// </summary>
    internal static Dictionary<string, Func<Model.Params, Serializer.SerializedData?, ISerializeable>> _serializeableInitializersForModelsWithoutArchetypes
      = new Dictionary<string, Func<Model.Params, Serializer.SerializedData?, ISerializeable>>();

    /// <summary>
    /// Default way to make a model given only the serialized data and nothing else.
    /// </summary>
    public static ISerializeable Make(Serializer.SerializedData? serializedData, Model.Params @params = null, System.Type modelType = null) {
      if (serializedData != null && serializedData.Value.sqlData.TryGetValue(Serializer.SQLTypeColumnName, out object archetypeExternalId)) {
        ISerializeable childModel = Static.Archetypes.Ids[archetypeExternalId as string].Archetype.MakeI();
        return childModel.deserialize(serializedData.Value);
      } else {
        string tableName = serializedData?.tableName ?? Serializer.Cache.GetDataModelInfo(modelType ?? throw new Exception($"Could not find the model class or table name for an attempted deserialization"))?.TableName;
        Type baseModelType = modelType ?? Type.GetType(Serializer.Cache.GetBasicModelTypeForTable(tableName));

        /// if it's cached already:
        if (_serializeableInitializersForModelsWithoutArchetypes.TryGetValue(baseModelType?.FullName ?? "", out Func<Model.Params, Serializer.SerializedData?, ISerializeable> existingCtor)) {
          return existingCtor.Invoke(@params, serializedData);
        }

        /// else cache the default ctor:
        System.Reflection.ConstructorInfo providedCtor = baseModelType?.GetConstructor(
          System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance,
          null,
          new Type[] { typeof(Model.Params), typeof(Serializer.SerializedData) },
          null
        );
        if (providedCtor == null) {
          throw new NotImplementedException($"Failure trying to deserialize data for table: {tableName ?? "NO-TABLE-ERROR"}" +
            $", onto model of type: {baseModelType?.ToString() ?? "NO-MODEL-TYPE-ERROR"}.\n" +
            $"The serialized model does not have a saved Archetype external id to build it with using Make(), and also lacks a backup constructor that takes a 'Model.Params' and a 'Serializer.SerializedData?' as the only arguments.\n" +
            $"\n" +
            $"Try to make sure the model is saved with an id for an archetype that knows how to initialize the model you'd like, or add a construtor that takes a Model.Params object and a Nullable Serializer.SerializedData struct as arguments to the model's class.\n" +
            $"This default ctor should work under the assumption that only one of the given values will not be null, but it could be either."
          );
        }
        _serializeableInitializersForModelsWithoutArchetypes.Add(
          baseModelType.FullName,
          (@params, serializedData) => providedCtor.Invoke(new object[] { @params, serializedData }) as ISerializeable
        );

        return _serializeableInitializersForModelsWithoutArchetypes[baseModelType.FullName].Invoke(@params, serializedData);
      }
    }

    /// <summary>
    /// Convert this model's data to a serialized state.
    /// </summary>
    virtual Serializer.SerializedData serialize(bool andAllChildrenRecursively = true, bool fullySerializeLinkedChildModels = true)
      => ISerializeableMethods.serialize(this, andAllChildrenRecursively, fullySerializeLinkedChildModels);

    /// <summary>
    /// Deserialize this model from the given data.
    /// </summary>
    virtual ISerializeable deserialize(
      Serializer.SerializedData serializedData,
      int? depthLimit = null
    ) => ISerializeableMethods.deserialize(this, serializedData, depthLimit);

    /// <summary>
    /// Get the table name used by the model type
    /// </summary>
    virtual string GetTableName()
      => ISerializeableMethods.GetTableName(this);

    /// <summary>
    /// Get the table name used by the model type
    /// </summary>
    virtual string toJson()
      => ISerializeableMethods.toJson(this);

    /// <summary>
    /// Get the table name used by the model type
    /// </summary>
    virtual JObject toJObject()
      => ISerializeableMethods.toJObject(this);

    /// <summary>
    /// Get the table name used by the model type
    /// </summary>
    virtual ISerializeable fromJson(string jsonString)
      => ISerializeableMethods.fromJson(this, jsonString);

    /// <summary>
    /// Deserialize this model from the given data.
    /// </summary>
    ISerializeable deserialize(Serializer.SerializedData serializedData)
      => this.deserialize(serializedData, null);

    /// <summary>
    /// (optional)Finish deserializing the model
    /// </summary>
    internal protected virtual void finishDeserialization() { }
  }
}

public static class ISerializeableMethods {

  /// <summary>
  /// Convert this model's data to a serialized state.
  /// </summary>
  public static Serializer.SerializedData serialize(
    this ISerializeable model,
    bool andAllChildrenRecursively = true,
    bool fullySerializeLinkedModels = true
  ) => Serializer.Serialize(
    model,
    !andAllChildrenRecursively,
    fullySerializeLinkedModels
  );

  /// <summary>
  /// Convert this model's data to a serialized state.
  /// </summary>
  public static string toJson(this ISerializeable model) 
    => model.serialize().toJson();

  /// <summary>
  /// Convert this model's data to a serialized state.
  /// </summary>
  public static JObject toJObject(this ISerializeable model)
    => model.serialize().toJObject();

  /// <summary>
  /// Convert this model's data to a serialized state.
  /// </summary>
  public static ISerializeable fromJson(this ISerializeable model, string jsonString) {
    Serializer.SerializedData data = Serializer.SerializedData.FromJson(jsonString);
    return model.deserialize(data, 1);
  }

  /// <summary>
  /// Deserialize this model from the given data.
  /// </summary>
  public static ISerializeable deserialize(
    this ISerializeable model,
    Serializer.SerializedData serializedData,
    int? depthLimit = null
  ) {
    model = Serializer.Deserialize(
      model,
      serializedData,
      depthLimit
    );

    model.finishDeserialization();
    if (model.GetType().IsValueType/* && !model.Equals(model)*/) {
      model.finishDeserialization();
    }

    return model;
  }

  /// <summary>
  /// Get the table name used by the model type
  /// </summary>
  public static string GetTableName(this ISerializeable serializeable)
    => Serializer.Cache.GetDataModelInfo(serializeable).TableName;
}
