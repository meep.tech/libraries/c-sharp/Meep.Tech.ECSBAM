﻿namespace Meep.Tech.Data.Serialization {

  public partial class Serializer {

    /// <summary>
    /// Store serialized data by table name then by id key.
    /// Helper class.
    /// </summary>
    /*public sealed class SerializedDataCollection 
      : Dictionary<string, Dictionary<string, IEnumerable<SerializedData>>> 
    {

      /// Special Keys:
      ///<summary>
      /// Used to represent non unique models without an id 
      ///</summary>
      public const string NoIdDataKey = "__NOID";


      ///<summary>
      ///Used to delete entries in a table by parent model id
      ///</summary>
      public const string DeleteDataKey = "__DELETE";

      /// <summary>
      /// Placeholders for serializeable components which don't have tables:
      /// </summary>
      public const string ComponentTableNamePlaceholder = "__component";

      /// <summary>
      /// Make a new serialized data collection
      /// </summary>
      /// <param name="serializedDatas"></param>
      public SerializedDataCollection(IEnumerable<SerializedData> serializedDatas = null) {
        if (serializedDatas != null) {
          addSerializedModelsToCollectionByTableNameThenId(serializedDatas);
        }
      }

      /// <summary>
      /// Make a collection from a single item
      /// </summary>
      public static SerializedDataCollection FromSingleItem(string tableName, string itemId, SerializedData singleDataItem) {
        return new SerializedDataCollection() {
          { tableName, new Dictionary<string, IEnumerable<SerializedData>>() {
            { itemId,
              singleDataItem.AsEnumerable()
            }
          }}
        };
      }

      /// <summary>
      /// Convert this model's data to a serialized state.
      /// </summary>
      public string toJson() {
        List<string> results = new List<string>();
        foreach (string tableName in Keys) {
          foreach (string modelId in this[tableName].Keys) {
            results.AddRange(this[tableName][modelId].ForEach(data => data.toJson()));
          }
        }

        return new JArray(results).ToString();
      }

      /// <summary>
      /// Get models from a serialized string of data:
      /// </summary>
      public static SerializedDataCollection FromJson(string jsonString) {
        SerializedDataCollection results = new SerializedDataCollection();
        JArray items = new JArray(jsonString);
        foreach (JObject item in items) {
          results.addSerializedModelsToCollectionByTableNameThenId(
            SerializedData.FromJson(item.ToString()).AsEnumerable()
          );
        }

        return results;
      }

      /// <summary>
      /// Shortcut helper to set the name of the field this model was on in the parent model.
      /// </summary>
      public void updateSqlOwnerFieldNameFieldForFirstSerializedDataItemFor(string modelId, string tableName, string parentModelFieldName) {
        var baseModelData = this[tableName][modelId].First();
        baseModelData.sqlData[SQLOwnerModelFieldNameColumnName] = parentModelFieldName;
        overwriteFirstSerializedDataItemFor(modelId, baseModelData);
      }

      /// <summary>
      /// Shortcut helper to add a json field to the base/first serialized value of a model
      /// </summary>
      public void appendJsonFieldToFirstSerializedDataItemFor(string modelId, string tableName, string fieldName, JToken fieldValue) {
        var baseModelData = this[tableName][modelId].First();
        baseModelData.jsonData.Add(fieldName, fieldValue);
        overwriteFirstSerializedDataItemFor(modelId, baseModelData);
      }

      /// <summary>
      /// Try to get parent model data from a collection given the parent model table name and id
      /// </summary>
      public bool tryToGetParentData(string modelTableName, string modelUniqueId, out SerializedData parentModelData) {
        int count;

        if ((count = this[modelTableName].Count) == 1) {
          parentModelData = this[modelTableName].First().Value.First();
          return true;
        } else if (count > 1) {
          parentModelData = this
            [modelTableName]
            [modelUniqueId ?? SerializedDataCollection.NoIdDataKey]
            .First();

          return true;
        } else {

          parentModelData = default;
          return false;
        }
      }

      /// <summary>
      /// Update each serialized element and return a new 
      /// </summary>
      internal SerializedDataCollection updateEach(Func<SerializedData, SerializedData> action) {
        SerializedDataCollection result = new SerializedDataCollection();
        foreach (string tableName in Keys) {
          foreach(string modelId in this[tableName].Keys) {
            IEnumerable<SerializedData> items = this[tableName][modelId].ForEach(action);
            result.addSerializedModelsToCollectionByTableNameThenId(items);
          }
        }

        return this;
      }

      /// <summary>
      /// Helper to update/overwite/replace the first data item for the given model.id
      /// </summary>
      void overwriteFirstSerializedDataItemFor(string modelId, SerializedData firstItem) {
        if (TryGetValue(firstItem.tableName, out Dictionary<string, IEnumerable<SerializedData>> serializedModelsById)) {
          if (serializedModelsById.TryGetValue(modelId, out IEnumerable<SerializedData> serializedDatas)) {
            this[firstItem.tableName][modelId] = serializedDatas.Skip(1).Prepend(firstItem);
          } else {
            this[firstItem.tableName][modelId] = firstItem.AsEnumerable();
          }
        } else {
          this[firstItem.tableName] = new Dictionary<string, IEnumerable<SerializedData>> {
            {modelId, firstItem.AsEnumerable()}
          };
        }
      }

      /// <summary>
      /// Helper to collect serialized models by table name and Id
      /// </summary>
      internal void prependSerializedModelToCollectionByTableNameThenId(string modelId, SerializedData modelToAdd) {
        if (TryGetValue(modelToAdd.tableName, out Dictionary<string, IEnumerable<SerializedData>> serializedModelsById)) {
          if (serializedModelsById.TryGetValue(modelId, out IEnumerable<SerializedData> serializedDatas)) {
            this[modelToAdd.tableName][modelId] = serializedDatas.Prepend(modelToAdd);
          } else {
            this[modelToAdd.tableName][modelId] = modelToAdd.AsEnumerable();
          }
        } else {
          this[modelToAdd.tableName] = new Dictionary<string, IEnumerable<SerializedData>> {
            {modelId, modelToAdd.AsEnumerable()}
          };
        }
      }

      /// <summary>
      /// Helper to collect serialized models by table name and Id
      /// </summary>
      internal void addSerializedModelToCollectionByTableNameThenId(string modelId, SerializedData modelToAdd) {
        if (TryGetValue(modelToAdd.tableName, out Dictionary<string, IEnumerable<SerializedData>> serializedModelsById)) {
          if (serializedModelsById.TryGetValue(modelId, out IEnumerable<SerializedData> serializedDatas)) {
            this[modelToAdd.tableName][modelId] = serializedDatas.Append(modelToAdd);
          } else {
            this[modelToAdd.tableName][modelId] = modelToAdd.AsEnumerable();
          }
        } else {
          this[modelToAdd.tableName] = new Dictionary<string, IEnumerable<SerializedData>> {
            {modelId, modelToAdd.AsEnumerable()}
          };
        }
      }

      /// <summary>
      /// Helper to collect serialized models by table name and Id
      /// </summary>
      internal void addSerializedModelToCollectionByTableNameThenId(SerializedData modelToAdd) {
        string modelId = GetModelId(modelToAdd);
        if (TryGetValue(modelToAdd.tableName, out Dictionary<string, IEnumerable<SerializedData>> serializedModelsById)) {
          if (serializedModelsById.TryGetValue(modelId, out IEnumerable<SerializedData> serializedDatas)) {
            this[modelToAdd.tableName][modelId] = serializedDatas.Append(modelToAdd);
          } else {
            this[modelToAdd.tableName][modelId] = modelToAdd.AsEnumerable();
          }
        } else {
          this[modelToAdd.tableName] = new Dictionary<string, IEnumerable<SerializedData>> {
            {modelId, modelToAdd.AsEnumerable()}
          };
        }
      }

      /// <summary>
      /// Helper to collect serialized models by table name and Id
      /// </summary>
      internal void addSerializedModelsToCollectionByTableNameThenId(IEnumerable<SerializedData> modelsToAdd) {
        modelsToAdd.ForEach(modelToAdd => {
          string modelId = GetModelId(modelToAdd);
          if (TryGetValue(modelToAdd.tableName, out Dictionary<string, IEnumerable<SerializedData>> serializedModelsById)) {
            if (serializedModelsById.TryGetValue(modelId, out IEnumerable<SerializedData> serializedDatas)) {
              this[modelToAdd.tableName][modelId] = serializedDatas.Append(modelToAdd);
            } else {
              this[modelToAdd.tableName][modelId] = modelToAdd.AsEnumerable();
            }
          } else {
            this[modelToAdd.tableName] = new Dictionary<string, IEnumerable<SerializedData>> {
              {modelId, modelToAdd.AsEnumerable()}
            };
          }
        });
      }

      /// <summary>
      /// Get a model Id from a set of serialized data
      /// </summary>
      /// <param name="serializedData"></param>
      /// <returns></returns>
      public static string GetModelId(SerializedData serializedData) {
        if (serializedData.sqlData == null) {
          return NoIdDataKey;
        }
        if (serializedData.tableName == ComponentTableNamePlaceholder) {
          return serializedData.sqlData[SQLTypeColumnName] as string;
        } else {
          return (string)(serializedData.sqlData.TryGetValue(SQLIDColumnName, out object foundID) ? foundID : NoIdDataKey);
        }
      }
    }*/
  }
}
