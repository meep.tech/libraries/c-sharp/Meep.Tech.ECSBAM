﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Serialization;

namespace Meep.Tech.Data.Base {

  /// <summary>
  /// Represents a unique serializeable data item with mutable data
  /// </summary>
  public interface IUnique : ISerializeable {

    /// <summary>
    /// The unique id of this item
    /// </summary>
    string id {
      get;
      internal protected set;
    }

    /// <summary>   
    /// Equality helper
    /// </summary>
    public static bool Equals(IUnique unique, IUnique other)
      => unique?.id == other?.id;
  }
}

public static class IUniqueFunctions {

  /// <summary>
  /// Helper for setting the owner
  /// </summary>
  internal static void setUniqueId(this IUnique unique, string uniqueId) {
    unique.id = uniqueId;
  }

  /// <summary>
  /// Helper for getting unique id from inherting class
  /// </summary>
  public static string getUniqueId(this IUnique unique)
    => unique.id;

}
