﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Data;
using Meep.Tech.Data.Attributes.Models;
using Meep.Tech.Data.Base;
using Meep.Tech.Data.Static;
using Meep.Tech.Data.Extensions;
using System.Collections;

namespace Meep.Tech.Data.Serialization {

  /// <summary>
  /// Used to serialized items.
  /// NOTE: lot of the time, serializeable items are just called Models for conveneince here.
  /// </summary>
  public partial class Serializer {

    /// <summary>
    /// The deph at which to load data for this model and it's hirerachal children
    /// Light means Load Light Data only, and light loading a child field means: don't get the full data from the DB and only load the model's ID and type etc.
    /// Full means Fully load all model data fields, and fully loading children means: fetch the docs for any children from the DB by ID.
    /// </summary>
    public enum LoadDepth {
      Model_AllRecusiveChildren = -1,
      Model_OnlyFieldData = 0,
      Model_FirstChildren = 1,
      Model_FirstChildren_NextChildren = 2,
      Model_FirstChildren_NextChildren_ThenTheirChildren = 3
    }

    public enum SaveDepth {
      ModelData_AllRecusiveChildData = 0,
      ModelDataOnly = 1
    }

    /// <summary>
    /// Settings for the Serializer
    /// </summary>
    public static class Settings {

      /// <summary>
      /// Json serializer settings for easy Component serialization
      /// </summary>
      public static JsonSerializerSettings ComponentJsonSerializerSettings {
        get;
        internal set;
      }

      /// <summary>
      /// Compiled component serializer from the above settings
      /// </summary>
      public static JsonSerializer ComponentJsonSerializer {
        get => _componentJsonSerializer ??= JsonSerializer.Create(ComponentJsonSerializerSettings);
      } static JsonSerializer _componentJsonSerializer;

      /// <summary>
      /// Storage/saving related settings.
      /// </summary>
      public static class Storage {

        #region Deligates

        /// <summary>
        /// Fetch data for a single Unique with a unique id:
        /// </summary>
        public delegate SerializedData? FetchModelData(
          string modelUniqueId,
          Type modelType
        );

        /// <summary>
        /// Fetch all models of a type that match the given select statement
        /// </summary>
        /// <param name="select">The full select statement.</param>
        public delegate List<Serializer.SerializedData> FetchAllModelsDataViaUnsafeSelect(
          string select,
          Type modelType
        );

        /// <summary>
        /// Fetch all models of a type that match the given where statement
        /// </summary>
        /// <param name="where">The where clause to add after WHERE</param>
        public delegate List<Serializer.SerializedData> FetchAllModelsWhereDatas(
          string where,
          Type modelType,
          IEnumerable<(string, object)> extraParameters = null
        );

        /// <summary>
        /// Used to fetch chld model datas using a parent id and optional fields
        /// </summary>
        /// <param name="parentModelUniqueId">The id of the parent, in the 'owner' field of the model's were looking for</param>
        /// <param name="childModelType">The type of the child models that we want</param>
        /// <param name="parentModelFieldName">(optional) the name of the parent field we want child models for/from</param>
        /// <param name="whereClause">(optional) some clausal text to add after the "WHERE"</param>
        /// <returns></returns>
        public delegate List<SerializedData> FetchChildModelsDatas(
          string parentModelUniqueId,
          Type childModelType,
          string parentModelFieldName = null,
          string whereClause = null
        );

        public delegate SerializedData? FetchLinkedModelsInfos(
          string parentModelUniqueId,
          SerializedData? existingData = null
        );

        public delegate Dictionary<string, object> FetchModelImageData(
          string modelUniqueId,
          string imageFieldName
        );

        public delegate void SaveModelDatas(
          Dictionary<string, Dictionary<string, ICollection<SerializedData>>> serializedModelDatas
        );

        public delegate void DeleteModelDatas(
          IEnumerable<(string, Type)> models,
          bool andAllChildrenRecusively = true
        );

        public delegate void DeleteChildModelDatas(
          string parentModelId,
          Type childModelsType,
          string childModelsFieldName
        );

        #endregion

        #region Required Logic
        //// This is logic that needs to be set by the storage handler

        /// <summary>
        /// Root path for save data of this kind
        /// </summary>
        public static string RootFileSystemSavePath;

        /// <summary>
        /// Logic that needs to be set for fetching a single models data.
        /// </summary>
        public static FetchModelData FetchDataForModelLogic;

        /// <summary>
        /// Logic that needs to be set for fetching all models datas that match a given select query.
        /// </summary>
        public static FetchAllModelsDataViaUnsafeSelect FetchDataForAllModelsUnsafeSelectLogic;

        /// <summary>
        /// Logic that needs to be set for fetching all models datas that match a given where clause.
        /// </summary>
        public static FetchAllModelsWhereDatas FetchDataForAllModelsWhereAddendumLogic;

        /// <summary>
        /// Logic that needs to be set for fetching a model's children
        /// </summary>
        public static FetchChildModelsDatas FetchDataForChildModelsLogic;

        /// <summary>
        /// Logic that needs to be set for fetching a model's linked child ids
        /// </summary>
        public static FetchLinkedModelsInfos FetchInfoForLinkedModelsLogic;

        /// <summary>
        /// Logic that needs to be set for fetching an image for a model
        /// </summary>
        public static FetchModelImageData FetchDataForModelImageLogic;

        /// <summary>
        /// Logic that needs to be set to save models
        /// </summary>
        public static SaveModelDatas SaveDatasForModelsLogic;

        /// <summary>
        /// Used for deleting single models by id
        /// </summary>
        public static DeleteModelDatas DeleteDataForModelsLogic;

        /// <summary>
        /// Used for deleting multiple child models by parent id:
        /// </summary>
        public static DeleteChildModelDatas DeleteChildModelDatasLogic;

        #endregion
      }
    }

    #region Constants

    /// Column Names:
    public const string SQLIDColumnName = "id";
    public const string SQLTypeColumnName = "type";
    public const string SQLOwnerColumnName = "owner";
    public const string SQLOwnerModelFieldNameColumnName = "ownerModelFieldName";
    public const string SQLJSONDataColumnName = "data";
    public const string SQLComponentsDataColumnName = "components";


    ///<summary>
    ///Used to delete entries in a table by parent model id
    ///</summary>
    public const string DeleteDataKey = "__delete_";

    /// <summary>
    /// Placeholders for serializeable components which don't have tables:
    /// </summary>
    public const string ComponentKeyNameAndTablePlaceholder = "__components_";

    /// Special Keys:
    ///<summary>
    /// Used to represent non unique models without an id 
    ///</summary>
    public const string NoIdDataKey = "__noid_";

    #endregion

    #region Initialization

    public static void Initialize() { }

    #endregion

    #region Save & Load

    #region Save

    /// <summary>
    /// Save this model to the DB by it's unique id
    /// </summary>
    public static void Save<TUniqueSerializeable>(
      TUniqueSerializeable model,
      SaveDepth saveDepth = SaveDepth.ModelData_AllRecusiveChildData
    ) where TUniqueSerializeable : IUnique {
      if(model.id == null) {
        throw new MissingFieldException("Cannot save a unique model without an id");
      }

      SerializedData serializedModelDatas = Serialize(model, saveDepth == SaveDepth.ModelDataOnly);
      serializedModelDatas.save();
    }

    /// <summary>
    /// Save this model to the DB by it's unique id
    /// </summary>
    public static void SaveChild<TOwnedSerializeable>(
      TOwnedSerializeable model,
      string parentModelFieldKey,
      SaveDepth saveDepth = SaveDepth.ModelData_AllRecusiveChildData
    ) where TOwnedSerializeable : IOwned {
      if(model.owner == null) {
        throw new MissingFieldException("Cannot save a child without an owner id");
      }

      SerializedData serializedModelDatas = Serialize(model, saveDepth == SaveDepth.ModelDataOnly);
      serializedModelDatas.setParentModelFieldIfEmpty(parentModelFieldKey);
      Settings.Storage.SaveDatasForModelsLogic(serializedModelDatas.getAllChildrenRecursively()
        // add this too!
        .Merge(new Dictionary<string, Dictionary<string, ICollection<SerializedData>>> {
          {
            serializedModelDatas.tableName,
            new Dictionary<string, ICollection<SerializedData>> {
              {serializedModelDatas.modelUniqueId, serializedModelDatas.AsEnumerable().ToList() } }
            }
          }
        ));
    }

    /// <summary>
    /// Save this model to the DB by it's unique id
    /// </summary>
    public static void SaveChildren<TOwnedSerializeable>(
      IEnumerable<TOwnedSerializeable> models,
      string parentModelFieldKey,
      SaveDepth saveDepth = SaveDepth.ModelData_AllRecusiveChildData
    ) where TOwnedSerializeable : IOwned {
      if(models.Any(model => model.owner == null)) {
        throw new MissingFieldException("Cannot save a child without an owner id");
      }

      Dictionary<string, Dictionary<string, ICollection<SerializedData>>> valuesToSerialize
       = new Dictionary<string, Dictionary<string, ICollection<SerializedData>>>();

      IEnumerable<SerializedData> serializedModelDatas = models.Select(model => Serialize(model, saveDepth == SaveDepth.ModelDataOnly));
      serializedModelDatas.ForEach(
        serializedModelData => {
          // merge all the datas together:
          serializedModelData.setParentModelFieldIfEmpty(parentModelFieldKey);
          valuesToSerialize.Merge(serializedModelData.getAllChildrenRecursively()
            // add this one too!
            .Merge(new Dictionary<string, Dictionary<string, ICollection<SerializedData>>> {
              {
                serializedModelData.tableName,
                new Dictionary<string, ICollection<SerializedData>> {
                  {serializedModelData.modelUniqueId, serializedModelData.AsEnumerable().ToList() } }
                }
              }
            )
          );
        }
      );

      Settings.Storage.SaveDatasForModelsLogic(valuesToSerialize);
    }

    #endregion

    #region Load

    /// <summary>
    /// Load data from storage into this model based on it's ID
    /// </summary>
    public static TUniqueSerializeable Load<TUniqueSerializeable>(
      TUniqueSerializeable model,
      LoadDepth loadDepth = LoadDepth.Model_AllRecusiveChildren,
      bool loadLinkedModelInfos = true
    ) where TUniqueSerializeable : IUnique
      => Load(ref model, loadDepth, loadLinkedModelInfos) ? model : default;

    /// <summary>
    /// Load a unique model from storage  based on it's ID
    /// </summary>
    public static IUnique Load(
      string modelId,
      Type modelType,
      LoadDepth loadDepth = LoadDepth.Model_AllRecusiveChildren,
      bool loadLinkedModelInfos = true
    ) => Load(modelId, modelType, out IUnique model, loadDepth, loadLinkedModelInfos)
      ? model
      : default;

    /// <summary>
    /// Load a unique model from storage  based on it's ID
    /// </summary>
    public static bool Load(
      string modelId,
      Type modelType,
      out IUnique model,
      LoadDepth loadDepth = LoadDepth.Model_AllRecusiveChildren,
      bool loadLinkedModelInfos = true
    ) {
      if(typeof(ICached).IsAssignableFrom(modelType)) {
        IUnique cachedModel;
        if((cachedModel = ICached.FromCache(modelId)) != null) {
          if(cachedModel.GetType() != modelType) {
            throw new InvalidCastException($"Fetched Model From Cache with ID {modelId} is likely not of desired type {modelType}. Actual type: {cachedModel.GetType().FullName ?? "NULL"}");
          } else {
            model = cachedModel;
            return true;
          }
        }
      }

      /// Get the root model's data
      SerializedData? serializedModelData = FetchDataForModel(modelId, modelType);

      // TODO: should this throw in the future? or maybe return bool and out model instead? probably the second.
      if(!serializedModelData.HasValue) {
        model = null;
        return false;
      }

      model = Archetypes.Ids[serializedModelData.Value.sqlData[SQLTypeColumnName] as string]
        .Archetype.MakeI(serializedModelData.Value, loadDepth == LoadDepth.Model_AllRecusiveChildren ? null : (int?)loadDepth) as IUnique;

      /// load all the children to the specified depth. 
      /// Loading children recusively only works if the children models are objects, not structs.
      /// For structs to work you may need to use custom [attributes]
      if(loadDepth != 0) {
        model = LoadAllChildrenOnto(
          model,
          loadDepth == LoadDepth.Model_AllRecusiveChildren
            ? (int?)null
            : (int)loadDepth - 1,
          loadLinkedModelInfos
        );
      }

      return true;
    }

    /// <summary>
    /// Load data from storage into this model based on it's ID
    /// </summary>
    public static bool Load<TUniqueSerializeable>(
      ref TUniqueSerializeable model,
      LoadDepth loadDepth = LoadDepth.Model_AllRecusiveChildren,
      bool loadLinkedModelInfos = true
    ) where TUniqueSerializeable : IUnique {
      if(Load(model.id, model.GetType(), out var loadedModel, loadDepth, loadLinkedModelInfos)) {
        model = (TUniqueSerializeable)loadedModel;
        return true;
      }

      return false;
    }

    /// <summary>
    /// Load all models using a given select query:
    /// </summary>
    public static ICollection<TSerializeableModel> LoadAllWith<TSerializeableModel>(
      string select,
      LoadDepth loadDepth = LoadDepth.Model_AllRecusiveChildren,
      bool? isUnique = null 
    ) where TSerializeableModel : ISerializeable {
      int? depthLimit = loadDepth == LoadDepth.Model_AllRecusiveChildren
        ? (int?)null
        : (int)loadDepth;
      List<SerializedData> modelCollectionDatas =
        FetchDataForAll<TSerializeableModel>(select);

      return DeserializeModelCollectionGenerically<TSerializeableModel>(
        isUnique,
        depthLimit,
        ref modelCollectionDatas
      );
    }

    /// <summary>
    /// Load all models using a given select query:
    /// </summary>
    public static ICollection<TSerializeableModel> LoadAll<TSerializeableModel>(
      string where,
      LoadDepth loadDepth = LoadDepth.Model_AllRecusiveChildren,
      bool? isUnique = null 
    ) where TSerializeableModel : ISerializeable {
      int? depthLimit = loadDepth == LoadDepth.Model_AllRecusiveChildren
        ? (int?)null
        : (int)loadDepth;
      List<SerializedData> modelCollectionDatas =
        FetchDataForAllWhere<TSerializeableModel>(where);

      return DeserializeModelCollectionGenerically<TSerializeableModel>(
        isUnique,
        depthLimit,
        ref modelCollectionDatas
      );
    }

    /// <summary>
    /// Load a single model's data onto this based on the select query.
    /// </summary>
    public static TSerializeableModel LoadForWith<TSerializeableModel>(
      TSerializeableModel model,
      string select,
      LoadDepth loadDepth = LoadDepth.Model_AllRecusiveChildren
    ) where TSerializeableModel : ISerializeable {
      int? depthLimit = loadDepth == LoadDepth.Model_AllRecusiveChildren
        ? (int?)null
        : (int)loadDepth;
      var modelDatas =
        FetchDataForAll<TSerializeableModel>(select);

      if(modelDatas?.Any() ?? false) {
        return model;
      }

      SerializedData modelData = modelDatas.First();

      IUnique unique = model as IUnique;
      if(unique != null) {
        modelData = Settings.Storage.FetchInfoForLinkedModelsLogic(
            modelData.modelUniqueId,
            modelData
          ) ?? modelData;
      }

      model = (TSerializeableModel)model
        .deserialize(modelData, depthLimit);

      if(unique != null) {
        model = (TSerializeableModel)LoadAllChildrenOnto(unique, depthLimit - 1);
      }

      return model;
    }

    /// <summary>
    /// Load a single model's data onto this based on the select query.
    /// </summary>
    public static TSerializeableModel LoadFor<TSerializeableModel>(
      TSerializeableModel model,
      string where,
      LoadDepth loadDepth = LoadDepth.Model_AllRecusiveChildren
    ) where TSerializeableModel : ISerializeable {
      int? depthLimit = loadDepth == LoadDepth.Model_AllRecusiveChildren
        ? (int?)null
        : (int)loadDepth;
      var modelDatas =
        FetchDataForAllWhere<TSerializeableModel>(where);

      if(modelDatas?.Any() ?? false) {
        return model;
      }

      SerializedData modelData = modelDatas.First();

      IUnique unique = model as IUnique;
      if(unique != null) {
        modelData = Settings.Storage.FetchInfoForLinkedModelsLogic(
            modelData.modelUniqueId,
            modelData
          ) ?? modelData;
      }

      model = (TSerializeableModel)model
        .deserialize(modelData, depthLimit);

      if(unique != null) {
        model = (TSerializeableModel)LoadAllChildrenOnto(unique, depthLimit - 1);
      }

      return model;
    }

    /// <summary>
    /// Load a single model's data onto this based on the select query.
    /// </summary>
    public static bool LoadWhere<TSerializeableModel>(
      ref TSerializeableModel model,
      string fieldName,
      object equals,
      LoadDepth loadDepth = LoadDepth.Model_AllRecusiveChildren
    ) where TSerializeableModel : ISerializeable {
      int? depthLimit = loadDepth == LoadDepth.Model_AllRecusiveChildren
        ? (int?)null
        : (int)loadDepth;
      var modelDatas =
        Settings.Storage.FetchDataForAllModelsWhereAddendumLogic(
          where: $" {fieldName} = @value ", 
          typeof(TSerializeableModel),
          ("value", equals).AsEnumerable()
        );

      if(modelDatas?.Any() ?? false) {
        return false;
      }

      SerializedData modelData = modelDatas.First();

      IUnique unique = model as IUnique;
      if(unique != null) {
        modelData = Settings.Storage.FetchInfoForLinkedModelsLogic(
            modelData.modelUniqueId,
            modelData
          ) ?? modelData;
      }

      model = (TSerializeableModel)model
        .deserialize(modelData, depthLimit);

      if(unique != null) {
        model = (TSerializeableModel)LoadAllChildrenOnto(unique, depthLimit - 1);
      }

      return true;
    }

    /// <summary> 
    /// Load all children of a given type that belong to the given parent model.
    /// </summary>
    /// <typeparam name="TChildModel">The desired type of child model.</typeparam>
    /// <param name="depthLimit">How many levels down to stop loading child models.</param>
    /// <param name="parentModelField">The field on the parent model that we want children for, if any.</param>
    /// <param name="where">optional where clause for the child models.</param>
    /// <returns></returns>
    public static ICollection<TChildModel> LoadAllChildrenOf<TChildModel>(
      IUnique parentModel,
      int? depthLimit = null,
      string parentModelField = null,
      string where = null
    ) where TChildModel : ISerializeable, IOwned {
      // cache data for parent:
      Type parentModelType = parentModel.GetType();
      bool? isUnique = null;
      List<SerializedData> childModelDatas = new List<SerializedData>();

      if(!string.IsNullOrEmpty(parentModelField)) {
        List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> parentModelDataFieldsInfo
        = Cache.GetFieldsInfo(parentModelType);

        // Make sure the parent model has this field:
        (MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)? fieldInfo;
        try {
          fieldInfo
            = parentModelDataFieldsInfo.First(field => field.GetNameKey() == parentModelField);
          isUnique = (fieldInfo.Value.dataModelFieldInfo as ChildModelFieldAttribute).IsUnique;
        }
        catch(Exception e) {
          throw new ArgumentException($"Child field with the name {parentModelField} is not present in the data model type: {parentModelType?.Name ?? "MISSINGTYPE"}", e);
        }
      }

      childModelDatas =
        FetchChildModelsDataFor(
            parentModel.id,
            parentModelField,
            typeof(TChildModel),
            where
          );

      /*if(!childModelDatas.Any()) {
        return Enumerable.Empty<TChildModel>().ToList();
      }

      /// links
      childModelDatas = childModelDatas.Select(
        childModelData
          => Settings.Storage.FetchInfoForLinkedModelsLogic(
            childModelData.modelUniqueId,
            childModelData
          ) ?? childModelData
      ).ToList();

      /// deserialize
      IEnumerable<TChildModel> childModelsForThisFieldType 
        = childModelDatas.Select(serializedChildData
          => (TChildModel)
            (Archetypes.Ids[serializedChildData.sqlData[SQLTypeColumnName] as string]
              .Archetype.MakeI(
                serializedChildData,
                depthLimit - 1
              )));

      // if the child fields are unique and we have depth to descend, recurse.
      /// Loading children recusively only works if the children models are objects, not structs.
      /// For structs to work you may need to use custom [attributes]
      if((childModelsForThisFieldType?.Any() ?? false)
        && ((!depthLimit.HasValue) || depthLimit.Value > 0)
        && ((isUnique.HasValue && isUnique.Value)
          || (!isUnique.HasValue && typeof(IUnique).IsAssignableFrom(typeof(TChildModel)))
        )
      ) {
        childModelsForThisFieldType.ForEach(child =>
           LoadAllChildrenOnto((IUnique)child, depthLimit - 1)
        );
      }

      return childModelsForThisFieldType.ToList();*/
      return DeserializeModelCollectionGenerically<TChildModel>(isUnique, depthLimit, ref childModelDatas);
    }

    /// <summary>
    /// Load all child models for the given model:
    /// </summary>
    public static IUnique LoadAllChildrenOnto(
      IUnique parentModel,
      int? depthLimit = null,
      bool loadLinkedModelInfoForChildren = true
    ) {
      // cache data for parent:
      Type parentModelType = parentModel.GetType();
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> parentModelDataFieldsInfo
        = Cache.GetFieldsInfo(parentModelType);

      // for each child model field, load all the child models:
      foreach ((MemberInfo objectFieldInfo, ChildModelFieldAttribute dataModelFieldInfo) childModelField in parentModelDataFieldsInfo
        .Where(field => field.dataModelFieldInfo is ChildModelFieldAttribute childFieldInfo && !(childFieldInfo is LinkedModelDataFieldAttribute))
      ) {
        string childFieldName = childModelField.GetNameKey();

        /// Fetch the child model datas:
        List<SerializedData> childModelDatas = childModelField.dataModelFieldInfo is ModelImageFieldAttribute imageFieldInfo
          // image field
          ? SerializedData.MakeForImage(
            Settings.Storage.FetchDataForModelImageLogic(parentModel.id, childFieldName)
          ).AsEnumerable().ToList()
          // normal field
          : FetchChildModelsDataFor(
            parentModel.id,
            childFieldName,
            childModelField.dataModelFieldInfo.ModelType
          );

        if(!childModelDatas.Any()) {
          continue;
        }

        // load each one's linked models if we're going recursive:
        if(loadLinkedModelInfoForChildren) {
          childModelDatas = childModelDatas.Select(
            childModelData 
              => Settings.Storage.FetchInfoForLinkedModelsLogic(
                childModelData.modelUniqueId,
                childModelData
              ) ?? childModelData
          ).ToList();
        }

        parentModel = DeserializeChildFieldOnto(
          parentModel,
          childModelDatas,
          childModelField,
          depthLimit,
          out List<ISerializeable> childModelsForThisFieldType
        );

        // if the child fields are unique and we have depth to descend, recurse.
        /// Loading children recusively only works if the children models are objects, not structs.
        /// For structs to work you may need to use custom [attributes]
        if((childModelsForThisFieldType?.Any() ?? false) 
          && ((!depthLimit.HasValue) || depthLimit.Value > 0) 
          && childModelField.dataModelFieldInfo.IsUnique
        ) {
          childModelsForThisFieldType?.ForEach(child => {
            child = LoadAllChildrenOnto(child as IUnique, depthLimit - 1);
          });
        }
      }

      return parentModel;
    }

    #endregion

    /// <summary>
    /// Delete a model from the DB
    /// </summary>
    /// <param name="model"></param>
    public static void Delete(IUnique model, bool andAllRecursiveChildren = true)
      => Settings.Storage.DeleteDataForModelsLogic((model.id, model.GetType()).AsEnumerable(), andAllRecursiveChildren);

    #region Storage Helpers
    // TODO: Make one for the linked stuff

    /// <summary>
    /// Logic that needs to be set for fetching a single models data.
    /// </summary>
    public static SerializedData? FetchDataForModel(string modelUniqueId, Type modelType)
      => Settings.Storage.FetchDataForModelLogic(modelUniqueId, modelType);

    /// <summary>
    /// Logic that needs to be set for fetching a single models data.
    /// </summary>
    public static SerializedData? FetchDataFor<TModel>(string modelUniqueId) where TModel : IUnique
      => Settings.Storage.FetchDataForModelLogic(modelUniqueId, typeof(TModel));

    /// <summary>
    /// Logic that needs to be set for fetching a single models data.
    /// </summary>
    public static List<SerializedData> FetchDataForAll<TSerializeableModel>(string select) where TSerializeableModel : ISerializeable
      => Settings.Storage.FetchDataForAllModelsUnsafeSelectLogic(select, typeof(TSerializeableModel));

    /// <summary>
    /// Logic that needs to be set for fetching a single models data.
    /// </summary>
    public static List<SerializedData> FetchDataForAllWhere<TSerializeableModel>(string where) where TSerializeableModel : ISerializeable
      => Settings.Storage.FetchDataForAllModelsWhereAddendumLogic(where, typeof(TSerializeableModel));

    /// <summary>
    /// Logic that needs to be set for fetching a model's children
    /// </summary>
    public static List<SerializedData> FetchChildModelsDataFor<TChildModel>(
      string parentModelUniqueId,
      string parentModelFieldName,
      string whereClause = null
    ) where TChildModel : IOwned
      => FetchChildModelsDataFor(
        parentModelUniqueId,
        parentModelFieldName,
        typeof(TChildModel),
        whereClause
      );

    /// <summary>
    /// Logic that needs to be set for fetching a model's children
    /// </summary>
    public static List<SerializedData> FetchChildModelsDataFor(string parentModelUniqueId, string parentModelFieldName, Type childModelType, string whereClause = null)
      => Settings.Storage.FetchDataForChildModelsLogic(parentModelUniqueId, childModelType, parentModelFieldName, whereClause);

    /// <summary>
    /// Logic that needs to be set to save models
    /// </summary>
    public static void SaveModelDatas(Dictionary<string, Dictionary<string, ICollection<SerializedData>>> serializedModelDatasByTableThenById)
      => Settings.Storage.SaveDatasForModelsLogic(serializedModelDatasByTableThenById);

    #endregion

    #endregion

    #region Serialization and Deserialization

    /// <summary>
    /// Serialize a model's data
    /// </summary>
    internal static SerializedData Serialize<TSerializeable>(
      TSerializeable model,
      bool thisModelsDataOnly = false,
      bool serializeLinkedModelsAsChildren = true
    ) where TSerializeable : ISerializeable {
      if (model == null) {
        return new SerializedData();
      }

      // get the fields data we need from the cache
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> modelDataFields
        = Cache.GetFieldsInfo(model);
      string parentModelTableName 
        = Cache.GetDataModelInfo(model).TableName;

      SerializedData serializedModel
        = new SerializedData(parentModelTableName, model is IFullySerializeableComponent);

      string parentModelId = null;

      // create the root json object
      JObject modelJSONFields = new JObject();
      Dictionary<string, object> modelSQLFields = new Dictionary<string, object>();
      foreach ((MemberInfo field, ModelDataFieldAttribute serializationInfo) in modelDataFields) {
        // skip these, they must be serialized manually
        if (field == null) {
          continue;
        }

        /// Try to get the field data:
        try {
          object fieldValue = field.GetValue(model);

          // If this is a child field, ignore it if we only want this model's data, unless it's not unique to it's own table.
          if (serializationInfo is ChildModelFieldAttribute childFieldInfo) {

            // models without unique ids are always saved with their parent
            if (fieldValue != null) {
              if (!thisModelsDataOnly || !childFieldInfo.IsUnique) {
                string parentModelFieldName = (field, serializationInfo).GetNameKey();
                /// add the special delete info data:
                if (childFieldInfo is ChildModelsCollectionFieldAttribute childCollection
                  && childCollection.DeleteAllOldChildrenBeforeSavingNewChildrenOfThisType
                ) {
                  parentModelId = (model as IUnique).id;
                  serializedModel.addChild(
                    childCollection.Name ?? field.Name,
                    SerializedData.MakeDeletionEntry(
                      parentModelId,
                      childCollection.TableName,
                      parentModelFieldName
                    )
                  );
                }

                /// add the special delete info data:
                if (childFieldInfo is LinkedModelDataFieldAttribute linkedFieldInfo
                  && linkedFieldInfo.DeleteAllExistingChildLinksBeforeSaving
                ) {
                  serializedModel.addLink(
                    parentModelFieldName,
                    linkedFieldInfo.TableName,
                    Serializer.DeleteDataKey
                  );
                }

                // serialize + add/link:
                serializedModel = SerializeAndAddOrLinkChildField(
                  model,
                  serializeLinkedModelsAsChildren,
                  serializedModel,
                  fieldValue,
                  childFieldInfo,
                  parentModelFieldName
                );
              } // linked children are also always at least linked:
              else if (thisModelsDataOnly && childFieldInfo is LinkedModelDataFieldAttribute linkedFieldInfo) {
                serializedModel = AddLinksWithoutSerializedDatas(
                  serializedModel,
                  fieldValue, 
                  (field, serializationInfo).GetNameKey(),
                  linkedFieldInfo
                );
              }
            }

            continue;
          } // image field saving
          else if (serializationInfo is ModelImageFieldAttribute imageFieldInfo) {
            if (model is IUnique parentModel) {
              parentModelId = parentModel.id;
              // if there's a dirty check generated, check if it's dirty, else always save it
              if (imageFieldInfo.CheckisDirty?.Invoke(model) ?? true) {
                string childFieldName = (field, serializationInfo).GetNameKey();
                serializedModel.addChild(
                  childFieldName,
                  (
                    ModelImageFieldAttribute.ImageTableName,
                    imageFieldInfo.SerializeToSQLRow(
                      parentModel.id,
                      childFieldName,
                      parentModelTableName,
                      fieldValue
                    ),
                    null
                  )
                );
              }

              continue;
            } else throw new NotImplementedException($"Cannot save images on non-IUnique models. Images are saved by parent model id");
          } // apply any custom serialization to other fields:
          else if (serializationInfo.HasCustomSerialization) {
            fieldValue = serializationInfo.HasAdvancedSerialization
              ? serializationInfo.AdvancedSerializationFunction?.Invoke(fieldValue, model as IUnique)
                ?? serializationInfo.AdvancedJSONSerializationFunction(fieldValue, model as IUnique)
              : serializationInfo.SerializationFunction(fieldValue);

            // if this was the model components, we need to grab any fully serialized ones.
            if (serializationInfo is ModelDataComponentCollectionFieldAttribute componentCollectionFieldAttribute) {
              (string fieldValueJson, List<SerializedData> fullySerializedComponents) 
                = ((string, List<SerializedData>))fieldValue;
              fieldValue = fieldValueJson;
              fullySerializedComponents?.ForEach(componentData => 
                serializedModel.addChild(
                  SQLComponentsDataColumnName,
                  componentData
                )
              );
            }
          }

          // if this a sql field, we need to add it to the sql list with it's field name:
          if (serializationInfo.IsASQLField) {
            string column = (field, serializationInfo).GetNameKey();
            modelSQLFields.Add(column, fieldValue);
          } // If this is a json field, add it to the json:
          else {
            JToken serializedFieldData = fieldValue is JToken serializedFieldValue
              ? serializedFieldValue
              : fieldValue == null
                ? JValue.CreateNull()
                : JToken.Parse(JsonConvert.SerializeObject(fieldValue));

            // add the field to the jsons
            string key = (field, serializationInfo).GetNameKey();
            modelJSONFields.Add(key, serializedFieldData);
          }
        } catch (Exception e) {
          throw new Exception($"Failed to serialize the field: {(field, serializationInfo).GetNameKey()}, on the model: {model}, of type: {model.GetType()}.\nINTERNAL ERROR: {e}");
        }
      }

      // place this model's data first
      serializedModel.setData(modelSQLFields, modelJSONFields);

      return serializedModel;
    }

    /// <summary>
    /// Logic to serialize and add a child field
    /// </summary>
    static SerializedData SerializeAndAddOrLinkChildField<TSerializeable>(TSerializeable model, bool serializeLinkedModelsAsChildren, SerializedData serializedModel, object fieldValue, ChildModelFieldAttribute childFieldInfo, string parentModelFieldName) where TSerializeable : ISerializeable {
      // if we're not serializing linked models fully, and this is for linked models:
      LinkedModelDataFieldAttribute linkedFieldInfo = childFieldInfo as LinkedModelDataFieldAttribute;
      if (!serializeLinkedModelsAsChildren && linkedFieldInfo != null) {
        serializedModel = AddLinksWithoutSerializedDatas(serializedModel, fieldValue, parentModelFieldName, linkedFieldInfo);
      } else {
        // serialize the child field models.
        /// The default ChildModelSerializationFunction calls Serialize recursively.
        var serializedChildren = (childFieldInfo.HasAdvancedChildSerialization
          ? childFieldInfo.ChildModelAdvancedSerializationFunction(fieldValue, model)
          : childFieldInfo.ChildModelSerializationFunction(fieldValue)
        );

        if (linkedFieldInfo != null) {
          serializedChildren.ForEach(linkedModelData => serializedModel.addLink(
            parentModelFieldName,
            linkedFieldInfo.TableName,
            linkedModelData.modelUniqueId,
            linkedModelData
          ));
        } else {
          serializedChildren.ForEach(childModel => serializedModel.addChild(
            parentModelFieldName,
            childModel
          ));
        }
      }

      return serializedModel;
    }

    /// <summary>
    /// Helper to just add links to the current serialized model
    /// </summary>
    static SerializedData AddLinksWithoutSerializedDatas(SerializedData serializedModel, object fieldValue, string parentModelFieldName, LinkedModelDataFieldAttribute linkedFiledInfo) {
      // collection:
      if (linkedFiledInfo is LinkedModelCollectionDataField linkedCollection) {
        ChildModelsCollectionFieldAttribute
          .Select(fieldValue, (linkedModel, _) => linkedModel)
          .Cast<ICached>()
          .ForEach(childModel => serializedModel.addLink(
            parentModelFieldName,
            linkedCollection.TableName,
            childModel.id
          )
        );
      } // single model:
      else {
        serializedModel.addLink(
          parentModelFieldName,
          linkedFiledInfo.TableName,
          ((IUnique)fieldValue).id
        );
      }

      return serializedModel;
    }

    /// <summary>
    /// Deserialize an entire tree of models as best we can from a serialized collection
    /// </summary>
    internal static TSerializeable Deserialize<TSerializeable>(
      TSerializeable model,
      SerializedData serializedModelData,
      int? depthLimit = null,
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> modelDataFields = null,
      bool loadMissingLinkedModels = true
    ) where TSerializeable : ISerializeable {
      IUnique parent = null;
      // get the fields data we need from the cache
      modelDataFields ??= Cache.GetFieldsInfo(model);

      /// deserialize each field onto the model:
      foreach((MemberInfo fieldInfo, ModelDataFieldAttribute fieldSerializationInfo) in modelDataFields) {
        // if we don't deserialize this field
        if(fieldSerializationInfo.SkipDuringDeserialization) {
          continue;
        }

        string fieldName = (fieldInfo, fieldSerializationInfo).GetNameKey();
        // if this is a child model field, we want to skip it. They get loaded through LoadAllChildModels
        if(fieldSerializationInfo is ChildModelFieldAttribute childModelFieldSerializationInfo) {
          parent ??= model as IUnique;

          /// Linked models
          if(childModelFieldSerializationInfo is LinkedModelDataFieldAttribute linkedModelInfo) {
            IEnumerable<string> linkedChildIds
              = serializedModelData.linkedDataInfosByParentField != null 
                && serializedModelData.linkedDataInfosByParentField.TryGetValue(fieldName, out var values)
                   ? values.Select(entry => entry.id) 
                   : Enumerable.Empty<string>();
            List<string> notFound = new List<string>();
            
            // try to get all of them from the cache:
            IEnumerable<IUnique> linkedModels = linkedChildIds.SelectWhere(id => {
              IUnique found = linkedModelInfo.FetchModelFromCache?.Invoke(id);
              if (found != null) {
                return (true, found);
              }
              else {
                notFound.Add(id);
                return default;
              }
            });

            // Load any not found models fully:
            if(loadMissingLinkedModels && notFound.Any()) {
              linkedModels = linkedModels.Concat(
                notFound.Select(
                  unloadedId
                    => Load(unloadedId, linkedModelInfo.ModelType)
                ).Where(value => value != null)
              );
            }

            // try to set them all at once using an enumerable.
            if(linkedModels.Any()) {
              var linkedModelCollectionInfo = linkedModelInfo as LinkedModelCollectionDataField;
              if(linkedModelCollectionInfo != null && linkedModelCollectionInfo.AdvancedSetCollectionOnParent != null) {
                model = (TSerializeable)linkedModelCollectionInfo.AdvancedSetCollectionOnParent(parent, linkedModels);
              }
              else {
                model = (TSerializeable)SetChildValueOn(
                  parent,
                  // if it's a collection there's multiple, if there's one there's one.
                  linkedModelCollectionInfo != null
                    ? (object)linkedModels
                    : linkedModels?.FirstOrDefault(),
                  fieldInfo,
                  linkedModelInfo
                );
              }
            }

            continue;
          }

          // if we reached serialization detph limit, don't go deeper
          if(!(depthLimit.HasValue && depthLimit.Value <= 0)) {
            DeserializeChildFieldOnto(
              parent,
              serializedModelData.getChildren(fieldName),
              (fieldInfo, childModelFieldSerializationInfo),
              depthLimit - 1,
              out _
            );
          }
        }
        else {
          model = DeserializeBasicFieldOnto(
            model,
            serializedModelData,
            fieldInfo,
            fieldSerializationInfo
          );
        }
      }

      return model;
    }


    /// <summary>
    /// Helper to deserialize a collection of models of the same type in the default way.
    /// </summary>
    static ICollection<TSerializeableModel> DeserializeModelCollectionGenerically<TSerializeableModel>(bool? isUnique, int? depthLimit, ref List<SerializedData> modelCollectionDatas) where TSerializeableModel : ISerializeable {
      if(!modelCollectionDatas.Any()) {
        return Enumerable.Empty<TSerializeableModel>().ToList();
      }

      /// load links
      modelCollectionDatas = modelCollectionDatas.Select(
        modelData
          => Settings.Storage.FetchInfoForLinkedModelsLogic(
            modelData.modelUniqueId,
            modelData
          ) ?? modelData
      ).ToList();

      /// deserialize the children genericaly
      IEnumerable<TSerializeableModel> deserializedModels
        = modelCollectionDatas.Select(serializedChildData
          => (TSerializeableModel)
            (Archetypes.Ids[serializedChildData.sqlData[SQLTypeColumnName] as string]
              .Archetype.MakeI(
                serializedChildData,
                depthLimit - 1
              )));

      // if the child fields are unique and we have depth to descend, recurse.
      /// Loading children recusively only works if the children models are objects, not structs.
      /// For structs to work you may need to use custom [attributes]
      if((deserializedModels?.Any() ?? false)
        && ((!depthLimit.HasValue) || depthLimit.Value > 0)
        && ((isUnique.HasValue && isUnique.Value) ||
          (!isUnique.HasValue && typeof(IUnique).IsAssignableFrom(typeof(TSerializeableModel))))
      ) {
        deserializedModels.ForEach(child =>
           LoadAllChildrenOnto((IUnique)child, depthLimit - 1)
        );
      }

      return deserializedModels.ToList();
    }


    /// <summary>
    /// Deserialize and set a basic field onto a serializeable.
    /// </summary>
    static TSerializeable DeserializeBasicFieldOnto<TSerializeable>(
      TSerializeable model,
      SerializedData serializedModelData,
      MemberInfo fieldInfo,
      ModelDataFieldAttribute fieldSerializationInfo
    ) where TSerializeable : ISerializeable {
      string fieldName = (fieldInfo, fieldSerializationInfo).GetNameKey();
      try {
        // get the serialized data from the right place:
        object serializedFieldData = fieldSerializationInfo.IsASQLField
          ? serializedModelData.sqlData[fieldName]
          : serializedModelData.jsonData.GetValue(fieldName);

        /// advanced deserialization is special, and takes the model itself and edits it:
        // JSON
        if (fieldSerializationInfo.HasAdvancedJSONFieldDeserialization) {
          model = (TSerializeable)fieldSerializationInfo.AdvancedJSONDeserializationFunction((JToken)serializedFieldData, model);
        } // SQL
        else if (fieldSerializationInfo.HasAdvancedSqlFieldDeserialization) {
          /// collect any fully serialized components:
          if (fieldSerializationInfo is ModelDataComponentCollectionFieldAttribute componentCollectionFieldAttribute) {
            (object, List<SerializedData>) serializedComponents 
              = (serializedFieldData, serializedModelData.getChildren(SQLComponentsDataColumnName).ToList());
            serializedFieldData = serializedComponents;
          }
          model = (TSerializeable)fieldSerializationInfo.AdvancedSqlObjectDeserializationFunction(serializedFieldData, model);
        } // if there's simple custom deserialization:
        else {
          object dataToSet = fieldSerializationInfo.HasCustomDeserialization
            // use the specified method of deserialization:
            ? fieldSerializationInfo.DeserializeObject?.Invoke(serializedFieldData) ?? fieldSerializationInfo.DeserializeJSON(serializedFieldData as JToken)
            // if it's just standard json deserialize it, else just take what it gave us
            : serializedFieldData is JToken jsonFieldData
              ? jsonFieldData.ToObject(fieldInfo.DataType())
              : serializedFieldData;

          // set the deserialized data to the field or property
          try {
            if (!fieldInfo.DataType().IsAssignableFrom(dataToSet?.GetType() ?? null) && dataToSet != null) {
              dataToSet = dataToSet.CastTo(fieldInfo.DataType());
            }
            if (fieldInfo is FieldInfo field) {
              field.SetValue(model, dataToSet);
            } else if (fieldInfo is PropertyInfo property) {
              if (property.SetMethod != null) {
                property.SetMethod.Invoke(model, dataToSet.AsArray());
              } else {
                if (!property.CanWrite) {
                  try {
                  // TODO: cache these?
                    property.GetBackingField().SetValue(model, dataToSet);
                  } catch(Exception ex) {
                    throw new NotImplementedException($"Property: {property.Name}, on model type: {model.GetType()}, cannot be deserialized to as it is not a writeable property. Attempted to deserialize to auto-backing field of prop:{property.GetBackingFieldName()}, instead but that also failed.");
                  }
                }
                property.GetSetMethod(true)?.Invoke(model, dataToSet?.AsArray());
              }
            }
          } catch (NotImplementedException ne) {
            throw new NotImplementedException($"Property of Field: {fieldInfo.Name}, on model type: {model.GetType()}, cannot be deserialized to due to errors during the SET attempt:\n{ne}", ne);
          } catch (Exception e) {
            throw new MethodAccessException($"Property of Field: {fieldInfo.Name}, on model type: {model.GetType()}, cannot be deserialized to due to errors during the SET attempt:\n{e}");
          }
        }
      } catch (Exception e) {
        throw new Exception($"Failed to deserialize the field: {fieldInfo.Name}, on the model: {model}, of type: {model.GetType()}.\nINTERNAL ERROR: {e}");
      }

      return model;
    }

    /// <summary>
    /// Helper for deserializing any kind of "child data" field, like images or child models.
    /// </summary>
    static IUnique DeserializeChildFieldOnto(
      IUnique parentModel,
      ICollection<SerializedData> serializedChildModelsDataCollection,
      (MemberInfo objectFieldInfo, ChildModelFieldAttribute dataModelFieldInfo) childModelField,
      int? depthLimit,
      out List<ISerializeable> childModelsForThisFieldType
    ) {
      // if there's nothing to deserialize, skip:
      if(!(serializedChildModelsDataCollection?.Any() ?? false)) {
        childModelsForThisFieldType = null;
        return parentModel;
      }

      if (childModelField.dataModelFieldInfo is ModelImageFieldAttribute imageData) {
        return DeserializeImageFieldOnto(
          parentModel,
          serializedChildModelsDataCollection.FirstOrDefault().sqlData,
          (childModelField.objectFieldInfo, imageData),
          out childModelsForThisFieldType
        );
      } else return DeserializeModelChildFieldOnto(
        parentModel,
        serializedChildModelsDataCollection,
        childModelField,
        depthLimit,
        out childModelsForThisFieldType
      );
    }

    /// <summary>
    /// Helper for deserializing images onto models.
    /// </summary>
    static IUnique DeserializeImageFieldOnto(
      IUnique parentModel,
      Dictionary<string, object> sqlData,
      (MemberInfo objectFieldInfo, ModelImageFieldAttribute imageFieldInfo) childModelField,
      out List<ISerializeable> childModelsForThisFieldType
    ) {
      object deserializedImage;
      childModelsForThisFieldType = new List<ISerializeable>();

      // set the image as the value to set if it exists:
      if (sqlData?[ModelImageFieldAttribute.ImageBytesColumnName] is byte[] imageBytes) {
        deserializedImage = childModelField.imageFieldInfo.DeserializeFromBytes(imageBytes);
      } else return parentModel;

      return SetChildValueOn(parentModel, deserializedImage, childModelField.objectFieldInfo, childModelField.imageFieldInfo);
    }

    /// <summary>
    /// Deserialize a child field made of one or more models onto the parent
    /// </summary>
    static IUnique DeserializeModelChildFieldOnto(
      IUnique parentModel,
      ICollection<SerializedData> serializedChildModelsDataCollection,
      (MemberInfo objectFieldInfo, ChildModelFieldAttribute dataModelFieldInfo) childModelField,
      int? depthLimit,
      out List<ISerializeable> childModelsForThisFieldType
    ) {
      childModelsForThisFieldType = new List<ISerializeable>();
      // as a whole
      if (childModelField.dataModelFieldInfo is ChildModelsCollectionFieldAttribute collection && collection.DeserializeCollectionAsAWholeObject) {
        DeserializeChildModelDataCollectionOnto(
          parentModel,
          childModelField,
          depthLimit,
          childModelsForThisFieldType,
          serializedChildModelsDataCollection
        );
      } else {
        // Normal child model field:
        foreach (SerializedData serializedChildModelData in serializedChildModelsDataCollection) {
          DeserializeChildModelDataOnto(
            parentModel,
            childModelField,
            depthLimit,
            childModelsForThisFieldType,
            serializedChildModelData
          );
        }
      }

      // if this is a multi model child field then set set the whole list, else just grab the first and only item from it:
      object deserializedChildModelDataToSet
        = childModelField.dataModelFieldInfo is ChildModelsCollectionFieldAttribute
          ? (object)childModelsForThisFieldType.ToList()
          : childModelsForThisFieldType.FirstOrDefault();

      return SetChildValueOn(
        parentModel,
        deserializedChildModelDataToSet,
        childModelField.objectFieldInfo,
        childModelField.dataModelFieldInfo
      );
    }

    /// <summary>
    /// Logic for deserializing child models one at a time onto a parent model.
    /// </summary>
    static void DeserializeChildModelDataOnto(
      IUnique parentModel,
      (MemberInfo objectFieldInfo, ChildModelFieldAttribute dataModelFieldInfo) childModelField, 
      int? depthLimit,
      List<ISerializeable> childModelsForThisFieldType, 
      SerializedData serializedChildModelData
    ) {
      DataModelAttribute childFieldModelData = Cache.GetDataModelInfo(childModelField.dataModelFieldInfo.ModelType);
      // if this is an owend model, record the name of the field it's from as well
      if (serializedChildModelData.sqlData.ContainsKey(SQLOwnerColumnName)) {
        serializedChildModelData.sqlData[SQLOwnerModelFieldNameColumnName]
          = childModelField.GetNameKey();
      }
      serializedChildModelData.sqlData.TryGetValue(SQLTypeColumnName, out object typeData);
      string childModelArchetypeId = typeData as string;
      // if it's unique, we need to add it to the uniqle list and recusively get it's children
      if (childModelField.dataModelFieldInfo.IsUnique) {
        string childModelUniqueId = serializedChildModelData.sqlData[SQLIDColumnName] as string;
        ISerializeable deserializedModel = childModelField.dataModelFieldInfo.HasAdvancedChildDeserialization
          ? childModelField.dataModelFieldInfo.ChildModelAdvancedDeserializationFunction(
            childModelUniqueId,
            childModelArchetypeId,
            serializedChildModelData,
            parentModel
          )
          : childModelField.dataModelFieldInfo.SingleChildModelDeserializationFunction(
            childModelUniqueId,
            childModelArchetypeId,
            serializedChildModelData
          );
        childModelsForThisFieldType.Add(deserializedModel);

        /// if we still have depth to descend, get the recusive children of the child model and add them too:
        if (!depthLimit.HasValue || depthLimit.Value > 0) {
          Deserialize(deserializedModel as IUnique, serializedChildModelData, depthLimit - 1);
        }
      } // if it's not unique it's just a simple deserialize:
      else {
        childModelsForThisFieldType.Add(childModelField.dataModelFieldInfo.HasAdvancedChildDeserialization
          ? childModelField.dataModelFieldInfo.ChildModelAdvancedDeserializationFunction(
            "",
            childModelArchetypeId,
            serializedChildModelData,
            parentModel
          )
          : childModelField.dataModelFieldInfo.SingleChildModelDeserializationFunction(
            "",
            childModelArchetypeId,
            serializedChildModelData
          )
        );
      }
    }

    /// <summary>
    /// Logic for deserializing child models one at a time onto a parent model.
    /// </summary>
    static void DeserializeChildModelDataCollectionOnto(
      IUnique parentModel,
      (MemberInfo objectFieldInfo, ChildModelFieldAttribute dataModelFieldInfo) childModelField,
      int? depthLimit,
      List<ISerializeable> childModelsForThisFieldType,
      ICollection<SerializedData> serializedChildModelDatas
    ) {
      DataModelAttribute childFieldModelData = Cache.GetDataModelInfo(childModelField.dataModelFieldInfo.ModelType);
      // if this is an owend model, record the name of the field it's from as well
      serializedChildModelDatas.ForEach(childData => {
        if (childData.sqlData.ContainsKey(SQLOwnerColumnName)) {
          childData.sqlData[SQLOwnerModelFieldNameColumnName]
            = childModelField.GetNameKey();
        }
      });

      ChildModelsCollectionFieldAttribute collectionFieldInfo
        = childModelField.dataModelFieldInfo as ChildModelsCollectionFieldAttribute;

      // if it's unique, we need to add it to the uniqle list and recusively get it's children
      var deserializedModels = collectionFieldInfo.ChildModelsCollectionAdvancedDeserializationFunction(
          serializedChildModelDatas,
          parentModel
        );
      childModelsForThisFieldType.AddRange(deserializedModels.SelectMany(entry => entry.Value));

      /// Auto deserialize children's children recursively
      if (collectionFieldInfo.AutoHandleChildrenOfChildrenCollection
        && childModelField.dataModelFieldInfo.IsUnique
        && (!depthLimit.HasValue || depthLimit.Value > 0)
      ) {
        serializedChildModelDatas.Select(serializedChild
          => deserializedModels[serializedChild.modelUniqueId].First().deserialize(
            serializedChild,
            depthLimit - 1
          ));
      }
    }

      /// <summary>
      /// Set a value on the model
      /// </summary>
      static IUnique SetChildValueOn(IUnique parentModel, object dataToSet, MemberInfo objectFieldInfo, ChildModelFieldAttribute dataModelFieldInfo) {
      // set the deserialized values to their field (if we want to):
      if(!dataModelFieldInfo.SkipSettingValueToFieldAfterDeserialzation) {
        // set the child model to it's field:
        if(objectFieldInfo is FieldInfo field) {
          field.SetValue(parentModel, dataToSet);
        }
        else if(objectFieldInfo is PropertyInfo property) {

          if(!property.CanWrite) {
            try {
              // TODO: cache these?
              property.GetBackingField().SetValue(parentModel, dataToSet);
            } catch {
              throw new NotImplementedException($"Property: {property.Name}, on model type: {parentModel.GetType()}, cannot be deserialized to as it is not a writeable property. Attempted to deserialize to auto-backing field of prop:{property.GetBackingFieldName()}, instead but that also failed.");
            }
          } else {
            try {
              property.SetMethod.Invoke(parentModel, dataToSet?.AsArray());
            }
            catch(System.ArgumentException e) {
              if(property.CanWrite
                && typeof(IEnumerable).IsAssignableFrom(property.DataType())
                && dataToSet is IEnumerable enumerableData
                && enumerableData.Cast<object>().Any()
              ) {
                // TODO: cache this:
                object enumValues = typeof(Enumerable)
                  .GetMethod("Cast")
                  .MakeGenericMethod(dataModelFieldInfo.ModelType)
                  .Invoke(null, new object[] { enumerableData });
                property.SetMethod.Invoke(parentModel, enumValues.AsArray());
              }
            }
          }
        }
      }

      return parentModel;
    }

    #endregion

    #region Exceptions

    /// <summary>
    /// This means a model's class or base class is missing a DataModelAttribute to show how to serialize it.
    /// </summary>
    public class ModelMissingDataModelAttributeException : NotImplementedException {
      public ModelMissingDataModelAttributeException(string message) : base(message) {}
    }

    #endregion

    #region TESTING

    /// <summary>
    /// do something on each model type currently loaded
    /// </summary>
    static void ForEachLoadedModelType(Action<Type, DataModelAttribute, List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>> action) {
      foreach (Type type in AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => assembly.GetTypes()).Where(type => !type.IsAbstract)) {
        if (type.GetCustomAttributes(typeof(DataModelAttribute), true).FirstOrDefault() is DataModelAttribute dataModelInfo) {
          action(type, dataModelInfo, Cache.GetFieldsInfo(type));
        }
      }
    }

    /// <summary>
    /// Run a test for each model type
    /// </summary>
    public static void RunModelTests() =>
      ForEachLoadedModelType((type, dataModelInfo, _) => {
        try {
          Debugger.Log($"TEST: Start | SerializationAndDeserialization test for {type.FullName}");
          if (true/*dataModelInfo.test_SerializationAndDeserialization(type)*/) {
            Debugger.Log($"TEST: Passed | SerializationAndDeserialization test for {type.FullName}");
          } else {
            Debugger.LogWarning($"TEST: Skipped | SerializationAndDeserialization test for {type.FullName}");
          }
        } catch (Exception e) {
          Debugger.LogError($"TEST: Failed | SerializationAndDeserialization test for {type.FullName}\nExeption was thrown\n{e.StackTrace}");
          throw e;
        }
      });

    #endregion

  }

  public static class DataFieldExtensions {
    public static string GetNameKey(this (MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo) fieldData)
      => fieldData.dataModelFieldInfo.Name ?? fieldData.objectFieldInfo.Name.Split(".").Last();
  }
}
