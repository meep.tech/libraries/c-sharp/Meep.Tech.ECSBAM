﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Serialization;

namespace Meep.Tech.Data.Base {

  /// <summary>
  /// Represents a unique serializeable data item with mutable data
  /// </summary>
  public interface IOwned : ISerializeable {

    /// <summary>
    /// If the owner of this owned serializable is needed to "Make" a new one.
    /// </summary>
    bool OwnerIsRequiredOnCreation
      => true;

    /// <summary>
    /// The unique id of the owner of this item
    /// </summary>
    abstract string owner {
      get;
      protected internal set;
    }
  }
}

public static class IOwnedFunctions {

  /// <summary>
  /// Helper for setting the owner
  /// </summary>
  internal static void setOwner(this IOwned owned, string ownerId) {
    owned.owner = ownerId;
  }

  /// <summary>
  /// Helper for getting the owner from inherted class
  /// </summary>
  public static string getOwnerId(this IOwned owned)
    => owned.owner;
}
