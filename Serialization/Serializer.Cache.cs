﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Data;
using Meep.Tech.Data.Attributes.Models;
using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Attributes.Components;

namespace Meep.Tech.Data.Serialization {

  public partial class Serializer {

    /// <summary>
    /// Cache for storing data
    /// </summary>
    public static class Cache {

      /// <summary>
      /// Unique id property get helper
      /// </summary>
      static readonly PropertyInfo UniqueIdPropertyInfo
        = typeof(IUnique).GetProperty("id");

      /// <summary>
      /// Just get property helper for owner ids
      /// </summary>
      static readonly PropertyInfo OwnerIdPropertyInfo
        = typeof(IOwned).GetProperty("owner");

      /// <summary>
      /// Cached model info
      /// </summary>
      static readonly Dictionary<string, string> TableNamesByModelType
        = new Dictionary<string, string>();

      /// <summary>
      /// Cached model info
      /// </summary>
      static readonly Dictionary<string, DataModelAttribute> DataModelTablesByName
        = new Dictionary<string, DataModelAttribute>();

      /// <summary>
      /// Cached model field info
      /// </summary>
      static readonly Dictionary<string, List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>> DataModelFieldsInfoByClassName
        = new Dictionary<string, List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>>();

      /// <summary>
      /// Cached model field info
      /// </summary>
      static readonly Dictionary<string, List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>> SqlFieldsForTablesInfo
        = new Dictionary<string, List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>>();

      /// <summary>
      /// Get the data model info
      /// </summary>
      /// <param name="dataModel"></param>
      /// <returns></returns>
      public static DataModelAttribute GetDataModelInfo(object dataModel)
        => GetDataModelInfo(dataModel.GetType());

      /// <summary>
      /// Get table name for the model class
      /// </summary>
      /// <param name="dataModel"></param>
      /// <returns></returns>
      public static string GetTableNameForModelType(Type systemType)
        => TableNamesByModelType.TryGetValue(systemType.FullName, out var value)
          ? value
          : throw new ModelMissingDataModelAttributeException($"Model of system type: {systemType}, does not have any data loaded into TableNamesByModelType.");

      /// <summary>
      /// Get table name for the model class
      /// </summary>
      /// <param name="dataModel"></param>
      /// <returns></returns>
      public static string GetBasicModelTypeForTable(string tableName)
        => TableNamesByModelType.First(entry => entry.Value == tableName).Value;

      /// <summary>
      /// Get the sql table rows for a given table:
      /// </summary>
      public static IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> GetSqlFieldsForTable(string tableName) {
        if (SqlFieldsForTablesInfo.TryGetValue(tableName, out var sqlColumnsInfo)) {
          return sqlColumnsInfo;
        } else throw new Exception($"SQL column data was never cached!");
      }

      /// <summary>
      /// Get the data model info
      /// </summary>
      /// <param name="dataModel"></param>
      /// <returns></returns>
      public static DataModelAttribute GetDataModelInfo(Type dataModelType) {
        DataModelAttribute tableData =
          TableNamesByModelType.TryGetValue(dataModelType.FullName, out string existingTableName)
            ? DataModelTablesByName[existingTableName]
            : null;

        if (tableData != null) {
          return tableData;
        }

        tableData = (DataModelAttribute)Attribute.GetCustomAttribute(dataModelType, typeof(DataModelAttribute));
        if (tableData == null) {
          if (typeof(IComponent).IsAssignableFrom(dataModelType)) {
            return null;
          }
          throw new ModelMissingDataModelAttributeException($"System.Type {dataModelType} does not impliment the attribute: DataModelAttribte, needed to serialize it and give it a table name.");
        }

        if (typeof(IFullySerializeableComponent).IsAssignableFrom(dataModelType) && !(tableData is ComponentDataModelAttribute)) {
          throw new Exception($"Components must use the ComponentDataModelAttribute for full serialization");
        }

        // if it's a link, find what it's linking too:
        if (tableData is DataModelWithExistingTableAttribute) {
          if (DataModelTablesByName.TryGetValue(tableData.TableName, out DataModelAttribute existingTableData)) {
            tableData = existingTableData;
          } else throw new Exception($"Could not find table data for table: {tableData.TableName}. It may not have been initialized yet.");
        } // if it's a defenition, make sure it's not a different re-defenition and store it: 
        else {
          /// Validate this table is only defined once, and in the same way everywhere:
          if (DataModelTablesByName.TryGetValue(tableData.TableName, out DataModelAttribute existingTableDefenition)) {
            if (!existingTableDefenition.Equals(tableData)) {
              throw new Exception($"Table with name {tableData.TableName} is already defined with a differing defenition elsewhere. Cannot set it for type {dataModelType.FullName} with the new defenition provided.");
            }
          } else {
            DataModelTablesByName.Add(tableData.TableName, tableData);
          }
        }

        TableNamesByModelType[dataModelType.FullName] = tableData.TableName;

        return tableData;
      }

      /// <summary>
      /// Get the data for the fields that we should serialize
      /// </summary>
      public static List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> GetFieldsInfo(Type modelType) {
        List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> modelDataFields
          = DataModelFieldsInfoByClassName.TryGetValue(modelType.FullName, out var cachedDataFieldsInfo)
            ? cachedDataFieldsInfo
            : null;
        List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFields;

        // if we have no fields at all, or we need the children fields and we're missing them:
        if (modelDataFields == null) {
          HashSet<string> fieldNames = new HashSet<string>();
          bool isComponent = typeof(IFullySerializeableComponent).IsAssignableFrom(modelType);
          BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
          List<MemberInfo> fields = modelType.GetFields(bindingFlags).Cast<MemberInfo>()
            .Concat(modelType.GetProperties(bindingFlags)).ToList();

          // make new list(s)
          modelDataFields
            = new List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>();
          sqlFields
            = new List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>();


          /// if we have no data, fetch it:
          // validation:
          int idFieldCount = 0;
          int typeFieldCount = 0;
          int ownerFieldCount = 0;
          foreach (MemberInfo field in fields) {
            if (field.GetCustomAttribute<ModelDataFieldAttribute>(true) is ModelDataFieldAttribute dataModelFieldInfo
              && field.GetCustomAttribute<IgnoreInheritedModelDataField>(true) == null
            ) {
              if (isComponent && !(dataModelFieldInfo is IComponentModelDataAttribute)) {
                throw new Exception($"field :{field.Name} is not annotated with an attribute of type IComponentModelDataAttribute, but the containing class is a component requesting full serialization. Components can only use the Attributes that extend IComponentModelDataAttribute");
              }

              if (dataModelFieldInfo is ChildModelFieldAttribute && !typeof(IUnique).IsAssignableFrom(modelType)) {
                throw new NotImplementedException($"Child models cannot be on non-IUnique parents");
              }

              string fieldName = (field, dataModelFieldInfo).GetNameKey();
              if (fieldNames.Contains(fieldName)) {
                throw new Exception($"Model of type {modelType.FullName} has two fields with the name {fieldName} that are labeled for serialization. Please rename one of them.");
              }

              // validation:
              if (dataModelFieldInfo is ModelIdFieldAttribute) {
                idFieldCount++;
              }
              if (dataModelFieldInfo is ModelTypeFieldAttribute) {
                typeFieldCount++;
              }
              if (dataModelFieldInfo is ModelOwnerFieldAttribute) {
                ownerFieldCount++;
              }

              modelDataFields.Add((field, dataModelFieldInfo));
              fieldNames.Add(fieldName);
            }
          }

          // add all extra sql fields, so they get validated for table creations:
          foreach ((string columnName, Type columnType, Type attributeType) in GetDataModelInfo(modelType).ExtraSqlColumns) {
            if (fieldNames.Contains(columnName)) {
              continue;
            }
            // validation:
            if (columnName == SQLIDColumnName) {
              idFieldCount++;
            }
            if (columnName == SQLTypeColumnName) {
              typeFieldCount++;
            }
            if (columnName == SQLOwnerColumnName) {
              ownerFieldCount++;
            }
            modelDataFields.Add((
              null,
              attributeType != null
                ? (ModelDataFieldAttribute)Activator.CreateInstance(attributeType)
                : new ModelDataFieldAttribute(
                  columnName,
                  CustomSerializeToType: columnType,
                  IsASQLField: true,
                  SkipDuringDeserialization: true
                )
            ));
          }

          if (typeof(IUnique).IsAssignableFrom(modelType) && idFieldCount == 0) {
            idFieldCount++;
            modelDataFields.Add((UniqueIdPropertyInfo, new ModelIdFieldAttribute()));
          }

          if (typeof(IOwned).IsAssignableFrom(modelType) && ownerFieldCount == 0) {
            ownerFieldCount++;
            modelDataFields.Add((OwnerIdPropertyInfo, new ModelOwnerFieldAttribute()));
          }

          // if this has an owner field, then add the owner row name field field to it's sql data
          if (modelDataFields.Any(item => item.dataModelFieldInfo.Name == SQLOwnerColumnName)) {
            modelDataFields.Add((
              null,
              new ModelDataFieldAttribute(
                SQLOwnerModelFieldNameColumnName,
                CustomSerializeToType: typeof(string),
                IsASQLField: true,
                SkipDuringDeserialization: true
              )
            ));
          }

          ///validation:
          // there must be exactly one id field per model
          if (idFieldCount > 1) {
            throw new MissingFieldException($"Model of type {modelType} cannot be serialized. In order to be used with the ModelJSONSerializer, a model's class must impliment the ModelIdFieldAttribute on only a single field or property within in itself or it's inheritance tree.");
          }
          // there must be exactly one owner field per model
          if (typeFieldCount > 1) {
            throw new MissingFieldException($"Model of type {modelType}, cannot be serialized. In order to be used with the ModelJSONSerializer, a model's class must impliment the ModelTypeFieldAttribute on only a single field or property within in itself or it's inheritance tree.");
          }
          // there must be exactly one type field per model
          if (typeFieldCount > 1) {
            throw new MissingFieldException($"Model of type {modelType}, cannot be serialized. In order to be used with the ModelJSONSerializer, a model's class must impliment the ModelOwnerFieldAttribute on only a single field or property within in itself or it's inheritance tree.");
          }

          sqlFields = modelDataFields.Where(field => field.dataModelFieldInfo.IsASQLField).ToList();

          // get any existing table fields for validation:
          string tableName = GetDataModelInfo(modelType).TableName;
          List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> existingSqlDataFieldsForTable
            = SqlFieldsForTablesInfo.TryGetValue(tableName, out List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> cachedTableDataFieldsInfo)
              ? cachedTableDataFieldsInfo
              : null;

          // if a table of this type has already defined fields, make sure the new defenitions are compatable with those.
          if (existingSqlDataFieldsForTable != null) {
            foreach ((MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo) newSqlFieldInfo in sqlFields) {
              (MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)? matchingField;
              if ((matchingField = existingSqlDataFieldsForTable.Find(field => field.dataModelFieldInfo.IsASQLField && field.GetNameKey() == newSqlFieldInfo.GetNameKey())).HasValue) {
                if (!matchingField.Value.dataModelFieldInfo.Equals(newSqlFieldInfo.dataModelFieldInfo)) {
                  throw new Exception($"Existing table defenition of fields for table {tableName} already contains a field named: {newSqlFieldInfo.GetNameKey()} with a non matching defention to the one declated by type: {modelType.FullName}. Are you trying to add a new sql field to a child type? This usually is not possible.");
                }
              } else {
                throw new Exception($"Existing table defenition of fields for table {tableName} does not contain field: {newSqlFieldInfo.GetNameKey()} as reqested by type {modelType.FullName}. Are you trying to add a new sql field to a child type? This usually is not possible.");
              }
            }
          }

          // update the caches and return
          SqlFieldsForTablesInfo[tableName] = sqlFields;
        }

        return (DataModelFieldsInfoByClassName[modelType.FullName] = modelDataFields);
      }

      /// <summary>
      /// Shortcut
      /// </summary>
      public static List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> GetFieldsInfo(object dataModel)
        => GetFieldsInfo(dataModel.GetType());
    }
  }
}
