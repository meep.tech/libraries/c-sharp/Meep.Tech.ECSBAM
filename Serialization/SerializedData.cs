﻿using Meep.Tech.Data.Attributes.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.Data.Serialization {

  public partial class Serializer {

    /**
     * Data structire for a serialized model's data, or for deleting models
     */
    public struct SerializedData {
      private const string IdDataKey = "_Id__";
      private const string TypeDataKey = "_type__";
      private const string SqlDataKey = "_sql__";
      private const string JsonDataKey = "_json__";
      private const string DataStringKey = "_data__";
      private const string LinkDataKey = "_links__";
      private const string ChildrenDataKey = "_children__";

      /// <summary>
      /// If this serialized data item actually signifies that this is for deleting a bunch of child models before serializing the new ones
      /// </summary>
      public readonly bool isForChildDeletion;

      /// <summary>
      /// If this item is a fully serialized component.
      /// </summary>
      public readonly bool isFullySerializedComponent;

      /// <summary>
      /// If this item was serialized as a linked model.
      /// </summary>
      public bool isLinkedToParent {
        get;
        internal set;
      }

      /// <summary>
      /// Table for this model
      /// </summary>
      public readonly string tableName;

      /// <summary>
      /// Serialized SQL row data
      /// </summary>
      public Dictionary<string, object> sqlData {
        get;
        private set;
      }

      /// <summary>
      /// Serialized Json Data 
      /// </summary>
      public JObject jsonData {
        get;
        private set;
      }

      /// <summary>
      /// Text representation of the serialized item
      /// </summary>
      public string dataString {
        get;
      }

      /// <summary>
      /// Child datas by tablename, then by it's id.
      /// </summary>
      public Dictionary<string, Dictionary<string, ICollection<SerializedData>>> childDatas {
        get;
        private set;
      }

      /// <summary>
      /// Child datas by the parent table name:
      /// </summary>
      public Dictionary<string, ICollection<(string tableName, string id)>> childDataInfosByParentField {
        get;
        private set;
      }

      /// <summary>
      /// Linked model ids by tablename
      /// </summary>
      public Dictionary<string, ICollection<string>> linkedDatas {
        get;
        private set;
      }

      /// <summary>
      /// Linked data ids by the parent table field :
      /// </summary>
      public Dictionary<string, ICollection<(string tableName, string id)>> linkedDataInfosByParentField {
        get;
        private set;
      }

      /// <summary>
      /// Model unique id cached.
      /// </summary>
      public string modelUniqueId {
        get => _id ??= GetModelId(this);
      } string _id;

      /// <summary>
      /// Make empty.
      /// </summary>
      public SerializedData(string tableName, bool isFullySerializedComponent = false) : this(tableName, null) {
        this.isFullySerializedComponent = isFullySerializedComponent;
      }

      /// <summary>
      /// Make a serialized data item based on a model to save to the DB in a row
      /// </summary>
      public SerializedData(
        string tableName, 
        Dictionary<string, object> sqlColumnsData,
        JObject jsonData,
        Dictionary<string, ICollection<(string tableName, string id)>> links = null
      ) {
        _id = null;
        this.tableName = tableName;
        sqlData = sqlColumnsData;
        this.jsonData = jsonData;
        isForChildDeletion = false;
        dataString = null;
        childDatas = null;
        childDataInfosByParentField = null;
        isFullySerializedComponent = false;
        isLinkedToParent = false;
        linkedDatas = null;
        linkedDataInfosByParentField = links;
      }

      /// <summary>
      /// Make a serialized data item based on a model compressed to a single string
      /// </summary>
      public SerializedData(string tableName, string dataString) {
        _id = null;
        this.tableName = tableName;
        sqlData = null;
        jsonData = null;
        childDatas = null;
        childDataInfosByParentField = null;
        isForChildDeletion = false;
        this.dataString = dataString;
        isFullySerializedComponent = false;
        isLinkedToParent = false;
        linkedDatas = null;
        linkedDataInfosByParentField = null;
      }

      /// <summary>
      /// Make a serialized data item that tells the system to delete rows based on an owenr id and child model type
      /// </summary>
      SerializedData(string ownerId, string childTableName, string childFieldName) {
        _id = null;
        tableName = childTableName;
        sqlData = new Dictionary<string, object> {
          {SQLOwnerColumnName, ownerId },
          {SQLOwnerModelFieldNameColumnName, childFieldName }
        };
        jsonData = null;
        isForChildDeletion = true;
        dataString = DeleteDataKey;
        childDatas = null;
        childDataInfosByParentField = null;
        isFullySerializedComponent = false;
        isLinkedToParent = false;
        linkedDatas = null;
        linkedDataInfosByParentField = null;
      }

      /// <summary>
      /// Named usage of the above ctor
      /// </summary>
      internal static SerializedData MakeDeletionEntry(string ownerId, string childTableName, string childFieldName) {
        return new SerializedData(ownerId, childTableName, childFieldName);
      }

      /// <summary>
      /// Make for a serialized component string.
      /// </summary>
      public static SerializedData MakeForComponent(string modelData) {
        return new SerializedData(ComponentKeyNameAndTablePlaceholder, modelData);
      }

      public static SerializedData MakeForImage(Dictionary<string, object> serializedImageData)
        => new SerializedData(ModelImageFieldAttribute.ImageTableName, serializedImageData, null);

      /// <summary>
      /// Add a linked data item
      /// </summary>
      public void addLink(string fieldOnParentModel, string linkedModelTableName, string modelId, SerializedData? linkModelData = null) {
        /// add the data as a child first if we have full data:
        if (linkModelData.HasValue) {
          SerializedData data = linkModelData.Value;
          data.isLinkedToParent = true;
          addChild(fieldOnParentModel, data, modelId);
        }

        /// then add it to the linked data lists
        if (linkedDatas == null) {
          linkedDatas = new Dictionary<string, ICollection<string>>();
          linkedDataInfosByParentField = new Dictionary<string, ICollection<(string tableName, string id)>>();
        }

        if (!linkedDatas.ContainsKey(linkedModelTableName)) {
          linkedDatas[linkedModelTableName]
            = new List<string>();
        }

        linkedDatas.AddToValueCollection(
          linkedModelTableName,
          modelId
        );
        linkedDataInfosByParentField.AddToValueCollection(
          fieldOnParentModel,
          (linkedModelTableName, modelId)
        );

      }


      /// <summary>
      /// Add a child serialized data item.
      /// </summary>
      public void addChild(string fieldOnParentModel, SerializedData childData, string uniqueChildIdOverride = null) {
        string childTableName = childData.isFullySerializedComponent ? ComponentKeyNameAndTablePlaceholder : childData.tableName;
        if (childDatas == null) {
          childDatas = new Dictionary<string, Dictionary<string, ICollection<SerializedData>>>();
          childDataInfosByParentField = new Dictionary<string, ICollection<(string tableName, string id)>>();
        }
        if (!childDatas.ContainsKey(childTableName)) {
          childDatas[childTableName]
            = new Dictionary<string, ICollection<SerializedData>>();
        }
        childData.setParentModelFieldIfEmpty(fieldOnParentModel);
        childDatas[childTableName].AddToValueCollection(
          uniqueChildIdOverride ?? childData.modelUniqueId,
          childData
        );
        childDataInfosByParentField.AddToValueCollection(
          fieldOnParentModel,
          (childTableName, childData.modelUniqueId)
        );
      }

      /// <summary>
      /// Get all children for a given field.
      /// </summary>
      public ICollection<SerializedData> getChildren(string forFieldOnParent) {
        ICollection<(string tableName, string id)> existingChildDatas = null;
        if (childDataInfosByParentField?.TryGetValue(forFieldOnParent, out existingChildDatas) ?? false) {
          List<SerializedData> fetchedDatas = new List<SerializedData>();
          HashSet<string> fetchedDataIds = new HashSet<string>();
          foreach (var data in existingChildDatas) {
            if (!fetchedDataIds.Contains(data.id)) {
              fetchedDataIds.Add(data.id);
              fetchedDatas.AddRange(childDatas[data.tableName][data.id]);
            }
          }

          return fetchedDatas;
        }

        return Array.Empty<SerializedData>();
      }

      /// <summary>
      /// Helper to set unset data.
      /// </summary>
      public void setData(Dictionary<string, object> sqlColumnsData = null, JObject jsonData = null) {
        if (sqlData == null) {
          sqlData = sqlColumnsData;
        } else if (sqlColumnsData != null) {
          throw new System.Exception("Cannot set data unless it's null for sqlData.");
        }
        if (this.jsonData == null) {
          this.jsonData = jsonData;
        } else if (jsonData != null) {
          throw new System.Exception("Cannot set data unless it's null for jsonData.");
        }
      }

      /// <summary>
      /// Get all the children as a flat collection of serialized datas by table, then by id.
      /// </summary>
      public Dictionary<string, Dictionary<string, ICollection<SerializedData>>> getAllChildrenRecursively() {
        if (childDatas != null) {
          Dictionary<string, Dictionary<string, ICollection<SerializedData>>> allChildren
            = new Dictionary<string, Dictionary<string, ICollection<SerializedData>>>();
          foreach ((string tablename, var tableDatas) in childDatas) {
            foreach ((string childId, ICollection<SerializedData> datasForId) in tableDatas) {
              foreach(SerializedData data in datasForId) {
                Dictionary<string, Dictionary<string, ICollection<SerializedData>>> recursiveChildren
                  = data.getAllChildrenRecursively();
                if (recursiveChildren != null) {
                  allChildren.MergeRecursively(recursiveChildren);
                }
                if (allChildren.TryGetValue(tablename, out var existingTableDatas)) {
                  existingTableDatas.AddToValueCollection(childId, data);
                } else {
                  allChildren.Add(
                    tablename,
                    new Dictionary<string, ICollection<SerializedData>> {
                      {childId, data.AsEnumerable().ToList() }
                    }
                  );
                }
              }
            }
          }

          return allChildren;
        } else {
          return null;
        }
      }

      /// <summary>
      /// Just this model's data packaged into a dictionary for the saving functionality.
      /// </summary>
      public Dictionary<string, Dictionary<string, ICollection<SerializedData>>> asSaveableCollection()
        => new Dictionary<string, Dictionary<string, ICollection<SerializedData>>> {{
            tableName,
            new Dictionary<string, ICollection<SerializedData>> {
              {modelUniqueId, this.AsEnumerable().ToList() }
            }
          }};

      /// <summary>
      /// Set the parent model field if it's empty:
      /// </summary>
      internal SerializedData setParentModelFieldIfEmpty(string parentModelFieldName) {
        if (sqlData.TryGetValue(SQLOwnerModelFieldNameColumnName, out object value)) {
          sqlData[SQLOwnerModelFieldNameColumnName] = string.IsNullOrEmpty(value as string)
            ? parentModelFieldName
            : value;
        } else {
          sqlData[SQLOwnerModelFieldNameColumnName] = parentModelFieldName;
        }

        return this;
      }

      internal void Deconstruct(out Dictionary<string, object> sqlColumnsData, out JObject jsonData) {
        sqlColumnsData = sqlData;
        jsonData = this.jsonData;
      }

      internal void Deconstruct(out string tableName, out Dictionary<string, object> sqlColumnsData, out JObject jsonData, out bool isForChildDeletion) {
        tableName = this.tableName;
        sqlColumnsData = sqlData;
        jsonData = this.jsonData;
        isForChildDeletion = this.isForChildDeletion;
      }

      internal void Deconstruct(out string tableName, out Dictionary<string, object> sqlColumnsData, out JObject jsonData) {
        tableName = this.tableName;
        sqlColumnsData = sqlData;
        jsonData = this.jsonData;
      }

      public static implicit operator SerializedData((string tableName, Dictionary<string, object> sqlParams, JObject jSONData) data) {
        return new SerializedData(data.tableName, data.sqlParams, data.jSONData);
      }

      /// <summary>
      /// Convert this model's data to a serialized state.
      /// </summary>
      public string toJson() {
        if (tableName != null) {
          return toJObject().ToString();
        }

        return "{}";
      }

      /// <summary>
      /// Convert this model's data to a serialized state.
      /// </summary>
      public JObject toJObject() {
        if (tableName != null) {
          if (dataString != null 
            || (!sqlData.Values.Any() && !jsonData.HasValues)
          ) {
            return JObject.FromObject(new Dictionary<string, object> {
              {TypeDataKey, tableName},
              {DataStringKey, dataString ?? ""},
            });
          }

          Dictionary<string, Dictionary<string, ICollection<SerializedData>>> childDatasByTableAndId
            = null;
          if (childDataInfosByParentField != null && childDataInfosByParentField.Any()) {
            childDatasByTableAndId = childDatas;
          }

          return JObject.FromObject(new Dictionary<string, object> {
            {TypeDataKey, tableName},
            {SqlDataKey, sqlData},
            {JsonDataKey, jsonData},
            {LinkDataKey, linkedDataInfosByParentField?.ToDictionary(
              linkedInfosByFieldName => linkedInfosByFieldName.Key,
              linkedInfosByFieldName => linkedInfosByFieldName.Value.Select(linkedInfo => new {
                __type_ = linkedInfo.tableName,
                __id_ = linkedInfo.id
              })
            )},
            {ChildrenDataKey, childDataInfosByParentField?.ToDictionary(
              childDatasByFieldName => childDatasByFieldName.Key,
              childDatasByFieldName => childDatasByFieldName.Value.Select(childData => new {
                __type_ = childData.tableName,
                __data_ = childDatasByTableAndId[childData.tableName][childData.id].Select(child => child.toJObject())
              })
            )}
          });
        }

        return new JObject();
      }

      /// <summary>
      /// Save this, and all children
      /// </summary>
      public void save() {
        Settings.Storage.SaveDatasForModelsLogic(getAllChildrenRecursively()?
          .Merge(asSaveableCollection())
        ?? asSaveableCollection());
      }

      /// <summary>
      /// Get models from a serialized string of data:
      /// </summary>
      public static SerializedData FromJson(string jsonString) {
        if (string.IsNullOrWhiteSpace(jsonString) || jsonString == "{}") {
          return default;
        }

        JObject item = JObject.Parse(jsonString);
        if (item.HasValues) {
          if (item.ContainsKey(TypeDataKey)) {
            if (item.TryGetValue(DataStringKey, out JToken value)) {
              return new SerializedData(
                item.Value<string>(TypeDataKey),
                value.ToString()
              );
            }
            var data = new SerializedData(
              item.TryGetValue<string>(TypeDataKey),
              item.TryGetValue<Dictionary<string, object>>(SqlDataKey),
              item.GetValue(JsonDataKey) as JObject,
              item.TryGetValue<Dictionary<string, ICollection<(string tableName, string id)>>>(LinkDataKey)
            );
            // TODO: deserialize provided children:

            return data;
          } else return new SerializedData();
        }

        return default;
      }

      /// <summary>
      /// Get a model Id from a set of serialized data
      /// </summary>
      /// <param name="serializedData"></param>
      /// <returns></returns>
      public static string GetModelId(SerializedData serializedData) {
        if (serializedData.sqlData == null) {
          return NoIdDataKey;
        }
        if (serializedData.tableName == ComponentKeyNameAndTablePlaceholder) {
          return serializedData.sqlData[SQLTypeColumnName] as string;
        } else {
          return (string)(serializedData.sqlData.TryGetValue(SQLIDColumnName, out object foundID) ? foundID : NoIdDataKey);
        }
      }
    }
  }
}
