﻿using Meep.Tech.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.Data {
  #region Append and Replacement Extension Helpers

  /// <summary>
  /// Helper extensions for chaining
  /// </summary>
  public static class DictionaryExtensions {

    /// <summary>
    /// Append an item to the dictionary and chain
    /// </summary>
    public static OrderdDictionary<Tkey, TValue> Append<Tkey, TValue>(this OrderdDictionary<Tkey, TValue> current, Tkey key, TValue value) {
      current.Add(key, value);
      return current;
    }

    /// <summary>
    /// Append an item to the dictionary and chain
    /// </summary>
    public static OrderdDictionary<Tkey, TValue> Append<Tkey, TValue>(
      this OrderdDictionary<Tkey, TValue> current,
      TValue value
    ) where Tkey : Component.Type.Id
      where TValue : IModel
    {
      current.Add(value.type.Id as Tkey, value);
      return current;
    }

    /// <summary>
    /// remove an item from the dictionary and chain
    /// </summary>
    public static OrderdDictionary<Tkey, TValue> Without<Tkey, TValue>(this OrderdDictionary<Tkey, TValue> current, Tkey key) {
      current.Remove(key);
      return current;
    }

    /// <summary>
    /// replace an item in the dictionary and return the result.
    /// </summary>
    public static OrderdDictionary<Tkey, TValue> Replace<Tkey, TValue>(this OrderdDictionary<Tkey, TValue> current, Tkey key, TValue value) {
      current.Remove(key);
      current.Add(key, value);

      return current;
    }

    /// <summary>
    /// update an existing item in the dictionary.
    /// </summary>
    public static OrderdDictionary<Tkey, TValue> Update<Tkey, TValue, TComponent>(
      this OrderdDictionary<Tkey, TValue> current,
      Tkey componentArchetypeId,
      Func<TComponent, TComponent> update
    ) 
      where Tkey : Component.Type.Id
      where TValue : IModel
      where TComponent : TValue
    {
      TValue currentComponent = current[componentArchetypeId];
      current[componentArchetypeId] = update((TComponent)currentComponent);

      return current;
    }

    /// <summary>
    /// Merge one or multple dictionaries together, overriding any values with the same key. Returns result for chaining
    /// </summary>
    public static OrderdDictionary<TKey, TValue> Merge<TKey, TValue>(this OrderdDictionary<TKey, TValue> baseDict, params IEnumerable<KeyValuePair<TKey, TValue>>[] collectionsToMerge) {
      if (collectionsToMerge == null) return baseDict;
      OrderdDictionary<TKey, TValue> result = baseDict;
      foreach (IEnumerable<KeyValuePair<TKey, TValue>> dictionary in collectionsToMerge) {
        foreach (KeyValuePair<TKey, TValue> entry in dictionary) {
          result[entry.Key] = entry.Value;
        }
      }

      return result;
    }

    /// <summary>
    /// Try to add all the values to the given dictionary, throwing an error if any exist already. Returns result for chaining
    /// </summary>
    public static OrderdDictionary<TKey, TValue> AddAll<TKey, TValue>(this OrderdDictionary<TKey, TValue> baseDict, params IEnumerable<KeyValuePair<TKey, TValue>>[] collectionsToAdd) {
      if (collectionsToAdd == null) return baseDict;
      OrderdDictionary<TKey, TValue> result = baseDict;
      foreach (IEnumerable<KeyValuePair<TKey, TValue>> dictionary in collectionsToAdd) {
        foreach (KeyValuePair<TKey, TValue> entry in dictionary) {
          result.Add(entry.Key, entry.Value);
        }
      }

      return result;
    }

    /// <summary>
    /// Remove all items that match:
    /// </summary>
    public static OrderdDictionary<TKey, TValue> RemoveAll<TKey, TValue>(this OrderdDictionary<TKey, TValue> current, Func<TKey, bool> shouldRemoveEntry) {
      foreach (TKey key in current.Keys.Reverse()) {
        if (shouldRemoveEntry(key)) {
          current.Remove(key);
        }
      }

      return current;
    }

    /// <summary>
    /// Remove all items that match:
    /// </summary>
    public static OrderdDictionary<TKey, TValue> RemoveAll<TKey, TValue>(this OrderdDictionary<TKey, TValue> current, Func<TValue, bool> shouldRemoveEntry) {
      foreach (TKey key in current.Keys.Reverse()) {
        if (shouldRemoveEntry(current[key])) {
          current.Remove(key);
        }
      }

      return current;
    }

    /// <summary>
    /// Remove all items that match:
    /// </summary>
    public static OrderdDictionary<TKey, TValue> RemoveAll<TKey, TValue>(this OrderdDictionary<TKey, TValue> current, Func<TKey, TValue, bool> shouldRemoveEntry) {
      foreach (TKey key in current.Keys.Reverse()) {
        if (shouldRemoveEntry(key, current[key])) {
          current.Remove(key);
        }
      }

      return current;
    }
  }

  #endregion
}
