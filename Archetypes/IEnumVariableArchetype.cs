﻿using System;

namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// Represents an Archetype that varies based on a specific enumeration
  /// This will register one Item.Type for each enumeration value
  /// </summary>
  /// <typeparam name="TVariableBase">The ID or Archetype to enumerate this archetype and make sub archetypes based on:</typeparam>
  public interface IEnumVariableArchetype<TVariableBase> : IEnumVariableType where TVariableBase : IArchetypeId, IEnumVariableSubType {

    /// <summary>
    /// The type of enum used
    /// This needs to be copy pasted into base types
    /// </summary>
    Type IEnumVariableType.VariationEnumType
      => typeof(TVariableBase);
  }

  /// <summary>
  /// Represents an EnumerationType that varies based on a specific enumeration.
  /// Use the above generic type<> to actually enable functionality in the mappers
  /// </summary>
  public interface IEnumVariableType {

    /// <summary>
    /// The id of the enumeration value this variant represents.
    /// This may change between runtimes!!!!
    /// </summary>
    int VariationEnumId {
      get;
    }

    /// <summary>
    /// The type of enum used
    /// </summary>
    public abstract Type VariationEnumType {
      get;
    }

    /// <summary>
    /// The Base enum id value. Needs to be overriden via attribute
    /// </summary>
    public static IArchetypeId VariableBaseId 
      => null;
  }

  // Represents a value for the variable enum type one of these variable enum, enum types.
  public interface IEnumVariableSubType {

    /// <summary>
    /// If this is excluded from type creation for the super-enum type(IEnumVariableType<EnumerationType>) for any reason.
    /// </summary>
    public bool IsExcluded
      => false;
  }
}