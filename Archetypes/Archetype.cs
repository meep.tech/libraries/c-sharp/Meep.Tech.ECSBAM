﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Loading;
using Meep.Tech.Data.Serialization;
using Meep.Tech.Data.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using static Meep.Tech.Data.Serialization.Serializer;

namespace Meep.Tech.Data.Base {

  /// <summary>
  /// Base class for archetypes.
  /// Archetypes hold singleton data in data components, and static logic to act on those singletons in system components.
  /// Extend the generic (<,,>) instead of this one
  /// </summary>
  public abstract partial class Archetype : IArchetype {

    #region Archetype Data

    /// <summary>
    /// The ID for this archetype:
    /// </summary>
    public Archetype.Identity Id {
      get;
    }

    /// <summary>
    /// The name of this archetype. Names must be unique by ArchetypeBaseType
    /// </summary>
    public string Name {
      get => Id.Name;
    }

    /// <summary>
    /// The assigned internal id of this archetype. This is only consistend within the current runtime and execution.
    /// </summary>
    public int InternalId {
      get => Id.InternalId;
    }

    /// <summary>
    /// The perminant and unique external id
    /// </summary>
    public string ExternalId {
      get => Id.ExternalId;
    }

    /// <summary>
    /// This Archetype's Archetype Base Type's System.Type.
    /// </summary>
    public abstract System.Type BaseArchetypeType {
      get;
    }

    /// <summary>
    /// This is itself:
    /// </summary>
    Archetype IArchetypeId.Archetype
      => this;

    /// <summary>
    /// Overrideable bool to allow runtime registrations of certain types:
    /// Consider sealing this in child types to avoid making them modable at runtime.
    /// </summary>
    protected internal virtual bool AllowSubtypeRuntimeRegistrations
      => false;

    #region Base Components

    /// <summary>
    /// Base logic components for this archetype
    /// </summary>
    public virtual OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent> DefaultSystemComponents {
      get;
    } = new OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent>();

    /// <summary>
    /// Base data components this archetype
    /// </summary>
    public virtual OrderdDictionary<Archetype.DataComponent.Id, IArchetypeDataComponent> DefaultDataComponents {
      get;
    } = new OrderdDictionary<Archetype.DataComponent.Id, IArchetypeDataComponent>();

    /// <summary>
    /// Base data components for building a model for this archetype
    /// </summary>
    public virtual OrderedSet<Model.DataComponent.Id> DefaultModelDataComponents {
      get;
    } = new OrderedSet<Model.DataComponent.Id>();

    #endregion

    #endregion

    #region Component Data

    #region Data Access

    /// <summary>
    /// The data components that make up this archetype
    /// </summary>
    public IReadOnlyDictionary<Archetype.DataComponent.Id, IArchetypeDataComponent> DataComponents
      => _dataComponents;

    /// <summary>
    /// The system components that make up this archetype
    /// </summary>
    public IReadOnlyDictionary<Archetype.SystemComponent.Id, ISystemComponent> SystemComponents
      => _systemComponents;

    /// <summary>
    /// The link system components that make up this archetype. Should all overlap with SystemComponents
    /// </summary>
    protected IReadOnlyDictionary<Model.DataComponent.Id, ILinkSystemComponent> LinkSystemComponents
      => _linkSystemComponents;

    /// <summary>
    /// The model components to be added to any model build with this archetype.
    /// </summary>
    protected IReadOnlyCollection<Model.DataComponent.Id> ModelDataComponents
      => _modelDataComponents.Concat(_linkedModelDataComponents).ToList();

    #endregion

    /// <summary>
    /// Used to make sure we dont add two components of the same base type.
    /// That's not allowed.
    /// </summary>
    Dictionary<System.Type, Archetype.Identity> _componentIdsByComponentModelBaseType
      = new Dictionary<Type, Identity>();

    /// <summary>
    /// The data components that make up this archetype
    /// </summary>
    internal Dictionary<Archetype.DataComponent.Id, IArchetypeDataComponent> _dataComponents {
      get;
    } = new Dictionary<Archetype.DataComponent.Id, IArchetypeDataComponent>();

    /// <summary>
    /// The system components that make up this archetype
    /// </summary>
    internal Dictionary<System.Type, Archetype.DataComponent.Id> _dataComponentIdsByBaseType {
      get;
    } = new Dictionary<System.Type, Archetype.DataComponent.Id>();

    /// <summary>
    /// The system components that make up this archetype
    /// </summary>
    internal Dictionary<Archetype.SystemComponent.Id, ISystemComponent> _systemComponents {
      get;
    } = new Dictionary<Archetype.SystemComponent.Id, ISystemComponent>();

    /// <summary>
    /// The system components that make up this archetype
    /// </summary>
    internal Dictionary<System.Type, Archetype.SystemComponent.Id> _systemComponentIdsByBaseType {
      get;
    } = new Dictionary<System.Type, Archetype.SystemComponent.Id>();

    /// <summary>
    /// The model components that are added to new models by this archetype
    /// </summary>
    internal OrderedSet<Model.DataComponent.Id> _modelDataComponents {
      get;
    } = new OrderedSet<Model.DataComponent.Id>();

    /// <summary>
    /// The model components that are added to new models by this archetype
    /// </summary>
    internal readonly OrderedSet<Model.DataComponent.Id> _linkedModelDataComponents
      = new OrderedSet<Model.DataComponent.Id>();

    /// <summary>
    /// The system components that make up this archetype
    /// </summary>
    internal readonly Dictionary<Model.DataComponent.Id, ILinkSystemComponent> _linkSystemComponents
      = new Dictionary<Model.DataComponent.Id, ILinkSystemComponent>();

    #endregion

    #region Initialization

    ///For adding components in mods at startup. 
    internal protected readonly OrderdDictionary<Archetype.DataComponent.Id, IArchetypeDataComponent> _startupDataComponentsToAdd
      = new OrderdDictionary<Archetype.DataComponent.Id, IArchetypeDataComponent>();
    internal protected readonly OrderdDictionary<Archetype.DataComponent.Id, IArchetypeDataComponent> _startupDataComponentsToReplace
      = new OrderdDictionary<Archetype.DataComponent.Id, IArchetypeDataComponent>();

    /// TODO: For replaing components in mods at startup. 
    internal protected readonly OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent> _startupSystemComponentsToAdd
      = new OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent>();
    internal protected readonly OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent> _startupSystemComponentsToReplace
      = new OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent>();

    internal protected readonly OrderedSet<Model.DataComponent.Id> _startupModelComponentsToReplace
      = new OrderedSet<Model.DataComponent.Id>();
    internal protected readonly OrderedSet<Model.DataComponent.Id> _startupModelComponentsToAdd
      = new OrderedSet<Model.DataComponent.Id>();

    /// <summary>
    /// Used to create and register a new archetype.
    /// </summary>
    protected Archetype(Archetype.Identity archetypeId, Collection collection) {
      Id = archetypeId;

      collection.Register(this);
    }

    /// <summary>
    /// Used to finalize and initialize archetypes. They can no longer be updated or changed after this is called.
    /// </summary>
    internal abstract void _initialize();

    /// <summary>
    /// Finish constructing the data
    /// </summary>
    internal abstract void _finalize();

    /// <summary>
    /// Helper to get an empty model even from the base class
    /// </summary>
    /// <returns></returns>
    internal abstract IModel MakeI(Dictionary<Param, object> @params = null);
    IModel IArchetype.MakeI(Dictionary<Param, object> @params)
      => MakeI(@params);

    /// <summary>
    /// Helper to get an empty model even from the base class
    /// </summary>
    /// <returns></returns>
    internal abstract IModel MakeI(SerializedData data, int? depthLimit = null);
    IModel IArchetype.MakeI(SerializedData data, int? depthLimit)
      => MakeI(data, depthLimit);

    /// <summary>
    /// Another makeI helper.
    /// </summary>
    internal abstract IModel MakeI(
      Serializer.SerializedData serializedData,
      Dictionary<Param, object> configurationParams,
      int? initialDataLoadDepthLimit = null
    );

    /// <summary>
    /// Helper to initialize a model
    /// </summary>
    /// <returns></returns>
    internal abstract IModel InitializeModelI(Model.Params @params = null);

    /// <summary>
    /// Helper to finalzie a model
    /// </summary>
    /// <returns></returns>
    internal abstract IModel FinalizeModelI(IModel model, Model.Params @params = null);

    #endregion

    #region Hash and Equality

    public override int GetHashCode() {
      return ExternalId.GetHashCode();
    }

    public override bool Equals(object obj) {
      return obj.GetType() == GetType();
    }

    public override string ToString() {
      return $"{InternalId}/_{ExternalId}";
    }

    #endregion

    /// <summary>
    /// Get an instance of the given archetype:
    /// </summary>
    /// <typeparam name="TArchetype"></typeparam>
    /// <returns></returns>
    public static TArchetype GetInstance<TArchetype>() where TArchetype : Archetype
      => typeof(TArchetype).AsArchetype() as TArchetype;

    #region Component Access

    /// <summary>
    /// Get a system by base type:
    /// </summary>
    public TSystemComponent GetSystem<TSystemComponent>()
      where TSystemComponent : ISystemComponent
        => (TSystemComponent)
          (_systemComponents[_systemComponentIdsByBaseType[typeof(TSystemComponent).GetBaseModelType()]]);

    /// <summary>
    /// Get a system by base type:
    /// </summary>
    public bool HasSystem<TSystemComponent>()
      where TSystemComponent : ISystemComponent
        => _systemComponentIdsByBaseType.ContainsKey(typeof(TSystemComponent).GetBaseModelType());

    /// <summary>
    /// Check if the system applies. Used for syntax like Is Stackable. Is Tiered. Is Consious.
    /// </summary>
    public bool Is<TLinkSystemComponent>()
      where TLinkSystemComponent : ILinkSystemComponent
        => _systemComponentIdsByBaseType.ContainsKey(typeof(TLinkSystemComponent).GetBaseModelType());

    /// <summary>
    /// Get a system by base type:
    /// </summary>
    public bool TryGetSystem<TSystemComponent>(out TSystemComponent systemComponent)
      where TSystemComponent : ISystemComponent {
      if (_systemComponentIdsByBaseType.TryGetValue(typeof(TSystemComponent).GetBaseModelType(), out Archetype.SystemComponent.Id componentArchetypeId)) {
        systemComponent = (TSystemComponent)(_systemComponents[componentArchetypeId]);
        return true;
      }

      systemComponent = default;
      return false;
    }

    /// <summary>
    /// Get a system by base type:
    /// </summary>
    public TSystemComponent TryGetSystem<TSystemComponent>()
      where TSystemComponent : ISystemComponent {
      if (_systemComponentIdsByBaseType.TryGetValue(typeof(TSystemComponent).GetBaseModelType(), out Archetype.SystemComponent.Id componentArchetypeId)) {
        return (TSystemComponent)(_systemComponents[componentArchetypeId]);
      }

      return default;
    }

    /// <summary>
    /// Get a system by base type:
    /// </summary>
    public TDataComponent GetData<TDataComponent>()
      where TDataComponent : IArchetypeDataComponent
        => (TDataComponent)
          (_dataComponents[_dataComponentIdsByBaseType[typeof(TDataComponent).GetBaseModelType()]]);

    /// <summary>
    /// Get a system by base type:
    /// </summary>
    public bool HasData<TDataComponent>()
      where TDataComponent : IArchetypeDataComponent
        => _dataComponentIdsByBaseType.ContainsKey(typeof(TDataComponent).GetBaseModelType());

    /// <summary>
    /// Get a system by base type:
    /// </summary>
    public bool TryGetData<TDataComponent>(out TDataComponent dataComponent)
      where TDataComponent : IArchetypeDataComponent {
      if (_dataComponentIdsByBaseType.TryGetValue(typeof(TDataComponent).GetBaseModelType(), out Archetype.DataComponent.Id componentArchetypeId)) {
        dataComponent = (TDataComponent)(_dataComponents[componentArchetypeId]);
        return true;
      }

      dataComponent = default;
      return false;
    }

    /// <summary>
    /// Get a system by base type:
    /// </summary>
    public TDataComponent TryToGet<TDataComponent>()
      where TDataComponent : IArchetypeDataComponent {
      if (_dataComponentIdsByBaseType.TryGetValue(typeof(TDataComponent).GetBaseModelType(), out Archetype.DataComponent.Id componentArchetypeId)) {
        return (TDataComponent)(_dataComponents[componentArchetypeId]);
      }

      return default;
    }

    #endregion
  }

  /// <summary>
  /// Used to build an archetype with a base model type and new root archetype base parent type
  /// </summary>
  /// <typeparam name="TModelBase"></typeparam>
  /// <typeparam name="TArchetypeBase"></typeparam>
  public abstract class Archetype<TModelBase, TArchetypeBase, TIdBase>
    : Archetype
    where TModelBase : IModel<TModelBase, TArchetypeBase, TIdBase>
    where TArchetypeBase : Archetype<TModelBase, TArchetypeBase, TIdBase>
    where TIdBase : IArchetypeId {

    /// <summary>
    /// The ID for this archetype, based on the generic base type:
    /// </summary>
    public TIdBase ArchetypeId {
      get => _id ??= (TIdBase)(IArchetype)Id;
    }
    TIdBase _id;

    /// <summary>
    /// This Archetype's Archetype Base Type's System.Type.
    /// </summary>
    public override System.Type BaseArchetypeType {
      get;
    } = typeof(TArchetypeBase);

    #region Initialization

    /// <summary>
    /// Used to create and register a new archetype.
    /// </summary>
    protected Archetype(TIdBase archetypeId, Collection collection)
      : base(archetypeId as Archetype.Identity, collection) {
      if (ArchetypeLoader.IsSealed && !(AllowSubtypeRuntimeRegistrations && ArchetypeLoader.Settings.AllowRuntimeTypeRegistrations)) {
        throw new ArchetypeLoader.AttemptedCreationWhileSealedException($"Cannot create a new type for ID:{archetypeId?.ExternalId ?? "MISSINGID"}, to be added to the archetype collection: {collection}. Archetype Loading is Sealed.");
      }
      if (archetypeId == null) {
        throw new ArchetypeLoader.CannotInitializeArchetypeException($"Archetype ID provided for the Archetype System Type: {GetType().FullName},'s Ctor is NULL. Please provide a valid non-null Archetype.Identity");
      }
      _id ??= archetypeId;
    }

    /// <summary>
    /// Initialize archetypes. This is called when it's first constructed afters it's added to collections
    /// </summary>
    internal override void _initialize() {

      /// Collect Lone Model Components:
      OrderedSet<Model.DataComponent.Id> remainingModelComponentsToAdd
        = DefaultModelDataComponents
          .AddAll(_startupModelComponentsToAdd)
          .Merge(_startupModelComponentsToReplace);

      // collect archetype data components:
      OrderdDictionary<Archetype.DataComponent.Id, IArchetypeDataComponent> remainingArchetypeDataComponentsToAdd
        = DefaultDataComponents
          .AddAll(_startupDataComponentsToAdd)
          .Merge(_startupDataComponentsToReplace);

      // collect archetype system components:
      OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent> remainingArchetypeSystemComponentsToAdd
        = DefaultSystemComponents
          .AddAll(_startupSystemComponentsToAdd)
          .Merge(_startupSystemComponentsToReplace);

      // loop though and try to add them all, accounting for dependencies:
      int componentInitializationAttempt = 0;
      while (++componentInitializationAttempt <= ArchetypeLoader.Settings.ComponentInitializationAttempts
        // stop if there aren't any left:
        && (remainingModelComponentsToAdd.Any()
          || remainingArchetypeDataComponentsToAdd.Any()
          || remainingArchetypeSystemComponentsToAdd.Any())
      ) {
        bool isFinalAttempt = componentInitializationAttempt == ArchetypeLoader.Settings.ComponentInitializationAttempts;

        // model data components
        remainingModelComponentsToAdd.RemoveAll(modelDataComponentId => {
          (bool success, string error) = addModelDataComponent(modelDataComponentId);
          if (success) {
            return true;
          } else if (isFinalAttempt) {
            throw new ArchetypeLoader.CannotInitializeArchetypeException(
              $"Could not add model data component of type: {modelDataComponentId}, to archetype: {this}.\n{error}"
            );
          }

          return false;
        });

        // archetype data components
        remainingArchetypeDataComponentsToAdd.RemoveAll((dataComponentId, dataComponent) => {
          (bool success, string error) = addArchetypeDataComponent(dataComponentId, dataComponent);
          if (success) {
            return true;
          } else if (isFinalAttempt) {
            throw new ArchetypeLoader.CannotInitializeArchetypeException(
              $"Could not add model data component of type: {dataComponentId}, to archetype: {this}.\n{error}"
            );
          }

          return false;
        });

        // archetype system components
        remainingArchetypeSystemComponentsToAdd.RemoveAll((systemComponentId, systemComponent) => {
          (bool success, string error) = addArchetypeSystemComponent(systemComponentId, systemComponent);
          if (success) {
            return true;
          } else if (isFinalAttempt) {
            throw new ArchetypeLoader.CannotInitializeArchetypeException(
              $"Could not add model data component of type: {systemComponentId}, to archetype: {this}.\n{error}"
            );
          }

          return false;
        });
      }
    }

    /// <summary>
    /// Add a model data component to the ones this makes when it makes a new model
    /// </summary>
    (bool success, string message) addModelDataComponent(Model.DataComponent.Id componentId, bool isFromLinkSystem = false) {
      if (!ModelDataComponents.Contains(componentId)) {
        (bool success, string missingComponentType) = checkComponentDependencies(componentId.Archetype);
        if (!success) {
          return (false, $"Model Data Component Missing Component Dependency of Type:{missingComponentType}");
        }

        if (isFromLinkSystem) {
          _linkedModelDataComponents.Add(componentId);
        } else {
          _modelDataComponents.Add(componentId);
        }

        return (true, null);
      } else
        throw new ArchetypeLoader.CannotInitializeArchetypeException(
          $"Tried to add existing model data component:{componentId?.ExternalId ?? "NULLCOMPONENT"} from DefaultModelDataComponents on archetype: {ExternalId}"
        );
    }

    /// <summary>
    /// Add a new data component
    /// </summary>
    (bool success, string message) addArchetypeDataComponent(Archetype.DataComponent.Id dataComponentId, IArchetypeDataComponent dataComponent) {
      if (!_dataComponents.ContainsKey(dataComponentId)) {
        (bool success, string missingComponentType) = checkComponentDependencies(dataComponentId.Archetype);
        if (!success) {
          return (false, $"Archetype Data Component Missing Component Dependency: {missingComponentType}");
        }

        _dataComponents.Add(
          dataComponentId,
          dataComponent
        );
        _dataComponentIdsByBaseType.Add(
          dataComponent.getBaseModelType(),
          dataComponentId
        );

        return (true, null);
      } else
        throw new ArchetypeLoader.CannotInitializeArchetypeException(
          $"Tried to add existing archetype data component:{dataComponent?.archetypeId.ExternalId ?? "NULLCOMPONENT"} from DefaultDataComponents on archetype: {ExternalId}"
        );
    }

    /// <summary>
    /// Add a new data component
    /// </summary>
    (bool success, string message) addArchetypeSystemComponent(Archetype.SystemComponent.Id componentId, ISystemComponent systemComponent) {
      if (!_systemComponents.ContainsKey(componentId)) {
        (bool success, string missingComponentType) = checkComponentDependencies(componentId.Archetype);
        if (!success) {
          return (false, $"Archetype System Component Missing Component Dependency: {missingComponentType}");
        }

        if (systemComponent is ILinkSystemComponent linkSystemComponent) {

          // add model data
          Model.DataComponent.Id modelDataTypeId = linkSystemComponent.type.ModelComponentArchetypeId;
          (bool modelAddSuccess, string message) = addModelDataComponent(modelDataTypeId, isFromLinkSystem: true);
          if (!modelAddSuccess) {
            return (false, $"Archetype Link System Sub-Component of Type Model Data Component is Missing Component Dependency: {message}");
          }
          _linkedModelDataComponents.Add(modelDataTypeId);

          // add archetype data:
          Archetype.DataComponent.Id archetypeDataTypeId = linkSystemComponent.constructedArchetypeDataComponent.archetypeId;
          (bool archAddSuccess, string archMessage) = addArchetypeDataComponent(
            archetypeDataTypeId,
            linkSystemComponent.constructedArchetypeDataComponent
          );
          if (!archAddSuccess) {
            return (false, $"Archetype Link System Sub-Component of Type Archetype Data Component is Missing Component Dependency: {archMessage}");
          }

          // Get the model data component type's id as the key
          if (!_linkSystemComponents.ContainsKey(modelDataTypeId)) {
            _linkSystemComponents.Add(
              modelDataTypeId,
              linkSystemComponent
            );
          } else
            throw new ArchetypeLoader.CannotInitializeArchetypeException($"Tried to add existing link system component:{componentId?.ExternalId ?? "NULLCOMPONENT"}//{modelDataTypeId?.ToString() ?? "NULLMODELID"} from DefaultSystemComponents on archetype: {ExternalId}");
        }

        _systemComponents.Add(
          componentId,
          systemComponent
        );
        _systemComponentIdsByBaseType.Add(
          systemComponent.getBaseModelType(),
          componentId
        );

        return (true, null);
      } else
        throw new ArchetypeLoader.CannotInitializeArchetypeException(
          $"Tried to add existing archetype data component:{systemComponent?.archetypeId.ExternalId ?? "NULLCOMPONENT"} from DefaultDataComponents on archetype: {ExternalId}"
        );
    }

    /// <summary>
    /// Make sure a component has the dependencies it needs to be added
    /// </summary>
    (bool success, string error) checkComponentDependencies(Archetype archetype) {
      if (archetype == null) {
        return (true, "No Archetype");
      }

      IComponentArchetype componentArchetype = archetype as IComponentArchetype;

      if (componentArchetype.RequiredModelDataComponents != null) {
        foreach (Model.DataComponent.Id requiredComponentId in componentArchetype.RequiredModelDataComponents) {
          if (!ModelDataComponents.Contains(requiredComponentId)) {
            return (false, $"Model Data Component of type {requiredComponentId}");
          }
        }
      }

      if (componentArchetype.RequiredArchetypeDataComponents != null) {
        foreach (Archetype.DataComponent.Id requiredComponentId in componentArchetype.RequiredArchetypeDataComponents) {
          if (!DataComponents.ContainsKey(requiredComponentId)) {
            return (false, $"Archetype Data Component of type {requiredComponentId}");
          }
        }
      }

        if (componentArchetype.RequiredSystemComponents != null) {
          foreach (Archetype.SystemComponent.Id requiredComponentId in componentArchetype.RequiredSystemComponents) {
            if (!SystemComponents.ContainsKey(requiredComponentId)) {
              return (false, $"Archetype System Component of type {requiredComponentId}");
            }
          }
        }

      return (true, null);
    }

    /// <summary>
    /// Finish setting up the archetype, including any components added from elsewehre.
    /// They should no longer be editied after this is called.
    /// This is called after all valid archetypes of this given type have been Initialized.
    /// </summary>
    internal override void _finalize() { }

    #region Model Construction

    /// <summary>
    /// Make/inistialize/construct an empty object for this archetype:
    /// </summary>
    protected abstract TModelBase InitializeModel(Model.Params @params = null);

    /// <summary>
    /// Make/inistialize/construct an empty object for this archetype:
    /// </summary>
    internal sealed override IModel InitializeModelI(Model.Params @params = null)
      => InitializeModel(@params);

    /// <summary>
    /// Configure and set param on the empty new model from InitializeModel.
    /// </summary>
    protected virtual TModelBase ConfigureModel(TModelBase model, Model.Params @params = null)
      => model;

    /// <summary>
    /// used to configre some basic model settings
    /// </summary>
    protected virtual TModelBase ConfigureBasicModelValues(TModelBase model, Model.Params @params = null) {

      // If this is owned, set the owner if it's passed in:
      if (model is IOwned ownedModel && ownedModel.owner == null) {
        if (ownedModel.OwnerIsRequiredOnCreation) {
          ownedModel.setOwner(@params?.GetAndValidateAs<string>(Param.OwnerId));
        } else {
          var owner = @params?.GetAs<string>(Param.OwnerId);
          if (!(owner is null)) {
            ownedModel.setOwner(owner);
          }
        }
      }

      // If this is unique, set the unique id:
      if (model is IUnique uniqueModel && uniqueModel.id == null) {
        string creatorToken
          = @params?.GetAs<string>(Param.CreatorToken);
        uniqueModel.setUniqueId(
          @params?.GetAs(
            Param.UniqueId,
            creatorToken != null
            ? RNG.GetNextUniqueId()
            : RNG.GetNextUniqueIdFromToken(creatorToken)
          ) ?? (creatorToken != null
            ? RNG.GetNextUniqueId()
            : RNG.GetNextUniqueIdFromToken(creatorToken)
          )
        );
      }

      return model;
    }

    /// <summary>
    /// Initialize and add components.
    /// </summary>
    protected virtual TModelBase InitializeModelDataComponents(IModelDataComponentStorage model, Model.Params @params = null) {
      //TODO: order these by dependenies prior to this:
      foreach (Model.DataComponent.Id modelDataComponentId in _modelDataComponents) {
        IArchetype modelDataComponentArchetype
          = modelDataComponentId.Archetype != null
            ? Components.ModelDatas.GetI(modelDataComponentId)
            : null;
        IModelDataComponent modelComponent = (
          modelDataComponentArchetype?.MakeI(@params)
            ?? ISerializeable.Make(
              null,
              @params,
              Archetype.Identity._systemTypesByModelTypeIdForDefaultCtorTypes[modelDataComponentId] // copies logic from ModelBaseType
            )
          ) as IModelDataComponent;

        // add the created component:
        model.addComponent(modelComponent);
      }

      // if we have a system linked to set this model up based on an archetype data component, we need to initialize it.
      /// First InitializeModel is called
      /// Then InitializeModelComponentData is called
      /// Then FinalizeModel is called on the component
      /// Then FinalizeModel is called on the parent model
      /// Lastly FinalizeModelComponentData is called on the component
      //TODO: order these by dependenies prior to this:
      //// TODO: we'll need to seperate this logic out into two functions, one for link systems, and one for models, and make combined list of both types in dependency order
      foreach (ILinkSystemComponent linkSystemComponent in LinkSystemComponents.Values) {
        IModelDataComponent newModelDataComponent;
        model.addComponent(
            /// initialize as component:
            linkSystemComponent.type.InitializeModelComponentData(
              /// but first, initialize as model
              newModelDataComponent = linkSystemComponent.type.ModelComponentArchetypeId.Archetype.InitializeModelI() as IModelDataComponent,
            DataComponents.TryGetValue(linkSystemComponent.type.ArchetypeComponentArchetypeId, out IArchetypeDataComponent archetypeDataComponent)
              ? archetypeDataComponent
              : throw new System.Exception($"Missing link component: {linkSystemComponent.type.ArchetypeComponentArchetypeId.ExternalId} needed on archetype: {ExternalId}, for link component: {linkSystemComponent.archetypeId.ExternalId}."),
            this,
            model,
            @params
          )
        );

        /// finalize component as model
        newModelDataComponent.type.FinalizeModelI(newModelDataComponent, @params);
      }

      return (TModelBase)model;
    }

    /// <summary>
    /// Helper to make a child model with params:
    /// </summary>
    /// <param name="params"></param>
    /// <returns></returns>
    public TDesiredModel Make<TDesiredModel>(Model.Params @params = null) 
      where TDesiredModel : TModelBase 
    {
      // initialize the model
      TModelBase newModel = InitializeModel(@params);
      // configure the model
      return ConfigureModelAndComponents<TDesiredModel>(newModel, @params);
    }

    /// <summary>
    /// Helper to make a child model with params:
    /// </summary>
    /// <param name="params"></param>
    /// <returns></returns>
    public TModelBase Make(Model.Params @params = null)
      => Make<TModelBase>(@params);

    /// <summary>
    /// Helper to make a child model with params:
    /// This one may be slower because it needs to construct a dictionary from an array.
    /// </summary>
    public TModelBase Make(params (Param param, object value)[] @params)
      => Make<TModelBase>(@params.ToDictionary(entry => entry.param, entry => entry.value));

    /// <summary>
    /// Helper to make a child model with params:
    /// This one may be slower because it needs to construct a dictionary from an array.
    /// </summary>
    public TDesiredModel Make<TDesiredModel>(params (Param param, object value)[] @params)
      where TDesiredModel : TModelBase
        => Make<TDesiredModel>(@params.ToDictionary(entry => entry.param, entry => entry.value));

    /// <summary>
    /// Helper to make a child model with params:
    /// </summary>
    public virtual TDesiredModel Make<TDesiredModel>(Dictionary<Param, object> @params)
      where TDesiredModel : TModelBase
        => Make<TDesiredModel>(@params == null ? null : new Model.Params(@params));

    /// <summary>
    /// Helper to make a child model with params:
    /// </summary>
    /// <param name="params"></param>
    /// <returns></returns>
    public TModelBase Make(Dictionary<Param, object> @params)
      => Make<TModelBase>(@params == null ? null : new Model.Params(@params));

    /// <summary>
    /// Helper to make a child model from serialized datas:
    /// </summary>
    public TDesiredModel Make<TDesiredModel>(SerializedData serializedData, int? depthLimit = null)
      where TDesiredModel : TModelBase
        => (TDesiredModel)Make(serializedData, depthLimit);

    /// <summary>
    /// Helper to make a child model from serialized datas:
    /// </summary>
    public virtual TModelBase Make(SerializedData serializedData, int? depthLimit = null)
      => serializedData.sqlData[SQLTypeColumnName] as string == ExternalId 
        ? (TModelBase)InitializeModel().deserialize(serializedData, depthLimit)
        : throw new InvalidCastException($"Cannot use archetype of type {ExternalId}, to make a model requiring archetype type: {serializedData.sqlData[SQLTypeColumnName] as string}");

    /// <summary>
    /// Helper to make a child model from serialized data, and then also configure with params:
    /// This works best if you use ??= and empty checks in ConfigureModel
    /// </summary>
    public virtual TDesiredModel Make<TDesiredModel>(
      SerializedData initialData,
      Model.Params configurationParams,
      int? initialDataLoadDepthLimit = null
    )
      where TDesiredModel : TModelBase 
    {
      // initialize the model
      TModelBase newModel = Make(initialData, initialDataLoadDepthLimit);
      // configure the model
      return ConfigureModelAndComponents<TDesiredModel>(newModel, configurationParams);
    }

    /// <summary>
    /// Helper to make a child model from serialized data, and then also configure with params:
    /// </summary>
    public TModelBase Make(SerializedData initialData, Model.Params configurationParams, int? initialDataLoadDepthLimit = null)
      => Make<TModelBase>(initialData, configurationParams, initialDataLoadDepthLimit);

    /// <summary>
    /// Helper to make a child model from serialized data, and then also configure with params:
    /// This works best if you use ??= and empty checks in ConfigureModel
    /// </summary>
    public virtual TDesiredModel Make<TDesiredModel>(
      SerializedData initialData,
      Dictionary<Param, object> configurationParams,
      int? initialDataLoadDepthLimit = null
    ) where TDesiredModel : TModelBase
      => Make<TDesiredModel>(initialData, configurationParams == null ? null : new Model.Params(configurationParams), initialDataLoadDepthLimit);

      /// <summary>
      /// Helper to make a child model from serialized data, and then also configure with params:
      /// </summary>
      public TModelBase Make(SerializedData initialData, Dictionary<Param, object> configurationParams, int? initialDataLoadDepthLimit = null)
      => Make<TModelBase>(initialData, configurationParams, initialDataLoadDepthLimit);

    /// <summary>
    /// Used for the parent base class to make a model interface
    /// </summary>
    internal sealed override IModel MakeI(Dictionary<Param, object> @params = null)
      => Make<TModelBase>(@params);

    /// <summary>
    /// Used for the parent base class to make a model interface
    /// </summary>
    internal sealed override IModel MakeI(SerializedData data, int? depthLimit = null)
      => Make(data, depthLimit);

    /// <summary>
    /// Another makeI helper.
    /// </summary>
    internal sealed override IModel MakeI(
      Serializer.SerializedData serializedData,
      Dictionary<Param, object> configurationParams,
      int? initialDataLoadDepthLimit = null
    ) => Make(serializedData, configurationParams, initialDataLoadDepthLimit);

    /// <summary>
    /// Override allowing you to modify the model after the default componenents have been added and Initialized.
    /// </summary>
    protected virtual TModelBase FinalizeModel(TModelBase model, Model.Params @params = null)
      => model;

    /// <summary>
    /// Make/inistialize/construct an empty object for this archetype:
    /// </summary>
    internal override IModel FinalizeModelI(IModel model, Model.Params @params = null)
      => FinalizeModel((TModelBase)model, @params);

    /// <summary>
    /// Finalize all model components
    /// </summary>
    protected virtual TModelBase FinalizeModelDataComponents(IModelDataComponentStorage model, Model.Params @params = null) {
      // For model components with link systems we need to finalize setting up all of the child components:
      foreach (ILinkSystemComponent linkSystemComponent in LinkSystemComponents.Values) {
        /// finalize the components as components
        model.updateComponent(
          linkSystemComponent.type.FinalizeModelComponentData(
            model.getComponent(linkSystemComponent.type.ModelComponentArchetypeId),
            DataComponents[linkSystemComponent.type.ArchetypeComponentArchetypeId],
            this,
            model,
            @params
          )
        );
      }

      return (TModelBase)model;
    }

    /// <summary>
    /// try to load a model via this type given the unique ID:
    /// </summary>
    public TModel Load<TModel>(string uniqueModelId) where TModel : TModelBase, IUnique {
      TModel model = (TModel)ConfigureBasicModelValues(InitializeModel(), new Model.Params {
        {Param.UniqueId, uniqueModelId }
      });
      return Serializer.Load(model);
    }

    /// <summary>
    /// try to load a model via this type given the unique ID:
    /// TODO: make this much more efficient by batching the loads like we do saves.
    /// </summary>
    public IEnumerable<TModel> Load<TModel>(IEnumerable<string> uniqueModelIds)
      where TModel : TModelBase, IUnique
        => uniqueModelIds.Select(uniqueModelId => Serializer
          .Load((TModel)
            ConfigureModel(
              InitializeModel(),
              new Model.Params {
                { Param.UniqueId, uniqueModelId }
              }
            )
          )
        );

    /// <summary>
    /// Overrideable helper for configuring a new model and it's components:
    /// </summary>
    protected virtual TDesiredModel ConfigureModelAndComponents<TDesiredModel>(
      TModelBase newModel,
      Model.Params configurationParams
    ) where TDesiredModel : TModelBase {
      /// Run configurations.
      newModel = ConfigureBasicModelValues(newModel, configurationParams);
      newModel = ConfigureModel(newModel, configurationParams);

      /// For models with components we need to set up all of those child components:
      if (newModel is IModelDataComponentStorage modelObject) {
        newModel = InitializeModelDataComponents(modelObject, configurationParams);
      }

      // finalize the parent model
      newModel = FinalizeModel(newModel, configurationParams);
      if ((modelObject = newModel as IModelDataComponentStorage) != null) {
        newModel = FinalizeModelDataComponents(modelObject, configurationParams);
      }

      // cache the model if it's cacheable
      if (newModel is ICached modelToCache) {
        ICached.Cache(modelToCache);
      }

      return (TDesiredModel)newModel;
    }

    #endregion

    #endregion

    /// <summary>
    /// Get an instance of the given archetype:
    /// </summary>
    public new static TArchetype GetInstance<TArchetype>() where TArchetype : TArchetypeBase
      => (TArchetype)typeof(TArchetype).AsArchetype();

    /// <summary>
    /// Get a model for this base type from the cache:
    /// Shortcut for ICached<TModelBase>.FromCache(modelUniqueId)
    /// </summary>
    public static TModel GetModelFromCache<TModel>(string modelUniqueId)
      where TModel : class, TModelBase, ICached<TModel>
        => ICached<TModel>.FromCache(modelUniqueId);

    /// <summary>
    /// Get a model for this base type from the cache:
    /// Shortcut for ICached<TModelBase>.FromCache(modelUniqueId)
    /// </summary>
    public static TModelBase GetModelFromCache(string modelUniqueId) {
      IUnique fetched = null;
      try {
        return (TModelBase)(fetched = ICached.FromCache(modelUniqueId));
      } catch (InvalidCastException e) {
        throw new InvalidCastException($"Fetched Model From Cache with ID {modelUniqueId} is likely not of type {typeof(TModelBase).FullName}. Actual type: {fetched?.GetType().FullName ?? "NULL"}", e);
      }
    }
  }
}

public static class ArchetypeExtensions {

  /// <summary>
  /// Get a model for this base type from the cache:
  /// Shortcut for ICached<TModelBase>.FromCache(modelUniqueId)
  /// </summary>
  public static IUnique GetModelFromCache<TArchetype, TModelBase, TArchetypeBase, TIdBase>(this TArchetype requestedType, string modelUniqueId)
    where TArchetype : Archetype<TModelBase, TArchetypeBase, TIdBase>
    where TModelBase : class, IModel<TModelBase, TArchetypeBase, TIdBase>, ICached<TModelBase>
    where TArchetypeBase : Archetype<TModelBase, TArchetypeBase, TIdBase>
    where TIdBase : IArchetypeId
      => ICached<TModelBase>.FromCache(modelUniqueId);
}