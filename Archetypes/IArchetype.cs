﻿using Meep.Tech.Data.Base;
using System.Collections.Generic;
using static Meep.Tech.Data.Serialization.Serializer;

namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// Internally used base interface for Archetypes:
  /// </summary>
  public interface IArchetype : IArchetypeId {

    /// <summary>
    /// The ID for this archetype:
    /// </summary>
    Archetype.Identity Id {
      get;
    }

    /// <summary>
    /// This Archetype's Archetype Base Type's System.Type.
    /// </summary>
    System.Type BaseArchetypeType {
      get;
    }

    /// <summary>
    /// Base logic components for this archetype
    /// </summary>
    OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent> DefaultSystemComponents {
      get;
    }

    /// <summary>
    /// Base data components this archetype
    /// </summary>
    OrderdDictionary<Archetype.DataComponent.Id, IArchetypeDataComponent> DefaultDataComponents {
      get;
    }

    /// <summary>
    /// Base data components for building a model for this archetype
    /// </summary>
    OrderedSet<Model.DataComponent.Id> DefaultModelDataComponents {
      get;
    }

    /// <summary>
    /// The data components that make up this archetype
    /// </summary>
    IReadOnlyDictionary<Archetype.DataComponent.Id, IArchetypeDataComponent> DataComponents { 
      get; 
    }

    /// <summary>
    /// The system components that make up this archetype
    /// </summary>
    IReadOnlyDictionary<Archetype.SystemComponent.Id, ISystemComponent> SystemComponents {
      get; 
    }

    /// <summary>
    /// Build model
    /// </summary>
    /// <returns></returns>
    internal IModel MakeI(Dictionary<Param, object> @params = null);

    /// <summary>
    /// Build model from serialized data
    /// </summary>
    /// <returns></returns>
    internal IModel MakeI(SerializedData data, int? depthLimit = null);
  };
}
