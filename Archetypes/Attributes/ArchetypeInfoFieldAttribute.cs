﻿using System;

namespace Meep.Tech.Data.Attributes.Archetypes {

  /// <summary>
  /// Represents data that should be considered a recorded/extrnal 'data field' of an attribute
  /// </summary>
  public class ArchetypeInfoFieldAttribute : Attribute {}
}
