﻿namespace Meep.Tech.Data.Attributes.Archetypes {

  /// <summary>
  /// Represents a data field of an archetype that comes from it's base id enumeration. Like ID
  /// </summary>
  public class BaseEnumArchetypeFieldAttribute : ArchetypeInfoFieldAttribute {}
}
