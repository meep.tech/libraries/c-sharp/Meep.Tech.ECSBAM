﻿using System;

namespace Meep.Tech.Data.Attributes.Archetypes {

  /// <summary>
  /// Prevents a type from being built. this is NOT inherited.
  /// </summary>
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = false)]
  public class DoNotBuildArchetypeInitiallyAttribute : Attribute { }

  /// <summary>
  /// Prevents a type and it's inherited types from being built into an archetype.
  /// </summary>
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
  public class DoNotBuildArchetypeOrChildrenInitiallyAttribute : DoNotBuildArchetypeInitiallyAttribute { }
}
