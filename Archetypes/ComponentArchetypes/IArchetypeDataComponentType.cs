﻿namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// Interface for generic access to certain things:
  /// </summary>
  public interface IArchetypeDataComponentType : IArchetype { }
}
