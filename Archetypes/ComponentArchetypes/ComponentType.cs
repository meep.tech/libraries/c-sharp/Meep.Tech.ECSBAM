﻿using Meep.Tech.Data.Base;
using System.Collections.Generic;

namespace Meep.Tech.Data.Extensions {
  public partial class Component {

    public static class Type {

      /// <summary>
      /// Base Ids for any type of component:
      /// </summary>
      public abstract class Id : Archetype.Identity {
        protected internal Id(string name, string externalIdPrefixEnding = "")
          : base(name, $"{externalIdPrefixEnding}") { }
        protected internal Id(string name, System.Type sealedComponentType)
          : base(name, sealedComponentType) { }
      }
    }

    /// <summary>
    /// Archetype Base used for components:
    /// </summary>
    public abstract class Type<TComponentModelBase, TComponentArchetypeBase, TIdBase>
    : Archetype<TComponentModelBase, TComponentArchetypeBase, TIdBase>,
      IComponentArchetype
    where TComponentModelBase
      : IComponent,
        IModel<TComponentModelBase, TComponentArchetypeBase, TIdBase>
    where TIdBase : Component.Type.Id
    where TComponentArchetypeBase : Type<TComponentModelBase, TComponentArchetypeBase, TIdBase> {

      /// <summary>
      /// system component dependencies for this component
      /// </summary>
      public virtual HashSet<Archetype.SystemComponent.Id> RequiredSystemComponents {
        get;
      } = new HashSet<Archetype.SystemComponent.Id>();

      /// <summary>
      /// data component dependencies for this component
      /// </summary>
      public virtual HashSet<Archetype.DataComponent.Id> RequiredArchetypeDataComponents {
        get;
      } = new HashSet<Archetype.DataComponent.Id>();

      /// <summary>
      /// model data component dependencies for this component
      /// </summary>
      public virtual HashSet<Model.DataComponent.Id> RequiredModelDataComponents {
        get;
      } = new HashSet<Model.DataComponent.Id>();

      /// <summary>
      /// Used for children to make component archetypes:
      /// </summary>
      protected Type(Component.Type.Id componentId, Collection collection)
        : base(componentId as TIdBase, collection) { }
    }
  }
}