﻿using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Static;
using System;
using System.Collections.Generic;

namespace Meep.Tech.Data.Base {
  public partial class Archetype {
    public partial class SystemComponent {

      /// <summary>
      /// Used to hold base functionality for Archetypes for Archetype System Components
      /// </summary>
      public abstract class Type<TComponentModelBase>
        : Component.Type<TComponentModelBase, Type<TComponentModelBase>, Archetype.SystemComponent.Id>,
          IArchetypeSystemComponentType
        where TComponentModelBase
          : ISystemComponent,
            IModel<TComponentModelBase, Type<TComponentModelBase>, Archetype.SystemComponent.Id> 
      {

        protected Type(Archetype.SystemComponent.Id componentId)
          : base(componentId, Components.ArchetypeSystemComponents) { }

        /// <summary>
        /// Cant make these via make
        /// </summary>
        sealed protected override TComponentModelBase InitializeModel(Model.Params @params = null)
          => throw new NotImplementedException("Archetype System Components must be initialized via a public CTOR inside their Archetype Definitions.");
      }
    }
  }
}