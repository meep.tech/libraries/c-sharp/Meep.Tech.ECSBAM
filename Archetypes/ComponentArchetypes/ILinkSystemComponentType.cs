﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Static;
using System;

namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// Interface for generic access to certain things:
  /// </summary>
  public interface ILinkSystemComponentType 
    : IArchetypeSystemComponentType 
  {

    /// <summary>
    /// system type of the linked model data component
    /// </summary>
    Type ModelComponentArchetypeSystemType {
      get;
    }

    /// <summary>
    /// system type of the linked archetype data component
    /// </summary>
    Type ArchetypeComponentArchetypeSystemType {
      get;
    }

    /// <summary>
    /// The type of the linked model component
    /// </summary>
    Type ModelComponentSystemType {
      get;
    }

    /// <summary>
    /// The type of the linked archetype component
    /// </summary>
    Type ArchetypeComponentSystemType {
      get;
    }

    /// <summary>
    /// External id of the linked model data component
    /// </summary>
    public Model.DataComponent.Id ModelComponentArchetypeId
      => (Model.DataComponent.Id)ModelComponentArchetypeSystemType.AsArchetype().Id;

    /// <summary>
    /// External id of the linked model data component
    /// </summary>
    public Archetype.DataComponent.Id ArchetypeComponentArchetypeId
      => (Archetype.DataComponent.Id)ArchetypeComponentArchetypeSystemType.AsArchetype().Id;

    /// <summary>
    /// Initialize the model's component data during model creation given an archetype and model system, and return the updated model component
    /// </summary>
    public IModelDataComponent InitializeModelComponentData(IModelDataComponent modelComponent, IArchetypeDataComponent archetypeComponent, IArchetype archetype, IModel model, Model.Params @params) {
      return modelComponent;
    }

    /// <summary>
    /// Finalize the model's component data during model creation given an archetype and model system, and return the updated model component
    /// </summary>
    public IModelDataComponent FinalizeModelComponentData(IModelDataComponent modelComponent, IArchetypeDataComponent archetypeComponent, IArchetype archetype, IModel model, Model.Params @params) {
      return modelComponent;
    }

    /// <summary>
    /// The default archetype component for people who may not want to provide one
    /// </summary>
    /// <returns></returns>
    public IArchetypeDataComponent GetDefaultArchetypeComponent()
      => (IArchetypeDataComponent)ArchetypeComponentArchetypeId.Archetype.MakeI();

  }

}
