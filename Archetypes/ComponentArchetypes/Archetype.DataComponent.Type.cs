﻿using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Static;
using System;
using System.Collections.Generic;

namespace Meep.Tech.Data.Base {
  public partial class Archetype {
    public partial class DataComponent {

      /// <summary>
      /// Used to hold base functionality for Archetypes for Archetype Data Components
      /// </summary>
      public abstract class Type<TComponentModelBase>
        : Component.Type<TComponentModelBase, Type<TComponentModelBase>, Archetype.DataComponent.Id>,
          IArchetypeDataComponentType
        where TComponentModelBase
          : IArchetypeDataComponent,
            IModel<TComponentModelBase, Type<TComponentModelBase>, Archetype.DataComponent.Id> 
      {

        protected Type(Archetype.DataComponent.Id componentId)
          : base(componentId, Components.ArchetypeDataComponents) { }

        sealed protected override TComponentModelBase InitializeModel(Model.Params @params = null)
            => throw new NotImplementedException("Archetype Data Components must be initialized via a public CTOR inside their Archetype Definitions.");
      }
    }
  }
}
