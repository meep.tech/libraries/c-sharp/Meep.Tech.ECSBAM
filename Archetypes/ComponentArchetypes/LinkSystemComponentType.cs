﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Static;
using System;
using System.Collections.Generic;

namespace Meep.Tech.Data.Base {
  public partial class Archetype {
    public partial class LinkSystemComponent {

      /// <summary>
      /// Used to hold base functionality for Archetypes for Link System Components
      /// </summary>
      public abstract class Type<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >
        : Component.Type<
            TComponentModelBase,
            Archetype.LinkSystemComponent.Type<
              TComponentModelBase,
              TLinkedModelComponent,
              TLinkedArchetypeComponent,
              TLinkedModelComponentArchetype,
              TLinkedArchetypeComponentArchetype
            >,
            LinkSystemComponent.Id
          >,
          ILinkSystemComponentType
        where TComponentModelBase
          : ILinkSystemComponent,
            IModel<
              TComponentModelBase,
              Archetype.LinkSystemComponent.Type<
                TComponentModelBase,
                TLinkedModelComponent,
                TLinkedArchetypeComponent,
                TLinkedModelComponentArchetype,
                TLinkedArchetypeComponentArchetype
              >,
              LinkSystemComponent.Id
            >
        where TLinkedModelComponentArchetype : IModelDataComponentType
        where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
        where TLinkedModelComponent : IModelDataComponent
        where TLinkedArchetypeComponent : IArchetypeDataComponent {

        /// <summary>
        /// Cant make these via make
        /// </summary>
        sealed protected override TComponentModelBase InitializeModel(Model.Params @params = null)
          => throw new NotImplementedException("Archetype Link System Components must be initialized via a public CTOR inside their Archetype Definitions.");

        #region System Logic Functions

        ///// Prefix:
        /// Simple: This only effects/takes the components as parameters.
        /// Advanced: This takes both components, the parent model, and archetype of the parent model as params

        ///// Suffix:
        /// Function means it takes custom Input params
        /// Logic means it only effects the model and components, taking no input params

        /// <summary>
        /// Used to build system logic functions
        /// </summary>
        public delegate TOut SimpleLogic<TOut>(
          TLinkedModelComponent modelData,
          TLinkedArchetypeComponent archetypeData
        );

        /// <summary>
        /// Used to build basic system logic functions
        /// </summary>
        public delegate void SimpleLogic(
          TLinkedModelComponent modelData,
          TLinkedArchetypeComponent archetypeData
        );

        /// <summary>
        /// Used to build advanced logic functions
        /// </summary>
        public delegate TOut AdvancedLogic<TOut>(
          TLinkedModelComponent modelComponentData,
          TLinkedArchetypeComponent archetypeComponentData,
          IModel model,
          Archetype archetype
        );

        /// <summary>
        /// Used to build advanced system logic functions
        /// </summary>
        public delegate void AdvancedLogic(
          TLinkedModelComponent modelComponentData,
          TLinkedArchetypeComponent archetypeComponentData,
          IModel model,
          Archetype archetype
        );

        /// <summary>
        /// Used to build system logic functions
        /// </summary>
        public delegate TOut SimpleFunction<TIn, TOut>(
          TIn input,
          TLinkedModelComponent modelData,
          TLinkedArchetypeComponent archetypeData
        );

        /// <summary>
        /// Used to build system logic functions
        /// </summary>
        public delegate void SimpleFunction<TIn>(
          TIn input,
          TLinkedModelComponent modelData,
          TLinkedArchetypeComponent archetypeData
        );

        /// <summary>
        /// Used to build advanced system logic functions
        /// </summary>
        public delegate TOut AdvancedFunction<TIn, TOut>(
          TIn input,
          TLinkedModelComponent modelComponentData,
          TLinkedArchetypeComponent archetypeComponentData,
          IModel model,
          Archetype archetype
        );

        /// <summary>
        /// Used to build advanced logic functions
        /// </summary>
        public delegate void AdvancedFunction<TIn>(
          TIn input,
          TLinkedModelComponent modelComponentData,
          TLinkedArchetypeComponent archetypeComponentData,
          IModel model,
          Archetype archetype
        );

        #endregion

        /// <summary>
        /// The type of the linked model component
        /// </summary>
        public Type ModelComponentArchetypeSystemType {
          get;
        } = typeof(TLinkedModelComponentArchetype);

        /// <summary>
        /// The type of the linked archetype component
        /// </summary>
        public Type ArchetypeComponentArchetypeSystemType {
          get;
        } = typeof(TLinkedArchetypeComponentArchetype);

        /// <summary>
        /// The type of the linked model component
        /// </summary>
        public Type ModelComponentSystemType {
          get;
        } = typeof(TLinkedModelComponent);

        /// <summary>
        /// The type of the linked archetype component
        /// </summary>
        public Type ArchetypeComponentSystemType {
          get;
        } = typeof(TLinkedArchetypeComponent);

        #region Initialization

        /// <summary>
        /// Ctor for making child types:
        /// </summary>
        protected Type(LinkSystemComponent.Id componentId)
          : base(componentId, Components.ArchetypeSystemComponents) { }

        /// <summary>
        /// Initialize the model's component data during model creation given an archetype and model system, and return the updated model component
        /// </summary>
        protected virtual TLinkedModelComponent InitializeModelComponentData(TLinkedModelComponent modelComponent, TLinkedArchetypeComponent archetypeComponent, IArchetype archetype, IModel model, Model.Params @params) {
          return modelComponent;
        }

        /// <summary>
        /// Finalize the model's component data during model creation given an archetype and model system, and return the updated model component
        /// </summary>
        protected virtual TLinkedModelComponent FinalizeModelComponentData(TLinkedModelComponent modelComponent, TLinkedArchetypeComponent archetypeComponent, IArchetype archetype, IModel model, Model.Params @params) {
          return modelComponent;
        }

        /// <summary>
        /// The default archetype component for people who may not want to provide one
        /// </summary>
        /// <returns></returns>
        protected virtual IArchetypeDataComponent GetDefaultArchetypeComponent()
          => (IArchetypeDataComponent)((ILinkSystemComponentType)this).ArchetypeComponentArchetypeId.Archetype.MakeI();

        /// <summary>
        /// Initialize the model's component data during model creation given an archetype and model system, and return the updated model component
        /// </summary>
        IModelDataComponent ILinkSystemComponentType.InitializeModelComponentData(IModelDataComponent modelComponent, IArchetypeDataComponent archetypeComponent, IArchetype archetype, IModel model, Model.Params @params)
          => InitializeModelComponentData((TLinkedModelComponent)modelComponent, (TLinkedArchetypeComponent)archetypeComponent, archetype, model, @params);

        /// <summary>
        /// Finalize the model's component data during model creation given an archetype and model system, and return the updated model component
        /// </summary>
        IModelDataComponent ILinkSystemComponentType.FinalizeModelComponentData(IModelDataComponent modelComponent, IArchetypeDataComponent archetypeComponent, IArchetype archetype, IModel model, Model.Params @params)
          => FinalizeModelComponentData((TLinkedModelComponent)modelComponent, (TLinkedArchetypeComponent)archetypeComponent, archetype, model, @params);

        /// <summary>
        /// The default archetype component for people who may not want to provide one
        /// </summary>
        /// <returns></returns>
        IArchetypeDataComponent ILinkSystemComponentType.GetDefaultArchetypeComponent()
          => GetDefaultArchetypeComponent();

        #endregion
      }
    }
  }
}

public static class LinkSystemTypeExtensions {

  /// <summary>
  /// Execute the system For this model
  /// </summary>
  public static void ExecuteFor<
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent.Type<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.SimpleLogic simpleLogic,
    TModel model
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    simpleLogic(
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>()
    );
  }

  /// <summary>
  /// Execute the system For this model
  /// </summary>
  public static TReturn ExecuteFor<
    TReturn,
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent.Type<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.SimpleLogic<TReturn> simpleLogic,
    TModel model
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    return simpleLogic(
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>()
    );
  }

  /// <summary>
  /// Execute the system ON this model, possibly editing the model
  /// </summary>
  public static void ExecuteOn<
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent.Type<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.AdvancedLogic simpleLogic,
    TModel model
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    simpleLogic(
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>(),
      model,
      model.type
    );
  }

  /// <summary>
  /// Execute the system ON this model, possibly editing the model
  /// </summary>
  public static TReturn ExecuteOn<
    TReturn,
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent.Type<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.AdvancedLogic<TReturn> simpleLogic,
    TModel model
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    return simpleLogic(
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>(),
      model,
      model.type
    );
  }

  /// <summary>
  /// Execute the system For this model
  /// </summary>
  public static void ExecuteFor<
    TInput,
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent.Type<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.SimpleFunction<TInput> simpleLogic,
    TModel model,
    TInput input = default
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    simpleLogic(
      input,
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>()
    );
  }

  /// <summary>
  /// Execute the system For this model
  /// </summary>
  public static TReturn ExecuteFor<
    TInput,
    TReturn,
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent.Type<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.SimpleFunction<TInput, TReturn> simpleLogic,
    TModel model,
    TInput input = default
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    return simpleLogic(
      input,
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>()
    );
  }

  /// <summary>
  /// Execute the system ON this model, possibly editing the model
  /// </summary>
  public static void ExecuteOn<
    TInput,
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent.Type<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.AdvancedFunction<TInput> simpleLogic,
    TModel model,
    TInput input = default
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    simpleLogic(
      input,
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>(),
      model,
      model.type
    );
  }

  /// <summary>
  /// Execute the system ON this model, possibly editing the model
  /// </summary>
  public static TReturn ExecuteOn<
    TInput,
    TReturn,
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent.Type<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.AdvancedFunction<TInput, TReturn> simpleLogic,
    TModel model,
    TInput input = default
  ) where TComponentModelBase
      : ILinkSystemComponent<
          TComponentModelBase,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel 
      : IModel,
        IModelDataComponentStorage 
    => simpleLogic(
      input,
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>(),
      model,
      model.type
    );
}