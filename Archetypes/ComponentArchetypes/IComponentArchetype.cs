﻿using Meep.Tech.Data.Base;
using System.Collections.Generic;

namespace Meep.Tech.Data.Extensions {
  internal interface IComponentArchetype {

    /// <summary>
    /// system component dependencies for this component
    /// </summary>
    HashSet<Archetype.SystemComponent.Id> RequiredSystemComponents {
      get;
    }

    /// <summary>
    /// data component dependencies for this component
    /// </summary>
    HashSet<Archetype.DataComponent.Id> RequiredArchetypeDataComponents {
      get;
    }

    /// <summary>
    /// model data component dependencies for this component
    /// </summary>
    HashSet<Model.DataComponent.Id> RequiredModelDataComponents {
      get;
    }
  }
}
