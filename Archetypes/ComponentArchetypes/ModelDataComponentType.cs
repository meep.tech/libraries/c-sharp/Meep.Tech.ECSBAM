﻿using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Static;

namespace Meep.Tech.Data.Base {
  public partial class Model {

    public partial class DataComponent {

      /// <summary>
      /// Used to hold base functionality for Archetypes for Model Data Components
      /// </summary>
      public abstract class Type<TComponentModelBase>
        : Component.Type<TComponentModelBase, Model.DataComponent.Type<TComponentModelBase>, Model.DataComponent.Id>,
          IModelDataComponentType
        where TComponentModelBase
          : IModelDataComponent,
            IModel<TComponentModelBase, Model.DataComponent.Type<TComponentModelBase>, Model.DataComponent.Id> 
      {
        protected Type(Model.DataComponent.Id componentId)
          : base(componentId, Components.ModelDatas) { }
      }
    }
  }
}