﻿using System;
using System.Linq;

namespace Meep.Tech.Data {
  #region Append and Replacement Extension Helpers

  /// <summary>
  /// Helper extensions for chaining
  /// </summary>
  public static class HashsetExtensions {

    /// <summary>
    /// Append an item to the OrderedSet and chain and return the same one.
    /// </summary>
    public static OrderedSet<Tkey> Append<Tkey>(this OrderedSet<Tkey> current, params Tkey[] keys) {
      foreach(Tkey key in keys) {
        current.Add(key);
      }

      return current;
    }

    /// <summary>
    /// remove an item from the OrderedSet and chain and return the same one.
    /// </summary>
    public static OrderedSet<Tkey> Without<Tkey>(this OrderedSet<Tkey> current, Tkey key) {
      current.Remove(key);
      return current;
    }

    /// <summary>
    /// Merge in one or multiuplle hash sets into this one, replacing duplicates. returns result for chaining
    /// </summary>
    public static OrderedSet<TKey> Merge<TKey>(this OrderedSet<TKey> current, params OrderedSet<TKey>[] hashsetsToMergeIn) {
      if (hashsetsToMergeIn == null) return current;
      OrderedSet<TKey> result = current;
      foreach (OrderedSet<TKey> hashset in hashsetsToMergeIn) {
        foreach (TKey entry in hashset) {
          if (result.Contains(entry)) {
            result.Remove(entry);
          }

          result.Add(entry);
        }
      }

      return result;
    }

    /// <summary>
    /// Merge in one or multiuplle hash sets, throwing an error on duplicates. returns result for chaining
    /// </summary>
    public static OrderedSet<TKey> AddAll<TKey>(this OrderedSet<TKey> current, params OrderedSet<TKey>[] hashsetsToMergeIn) {
      if (hashsetsToMergeIn == null) return current;
      OrderedSet<TKey> result = current;
      foreach (OrderedSet<TKey> hashset in hashsetsToMergeIn) {
        foreach (TKey entry in hashset) {
          result.Add(entry);
        }
      }

      return result;
    }

    /// <summary>
    /// Remove all items that match:
    /// </summary>
    public static OrderedSet<TKey> RemoveAll<TKey>(this OrderedSet<TKey> current, Func<TKey, bool> shouldRemoveEntry) {
      foreach (TKey entry in current.Reverse()) {
        if (shouldRemoveEntry(entry)) {
          current.Remove(entry);
        }
      }

      return current;
    }
  }

  #endregion
}
