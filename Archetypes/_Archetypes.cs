﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Serialization;
using System;
using System.Collections.Generic;

namespace Meep.Tech.Data.Static {

  /// <summary>
  /// Constants and statics related to all Archetypes.
  /// </summary>
  public static class Archetypes {

    /// <summary>
    /// All ids currently loaded into memory and registered to an archetype
    /// </summary>
    public static IReadOnlyDictionary<string, Archetype.Identity> Ids {
      get => _allIds;
    } static Dictionary<string, Archetype.Identity> _allIds
      = new Dictionary<string, Archetype.Identity>();

    /// <summary>
    /// All Archetypes currently loaded into memory
    /// </summary>
    public static IReadOnlyDictionary<Archetype.Identity, Archetype> All {
      get => _allArchetypes;
    } static Dictionary<Archetype.Identity, Archetype> _allArchetypes
      = new Dictionary<Archetype.Identity, Archetype>();

    /// <summary>
    /// All Archetype collections currently loaded into memory, indexed by the Archetype Base Type of the collection's system.type
    /// </summary>
    public static IReadOnlyDictionary<Type, Archetype.Collection> Collections {
      get => _allCollections;
    } internal static Dictionary<Type, Archetype.Collection> _allCollections
      = new Dictionary<Type, Archetype.Collection>();

    /// <summary>
    /// Archetype Base Type's System.Types indexed by each Archetype Type
    /// </summary>
    static Dictionary<Type, Type> _archetypeBaseTypes
      = new Dictionary<Type, Type>();

    /// <summary>
    /// Try to turn a system type into an archetype.
    /// false and null on failure to find one that matches
    /// </summary>
    public static TArchetype GetInstance<TArchetype>() where TArchetype : Archetype {
      if (_archetypeBaseTypes.TryGetValue(typeof(TArchetype), out Type archetypeBaseType)) {
        if (_allCollections.TryGetValue(archetypeBaseType, out Archetype.Collection archetypeCollection)) {
          return archetypeCollection.Get<TArchetype>();
        } else throw new Archetype.Collection.ArchetypeCollectionNotFoundException(archetypeBaseType.FullName, "Archetype Base Type");
      } else throw new Archetype.Collection.ArchetypeNotFoundException(typeof(TArchetype));
    }

    /// <summary>
    /// Register a new type of archetype with the global collections
    /// </summary>
    internal static void RegisterGlobaly(Archetype archetype) {
      _allIds.Add(archetype.Id.ExternalId, archetype.Id);
      _allArchetypes.Add(archetype.Id, archetype);
      if (archetype is IComponentArchetype) {
        Type baseComponentArchetype;
        switch (archetype) {
          case IModelDataComponentType _:
            baseComponentArchetype = typeof(IModelDataComponentType);
            break;
          case IArchetypeDataComponentType _:
            baseComponentArchetype = typeof(IArchetypeDataComponentType);
            break;
          case IArchetypeSystemComponentType _:
            baseComponentArchetype = typeof(IArchetypeSystemComponentType);
            break;
          default:
            throw new NotImplementedException($"Component Archetype: {archetype}, is not a ModelData, ArchetypeData, or ArchetypeSystem component.");
        }

        _archetypeBaseTypes.Add(
          archetype.GetType(),
          baseComponentArchetype
        );
      } else {
        _archetypeBaseTypes.Add(archetype.GetType(), archetype.BaseArchetypeType);
      }
    }

    /// <summary>
    /// Undo global registration
    /// </summary>
    internal static void DeRegisterGlobaly(Archetype archetype) {
      _allIds.Remove(archetype.Id.ExternalId);
      _allArchetypes.Remove(archetype.Id);
      _archetypeBaseTypes.Remove(archetype.GetType());
    }

    /// <summary>
    /// Try to turn a system type into an archetype.
    /// false and null on failure to find one that matches
    /// </summary>
    public static bool TryToGetAsArchetype(this Type achetypeType, out Archetype archetype) {
      if (_archetypeBaseTypes.TryGetValue(achetypeType, out Type archetypeBaseType)) {
        if (_allCollections.TryGetValue(archetypeBaseType, out Archetype.Collection archetypeCollection)) {
          archetype = archetypeCollection.GetI(achetypeType) as Archetype;
          return true;
        }
      }

      archetype = null;
      return false;
    }

    /// <summary>
    /// Try to turn a system type into an archetype.
    /// false and null on failure to find one that matches
    /// </summary>
    public static Archetype AsArchetype(this Type achetypeType) {
      if (_archetypeBaseTypes.TryGetValue(achetypeType, out Type archetypeBaseType)) {
        if (_allCollections.TryGetValue(archetypeBaseType, out Archetype.Collection archetypeCollection)) {
          return archetypeCollection.GetI(achetypeType) as Archetype;
        } else throw new Archetype.Collection.ArchetypeCollectionNotFoundException(archetypeBaseType.FullName, "Archetype Base Type");
      } else throw new Archetype.Collection.ArchetypeNotFoundException(achetypeType);
    }
  }

  /// <summary>
  /// Archetype instance helper class:
  /// </summary>
  public static class Archetype<TArchetype> where TArchetype : Archetype {

    /// <summary>
    /// The singleton instance of the type of archetype
    /// </summary>
    public static TArchetype Instance
      => Archetypes.GetInstance<TArchetype>();

    /// <summary>
    /// The singleton instance of the type of archetype
    /// </summary>
    /// <returns></returns>
    public static TArchetype @_
      => Archetypes.GetInstance<TArchetype>();

    /// <summary>
    /// The singleton instance of the type of archetype
    /// </summary>
    /// <returns></returns>
    public static TArchetypeBase As<TArchetypeBase>()
      where TArchetypeBase : Archetype
        => Archetypes.GetInstance<TArchetype>() as TArchetypeBase;

    /// <summary>
    /// The singleton instance of the type of archetype
    /// </summary>
    /// <returns></returns>
    public static TArchetype Self
      => Archetypes.GetInstance<TArchetype>();

    /// <summary>
    /// The singleton instance of the type of archetype
    /// </summary>
    /// <returns></returns>
    public static TArchetype Type
      => Archetypes.GetInstance<TArchetype>();

    /// <summary>
    /// The system type of this archetype
    /// </summary>
    /// <returns></returns>
    public static Type SystemType
      => typeof(TArchetype);

    /// <summary>
    /// Get the id singleton for this archetype
    /// </summary>
    public static Archetype.Identity Id
      => Instance.Id;

    /// <summary>
    /// Get the singleton instance for this type of archetype
    /// </summary>
    /// <returns></returns>
    public static TArchetype Get()
      => Archetypes.GetInstance<TArchetype>();

    /// <summary>
    /// Helper to make a child model with params:
    /// </summary>
    public static TDesiredModel Make<TDesiredModel>(Model.Params @params = null)
      where TDesiredModel : Model
        => Instance.MakeI(@params) as TDesiredModel;

    /// <summary>
    /// Helper to make a child model with params:
    /// </summary>
    public static TDesiredModel Make<TDesiredModel>(Dictionary<Param, object> @params)
      where TDesiredModel : Model
        => Instance.MakeI(@params) as TDesiredModel;

    /// <summary>
    /// Helper to make a child model from serialized datas:
    /// </summary>
    public static TDesiredModel Make<TDesiredModel>(Serializer.SerializedData serializedData, int? depthLimit = null)
      where TDesiredModel : Model
        => Instance.MakeI(serializedData, depthLimit) as TDesiredModel;

    /// <summary>
    /// Helper to make a child model from serialized datas:
    /// </summary>
    public static TDesiredModel Make<TDesiredModel>(Serializer.SerializedData serializedData, Dictionary<Param, object> configurationParams, int? initialDataLoadDepthLimit = null)
      where TDesiredModel : Model
        => Instance.MakeI(serializedData, configurationParams, initialDataLoadDepthLimit) as TDesiredModel;

    /// <summary>
    /// Helper to make a child model from serialized datas:
    /// </summary>
    public static TDesiredModel Make<TDesiredModel>(Serializer.SerializedData serializedData, Model.Params configurationParams, int? initialDataLoadDepthLimit = null)
      where TDesiredModel : Model
        => Instance.MakeI(serializedData, configurationParams, initialDataLoadDepthLimit) as TDesiredModel;
  }
}
