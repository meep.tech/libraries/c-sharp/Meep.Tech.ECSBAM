﻿using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Loading;
using Meep.Tech.Data.Serialization;
using Meep.Tech.Data.Static;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Meep.Tech.Data.Base {

  public partial class Archetype {

    /// <summary>
    /// Base generic class for an Archetyp Collection
    /// </summary>
    public abstract class Collection {

      /// <summary>
      /// Register a new archetype. Can only be used until the archetypes are finalized.
      /// </summary>
      /// <param name="archetype"></param>
      public abstract void Register(Archetype archetype);

      /// <summary>
      /// Get the archetype as a basic interface:
      /// </summary>
      public abstract IArchetype GetI(Type type);

      /// <summary>
      /// Get the archetype as a basic interface:
      /// </summary>
      public abstract TArchetype Get<TArchetype>()
        where TArchetype : Archetype;

      /// <summary>
      /// Get the archetype as a basic interface:
      /// </summary>
      public abstract IArchetype GetI(IArchetypeId id);

      /// <summary>
      /// Get the archetype as a basic interface:
      /// </summary>
      public abstract IArchetype GetI(string externalId);

      /// <summary>
      /// Get the archetype as a basic interface:
      /// </summary>
      public abstract IArchetype GetI(int internalId);

      #region Exceptions

      public sealed class ArchetypeNotFoundException : KeyNotFoundException {

        public ArchetypeNotFoundException(string message) : base(message) { }

        public ArchetypeNotFoundException(Collection collection, string archetypeLookupKey) : base(
          $"Could not find Archetype with the unique identifier: {archetypeLookupKey}, in the Archetype Collection: {collection}."
        ) { }

        public ArchetypeNotFoundException(Collection collection, string archetypeLookupKey, Exception internalException) : base(
          $"Could not find Archetype with the unique identifier: {archetypeLookupKey}, in the Archetype Collection: {collection}.",
          internalException
        ) { }

        public ArchetypeNotFoundException(Collection collection, int internalId, Exception internalException) : base(
          $"Could not find Archetype with the internal id of: {internalId}, in the Archetype Collection: {collection}.",
          internalException
        ) { }

        public ArchetypeNotFoundException(Type archetypeType) : base(
          $"Could not find an Archetype Base Type registered for the System Type: {archetypeType}"
        ) { }

        public ArchetypeNotFoundException(string message, Exception innerException) : base(message, innerException) { }
      }

      public sealed class ArchetypeCollectionNotFoundException : KeyNotFoundException {

        public ArchetypeCollectionNotFoundException(string message) : base(message) { }

        public ArchetypeCollectionNotFoundException(string archetypeLookupKey, string keyType) : base(
          $"Could not find the the Archetype Collection with the key: {archetypeLookupKey}, of type {keyType}."
        ) { }

        public ArchetypeCollectionNotFoundException(string message, Exception innerException) : base(message, innerException) { }
      }

      public sealed class ArchetypeTypeMismatchException : ArrayTypeMismatchException {

        public ArchetypeTypeMismatchException(string message) : base(message) { }

        public ArchetypeTypeMismatchException(Collection incorrectCollection, Collection correctCollection, string archetypeLookupKey) : base(
          $"Archetype Type Mismatch. Archetype with the unique identifier: {archetypeLookupKey}, is not in the Archetype Collection: {incorrectCollection}. That Archetype should be under the correct collection {correctCollection}"
        ) { }

        public ArchetypeTypeMismatchException(Collection incorrectCollection, Collection correctCollection, string archetypeLookupKey, Exception internalException) : base(
          $"Archetype Type Mismatch. Archetype with the unique identifier: {archetypeLookupKey}, is not in the Archetype Collection: {incorrectCollection}. That Archetype should be under the correct collection {correctCollection}",
          internalException
        ) { }

        public ArchetypeTypeMismatchException(Collection incorrectCollection, Collection correctCollection, int internalId, Exception internalException) : base(
          $"Archetype Type Mismatch. Archetype with the internal Id: {internalId}, is not in the Archetype Collection: {incorrectCollection}. That Archetype should be under the correct collection {correctCollection}",
          internalException
        ) { }

        public ArchetypeTypeMismatchException(string message, Exception innerException) : base(message, innerException) { }
      }

      #endregion
    }

    /// <summary>
    /// Used to register Archetypes into collections based on a base parent type.
    /// EX: if TArchetypeBase = Item.Type: the collection would contain all child archetypes of Item.Type, like Weapon:Item.Type for example.
    /// </summary>
    public class Collection<TModelBase, TArchetypeBase, TIdBase>
      : Collection,
        IEnumerable<TArchetypeBase>
      where TModelBase : IModel
      where TArchetypeBase : IArchetype
      where TIdBase : IArchetypeId {
      #region Initialization

      /// <summary>
      /// How many archetypes are in this collection
      /// </summary>
      int _initCount
        = 0;

      /// <summary>
      /// Initializaton of a new archeytpe collection:
      /// </summary>
      public Collection() {
        if (ArchetypeLoader.IsSealed) {
          throw new ArchetypeLoader.AttemptedRegistryWhileSealedException($"Cannot register a new archetype collection: {this}. Archetype Loading is Sealed.");
        }

        Archetypes._allCollections.Add(typeof(TArchetypeBase), this);
      }

      /// <summary>
      /// Register a new archetype.
      /// Only call this in
      /// </summary>
      public override void Register(Archetype archetype) {
        if (ArchetypeLoader.IsSealed && !(archetype.AllowSubtypeRuntimeRegistrations && ArchetypeLoader.Settings.AllowRuntimeTypeRegistrations)) {
          throw new ArchetypeLoader.AttemptedRegistryWhileSealedException($"Cannot register a new type:{archetype?.ExternalId ?? "MISSINGID"}, to the archetype collection: {this}. Archetype Loading is Sealed.");
        }

        TArchetypeBase archetypeToAdd;

        /// First try to add it to the collections:
        // Make sure it's our correct base type for this collection:
        try {
          archetypeToAdd = (TArchetypeBase)(archetype as IArchetype);
        } catch (Exception e) {
          throw new InvalidCastException($"Could not cast the Archetype: {archetype.ExternalId}, with Type: {archetype.GetType()} to the given base Archetype Type: {typeof(TArchetypeBase)}\nINTERNAL EXCEPTION:\n{e}");
        }

        /// Add to each collection, and remove on failure
        RegisterID(archetypeToAdd);
        RegisterName(archetypeToAdd);
        RegisterSystemType(archetypeToAdd);
        RegisterNewInternalId(archetypeToAdd);

        /// Finally; initalize the archetype after it's registered:
        try {
          Archetypes.RegisterGlobaly(archetype);
          archetype._initialize();
        } catch (Exception e) {
          _initCount--;
          Count--;

          _archetypesByInternalId.Remove(archetypeToAdd.InternalId);
          _archetypesByName.Remove(archetypeToAdd.Name);
          _archetypesByExternalId.Remove(archetypeToAdd.ExternalId);
          _archetypesByExternalId.Remove(archetypeToAdd.GetType().FullName);

          Archetypes.DeRegisterGlobaly(archetype);
          throw new Exception($"Failed to initialize new Archetype: {archetype.ExternalId}\nINTERNAL EXCEPTION:\n{e}");
        }
      }

      /// <summary>
      /// Re register an existing type to a child/sub collection
      /// </summary>
      /// <param name="archetype"></param>
      public void ReRegisterToSubCollection(Archetype archetype) {
        if (ArchetypeLoader.IsSealed) {
          throw new ArchetypeLoader.AttemptedRegistryWhileSealedException($"Cannot register a new type:{archetype?.ExternalId ?? "MISSINGID"}, to the archetype collection: {this}. Archetype Loading is Sealed.");
        }
        TArchetypeBase archetypeToAdd;

        /// First try to add it to the collections:
        // Make sure it's our correct base type for this collection:
        try {
          archetypeToAdd = (TArchetypeBase)(archetype as IArchetype);
        } catch (Exception e) {
          throw new InvalidCastException($"Could not cast the Archetype: {archetype.ExternalId}, with Type: {archetype.GetType()} to the given base Archetype Type: {typeof(TArchetypeBase)}\nINTERNAL EXCEPTION:\n{e}");
        }

        /// Make sure it's already been registered
        if (!Archetypes.All.ContainsKey(archetypeToAdd.Id)) {
          throw new Exception($"Cannot re-register the archetype {archetype.ExternalId ?? "MISSINGID"}, to the collection: {this}. It has not been registered anywhere yet.");
        }

        /// Add to each collection, and remove on failure
        RegisterID(archetypeToAdd);
        RegisterName(archetypeToAdd);
        RegisterSystemType(archetypeToAdd);
        ReRegisterInternalId(archetypeToAdd);
      }

      /// Registration helper functions. KEEP THEM IN ORDER:

      void RegisterID(TArchetypeBase archetypeToAdd) {
        try {
          _archetypesByExternalId.Add(archetypeToAdd.ExternalId, archetypeToAdd);
        } catch (Exception e) {
          throw new ArgumentException($"Could not register the new Archetype: {archetypeToAdd.ExternalId}, with ExternalId: {archetypeToAdd.ExternalId}. Name is already in use for the given Archetype: {_archetypesByExternalId[archetypeToAdd.ExternalId].Name}\nINTERNAL EXCEPTION:\n{e}");
        }
      }

      void RegisterName(TArchetypeBase archetypeToAdd) {
        try {
          _archetypesByName.Add(archetypeToAdd.Name, archetypeToAdd);
        } catch (Exception e) {
          _archetypesByExternalId.Remove(archetypeToAdd.ExternalId);
          throw new ArgumentException($"Could not register the new Archetype: {archetypeToAdd.ExternalId}, with Name: {archetypeToAdd.Name}. Name is already in use for the given Archetype: {_archetypesByName[archetypeToAdd.Name].ExternalId}\nINTERNAL EXCEPTION:\n{e}");
        }
      }

      void RegisterSystemType(TArchetypeBase archetypeToAdd) {
        try {
          _archetypesByTypeFullName.Add(archetypeToAdd.GetType().FullName, archetypeToAdd);
        } catch (Exception e) {
          _archetypesByName.Remove(archetypeToAdd.Name);
          _archetypesByExternalId.Remove(archetypeToAdd.ExternalId);
          throw new ArgumentException($"Could not register the new Archetype: {archetypeToAdd.ExternalId}, with System.Type.FullName: {archetypeToAdd.GetType().FullName}. Type is already in use for the given Archetype: {_archetypesByTypeFullName[archetypeToAdd.GetType().FullName].ExternalId}\nINTERNAL EXCEPTION:\n{e}");
        }
      }

      // These two are either or:

      void RegisterNewInternalId(TArchetypeBase archetypeToAdd) {
        try {
          _archetypesByInternalId.Add(archetypeToAdd.InternalId, archetypeToAdd);
          _initCount++;
          Count++;
        } catch (Exception e) {
          Count--;
          _initCount--;
          _archetypesByName.Remove(archetypeToAdd.Name);
          _archetypesByExternalId.Remove(archetypeToAdd.ExternalId);
          _archetypesByTypeFullName.Remove(archetypeToAdd.GetType().FullName);
          throw new ArgumentException($"Could not register the new Archetype: {archetypeToAdd.ExternalId}, using Internal Id: {Count + 1}. Name is already in use for the given Archetype: {_archetypesByInternalId[Count + 1].ExternalId}\nINTERNAL EXCEPTION:\n{e}");
        }
      }

      void ReRegisterInternalId(TArchetypeBase archetypeToAdd) {
        try {
          _archetypesByInternalId.Add(archetypeToAdd.InternalId, archetypeToAdd);
          Count++;
        } catch (Exception e) {
          _archetypesByName.Remove(archetypeToAdd.Name);
          _archetypesByExternalId.Remove(archetypeToAdd.ExternalId);
          _archetypesByTypeFullName.Remove(archetypeToAdd.GetType().FullName);
          throw new ArgumentException($"Could not re-register the existing Archetype: {archetypeToAdd.ExternalId}, using Internal Id: {archetypeToAdd.InternalId}, to collection: {this}. The collection already has that internal id in use by: {_archetypesByInternalId[archetypeToAdd.InternalId].ExternalId}\nINTERNAL EXCEPTION:\n{e}");
        }
      }

      #endregion

      #region Data Access

      /// <summary>
      /// How many archetypes are in this collection
      /// </summary>
      public int Count {
        get;
        private set;
      }

      /// <summary>
      /// Readonly list of archetypes by internal ID
      /// </summary>
      public IReadOnlyDictionary<int, TArchetypeBase> ByInternalId {
        get => _archetypesByInternalId;
      }
      internal readonly Dictionary<int, TArchetypeBase> _archetypesByInternalId
      = new Dictionary<int, TArchetypeBase>();

      /// <summary>
      /// Readonly list of archetypes by name
      /// </summary>
      public IReadOnlyDictionary<string, TArchetypeBase> ByName {
        get => _archetypesByName;
      }
      internal readonly Dictionary<string, TArchetypeBase> _archetypesByName
      = new Dictionary<string, TArchetypeBase>();

      /// <summary>
      /// Readonly list of archetypes by external ID
      /// </summary>
      public IReadOnlyDictionary<string, TArchetypeBase> ByExternalId {
        get => _archetypesByExternalId;
      }
      internal readonly Dictionary<string, TArchetypeBase> _archetypesByExternalId
      = new Dictionary<string, TArchetypeBase>();

      /// <summary>
      /// Readonly list of archetypes by System.Type.FullName
      /// </summary>
      public IReadOnlyDictionary<string, TArchetypeBase> ByType {
        get => _archetypesByTypeFullName;
      }
      internal readonly Dictionary<string, TArchetypeBase> _archetypesByTypeFullName
      = new Dictionary<string, TArchetypeBase>();

      /// <summary>
      /// Get an archetype by internal ID:
      /// </summary>
      /// <returns></returns>
      public TArchetypeBase Get(int internalId)
        => Get<TArchetypeBase>(internalId);

      /// <summary>
      /// Get an archetype by internal ID:
      /// </summary>
      /// <returns></returns>
      public TDesiredArchetypeBase Get<TDesiredArchetypeBase>(int internalId) where TDesiredArchetypeBase : TArchetypeBase {
        try {
          return (TDesiredArchetypeBase)_archetypesByInternalId[internalId];
        } catch (KeyNotFoundException knfe) {
          throw new ArchetypeNotFoundException(this, internalId, knfe);
        };
      }

      /// <summary>
      /// Get an archetype by external ID:
      /// </summary>
      /// <returns></returns>
      public TDesiredArchetypeBase Get<TDesiredArchetypeBase>(string externalId) where TDesiredArchetypeBase : Archetype, TArchetypeBase {
        try {
          return _archetypesByExternalId[externalId] as TDesiredArchetypeBase;
        } catch (KeyNotFoundException knfe) {
          if (Archetypes.Ids.TryGetValue(externalId, out Archetype.Identity archetypeId)) {
            throw new ArchetypeTypeMismatchException(this, Archetypes.Collections[archetypeId.Archetype.BaseArchetypeType], archetypeId.ExternalId, knfe);
          }
          throw new ArchetypeNotFoundException(this, externalId, knfe);
        };
      }

      /// <summary>
      /// Get an archetype by the full ID object
      /// </summary>
      public TDesiredArchetypeBase Get<TDesiredArchetypeBase>(TIdBase archetypeId) where TDesiredArchetypeBase : Archetype, TArchetypeBase {
        try {
          return _archetypesByExternalId[archetypeId.ExternalId] as TDesiredArchetypeBase;
        } catch (KeyNotFoundException knfe) {
          if (Archetypes.Collections.TryGetValue(archetypeId.Archetype.BaseArchetypeType, out var Collection)) {
            throw new ArchetypeTypeMismatchException(this, Collection, archetypeId.ExternalId, knfe);
          }
          throw new ArchetypeNotFoundException(this, archetypeId.ExternalId, knfe);
        };
      }

      /// <summary>
      /// Get an archetype by the system type
      /// </summary>
      public TDesiredArchetypeBase Get<TDesiredArchetypeBase>(Type systemType) where TDesiredArchetypeBase : Archetype, TArchetypeBase {
        try {
          return _archetypesByTypeFullName[systemType.FullName] as TDesiredArchetypeBase;
        } catch (KeyNotFoundException knfe) {
          if (Archetypes.Collections.TryGetValue(systemType, out Collection correctCollection)) {
            throw new ArchetypeTypeMismatchException(this, correctCollection, $"System.Type: {systemType.FullName}", knfe);
          }
          throw new ArchetypeNotFoundException(this, $"System.Type: {systemType.FullName}", knfe);
        };
      }


      /// <summary>
      /// Generic version
      /// </summary>
      public TArchetypeBase Get(Type systemType) {
        try {
          return _archetypesByTypeFullName[systemType.FullName];
        } catch (KeyNotFoundException knfe) {
          if (systemType.TryToGetAsArchetype(out Archetype archetype)) {
            throw new ArchetypeTypeMismatchException(this, Archetypes.Collections[archetype.BaseArchetypeType], $"System.Type: {systemType.FullName}", knfe);
          }
          throw new ArchetypeNotFoundException(this, $"System.Type: {systemType.FullName}", knfe);
        };
      }

      /// <summary>
      /// Get an archetype by the full ID object
      /// </summary>
      public TArchetypeBase Get<TArchetypeIdType>(TArchetypeIdType archetypeId) where TArchetypeIdType : TIdBase {
        try {
          return _archetypesByExternalId[archetypeId.ExternalId];
        } catch (KeyNotFoundException knfe) {
          if (Archetypes.Collections.TryGetValue(archetypeId.Archetype.BaseArchetypeType, out var Collection)) {
            throw new ArchetypeTypeMismatchException(this, Collection, archetypeId.ExternalId, knfe);
          }
          throw new ArchetypeNotFoundException(this, archetypeId.ExternalId, knfe);
        };
      }

      /// <summary>
      /// Get an archetype by the full ID object
      /// </summary>
      public TArchetypeBase Get(TIdBase archetypeId) {
        try {
          return _archetypesByExternalId[archetypeId.ExternalId];
        } catch (KeyNotFoundException knfe) {
          if (Archetypes.Collections.TryGetValue(archetypeId.Archetype.BaseArchetypeType, out var Collection)) {
            throw new ArchetypeTypeMismatchException(this, Collection, archetypeId.ExternalId, knfe);
          }
          throw new ArchetypeNotFoundException(this, archetypeId.ExternalId, knfe);
        };
      }

      /// <summary>
      /// Get an archetype by the full ID object
      /// </summary>
      public TArchetypeBase Get(string externalId) {
        try {
          return _archetypesByExternalId[externalId];
        } catch (KeyNotFoundException knfe) {
          if (Archetypes.Ids.TryGetValue(externalId, out Archetype.Identity archetypeId)) {
            throw new ArchetypeTypeMismatchException(this, Archetypes.Collections[archetypeId.Archetype.BaseArchetypeType], archetypeId.ExternalId, knfe);
          }
          throw new ArchetypeNotFoundException(this, externalId, knfe);
        };
      }



      /// <summary>
      /// Generic version
      /// </summary>
      public override IArchetype GetI(Type systemType) {
        try {
          return _archetypesByTypeFullName[systemType.FullName];
        } catch (KeyNotFoundException knfe) {
          if (Archetypes.Collections.TryGetValue(systemType, out Collection correctCollection)) {
            throw new ArchetypeTypeMismatchException(this, correctCollection, $"System.Type: {systemType.FullName}", knfe);
          }
          throw new ArchetypeNotFoundException(this, $"System.Type: {systemType.FullName}", knfe);
        };
      }


      /// <summary>
      /// Generic version
      /// </summary>
      /// <returns></returns>
      public override IArchetype GetI(int internalId) {
        try {
          return _archetypesByInternalId[internalId];
        } catch (KeyNotFoundException knfe) {
          throw new ArchetypeNotFoundException(this, internalId, knfe);
        };
      }


      /// <summary>
      /// Get an archetype by the full ID object
      /// </summary>
      public override IArchetype GetI(IArchetypeId archetypeId) {
        try {
          return _archetypesByExternalId[archetypeId.ExternalId];
        } catch (KeyNotFoundException knfe) {
          if (Archetypes.Collections.TryGetValue(archetypeId.Archetype.BaseArchetypeType, out Collection correctCollection)) {
            throw new ArchetypeTypeMismatchException(this, correctCollection, archetypeId.ExternalId, knfe);
          }
          throw new ArchetypeNotFoundException(this, archetypeId.ExternalId, knfe);
        };
      }


      /// <summary>
      /// Get an archetype by the full ID object
      /// </summary>
      public override IArchetype GetI(string externalId) {
        try {
          return _archetypesByExternalId[externalId];
        } catch (KeyNotFoundException knfe) {
          if (Archetypes.Ids.TryGetValue(externalId, out Archetype.Identity archetypeId)) {
            throw new ArchetypeTypeMismatchException(this, Archetypes.Collections[archetypeId.Archetype.BaseArchetypeType], archetypeId.ExternalId, knfe);
          }
          throw new ArchetypeNotFoundException(this, externalId, knfe);
        };
      }

      /// <summary>
      /// Get as helper
      /// </summary>
      public override TArchetype Get<TArchetype>() {
        return Get(typeof(TArchetype)) as TArchetype;
      }


      #endregion

      #region  Model Construction Shortcuts

      /// <summary>
      /// Helper to make a child model with params:
      /// </summary>
      /// <param name="params"></param>
      /// <returns></returns>
      public virtual TDesiredModel Make<TDesiredModel>(TIdBase id, Dictionary<Param, object> @params = null)
        where TDesiredModel : TModelBase
         => (TDesiredModel)id.Archetype.MakeI(@params);

      /// <summary>
      /// Helper to make a child model with params:
      /// </summary>
      /// <param name="params"></param>
      /// <returns></returns>
      public virtual TModelBase Make(TIdBase id, Dictionary<Param, object> @params = null)
        => (TModelBase)id.Archetype.MakeI(@params);

      /// <summary>
      /// Helper to make a child model from serialized datas:
      /// </summary>
      public virtual TModelBase Make(Serializer.SerializedData serializedData, int? depthLimit = null)
        => (TModelBase)
          (Get(serializedData.sqlData[Serializer.SQLTypeColumnName] as string)
            .MakeI(serializedData, depthLimit));

      /// <summary>
      /// Make a model from a json string.
      /// </summary>
      /// <returns></returns>
      public virtual TModelBase MakeFromJson(string jsonString)
        => Make(Serializer.SerializedData.FromJson(jsonString));

      #endregion

      #region IEnumerator

      public IEnumerator<TArchetypeBase> GetEnumerator() =>
        ByInternalId.Values.GetEnumerator();

      IEnumerator IEnumerable.GetEnumerator()
        => GetEnumerator();

      #endregion
    }
  }
}