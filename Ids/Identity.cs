﻿using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Static;
using System;
using System.Collections.Generic;

namespace Meep.Tech.Data.Base {
  public partial class Archetype {

    /// <summary>
    /// An archetype Id.
    /// These should be declared as static items,
    /// used to index Archetypes.
    /// </summary>
    public class Identity
      : Enumeration<Identity>,
        IArchetypeId 
    {

      /// <summary>
      /// Models linked to ids for models without archetypes
      /// </summary>
      internal static Dictionary<Identity, System.Type> _systemTypesByModelTypeIdForDefaultCtorTypes
        = new Dictionary<Identity, System.Type>();

      /// <summary>
      /// Model base types linked to ids for models without archetypes, reverse of above.
      /// </summary>
      internal static Dictionary<System.Type, Identity> _defaultArchetypelessTypeIdsByModelBaseTypeForDefaultCtorTypes
        = new Dictionary<Type, Identity>();

      /// <summary>
      /// Get the archetype registered to this ID
      /// </summary>
      public Archetype Archetype
        => Archetypes.All.TryGetValue(this, out Archetype archetype)
          ? archetype
          : null;

      /// <summary>
      /// The System.Type of the archetype this Id represents
      /// </summary>
      public Type ArchetypeType
        => Archetype?.GetType();

      /// <summary>
      /// The base type of the model associated with this id or the archetype it links to
      /// </summary>
      public Type ModelBaseType
        => ArchetypeType ?? _systemTypesByModelTypeIdForDefaultCtorTypes[this];

      #region Initialization

      /// <summary>
      /// Make an archetype ID
      /// </summary>
      public Identity(string name, string externalIdPrefix)
        : base(name, externalIdPrefix) { }

      /// <summary>
      /// Make an archetype ID
      /// </summary>
      public Identity(string name, System.Type baseSealedModelType)
        : base(name, baseSealedModelType.Name) 
      {
        _systemTypesByModelTypeIdForDefaultCtorTypes.Add(
          this,
          baseSealedModelType
        );
        _defaultArchetypelessTypeIdsByModelBaseTypeForDefaultCtorTypes.Add(
          baseSealedModelType,
          this
        );
      }

      #endregion

      #region Hash and Equality

      public override int GetHashCode() {
        return InternalId.GetHashCode();
      }

      public override bool Equals(object obj) {
        return obj is Identity id
           ? id.InternalId == InternalId
           : obj == null && this == null;
      }

      #endregion
    }
  }
}
