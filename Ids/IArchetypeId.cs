﻿using Meep.Tech.Data.Base;

namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// Simple interface for an archetype ID
  /// </summary>
  public interface IArchetypeId {

    /// <summary>
    /// The name of this archetype. Names must be unique by ArchetypeBaseType
    /// </summary>
    public string Name {
      get;
    }

    /// <summary>
    /// The assigned internal id of this archetype. This is only consistend within the current runtime and execution.
    /// </summary>
    public int InternalId {
      get;
    }

    /// <summary>
    /// The perminant and unique external id
    /// </summary>
    public string ExternalId {
      get;
    }

    /// <summary>
    /// This Id's associated archetype:
    /// </summary>
    Archetype Archetype {
      get;
    }
  }
}
