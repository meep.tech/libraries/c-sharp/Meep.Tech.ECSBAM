﻿using System;

namespace Meep.Tech.Data {

  /// <summary>
  /// Represents a param used to build a model
  /// </summary>
  public class Param : Enumeration<Param> {

    /// <summary>
    /// What type of param this is.
    /// </summary>
    public Type ValueType {
      get;
    }

    /// <summary>
    /// The basic unique ID of a model
    /// </summary>
    public static Param UniqueId { 
      get; 
    } = new Param("UniqueId", typeof(string));

    /// <summary>
    /// A token that can be used for RNG seeding/naming etc.
    /// </summary>
    public static Param CreatorToken { 
      get; 
    } = new Param("Creator Token", typeof(string));

    /// <summary>
    /// The basic unique ID of the owner/parent model of this model
    /// </summary>
    public static Param OwnerId {
      get;
    } = new Param("OwnerId", typeof(string));

    /// <summary>
    /// Make subtypes with this
    /// </summary>
    protected Param(string name, Type type, string externalIdEnding = null) : base($"Param.{externalIdEnding ?? name}", name) {
      ValueType = type;
    }
  }
}
