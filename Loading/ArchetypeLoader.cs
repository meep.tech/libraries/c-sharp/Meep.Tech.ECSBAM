﻿using Meep.Tech.Data.Attributes.Archetypes;
using Meep.Tech.Data.Attributes.Models;
using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Meep.Tech.Data.Loading {

  /// <summary>
  /// Used to load archetypes at startup
  /// </summary>
  public static class ArchetypeLoader {

    /// <summary>
    /// Settings for the loader
    /// </summary>
    public static class Settings {

      /// <summary>
      /// Assemblies that should be included in the loading that are built in.
      /// This helps prevent assemblies from not being loaded yet on initial searches
      /// </summary>
      public static ICollection<Assembly> PreLoadAssemblies
        = new List<Assembly>();

      /// <summary>
      /// If a single archetype not being initialized should throw a fatal.
      /// </summary>
      public static bool FatalOnCannotInitializeArchetype
        = false;

      /// <summary>
      /// The prefix to limit assemblies to for loading archetypes
      /// </summary>
      public static string ArchetypeAssembliesPrefix
        = "";

      /// <summary>
      /// How many times to re-run initialization to account for types that require others
      /// </summary>
      public static short InitializationAttempts
        = 10;

      /// <summary>
      /// How many times to attempt to run finalization on remaining initializing types
      /// </summary>
      public static short FinalizationAttempts
        = 1;

      /// <summary>
      /// How many times to loop though components to initialize them accounting for dependency misses
      /// </summary>
      public static int ComponentInitializationAttempts
        = 4;

      /// <summary>
      /// Overrideable bool to allow runtime registrations of types that set AllowSubtypeRuntimeRegistrations to true.
      /// </summary>
      public static bool AllowRuntimeTypeRegistrations
        = false;
    }

    /// <summary>
    /// If all archetypes have been initialized and the loader is finished.
    /// Once this is true, you cannot modify archetypes or their collections anymore.
    /// </summary>
    public static bool IsSealed {
      get;
      private set;
    } = false;

    /// <summary>
    /// The types we need to construct and map data to
    /// </summary>
    static List<Type> UninitializedTypes
      = new List<Type>();

    /// <summary>
    /// The types we need to construct and map data to
    /// </summary>
    static List<Archetype> InitializedTypes
      = new List<Archetype>();

    /// <summary>
    /// How many initalization attempts are remaining
    /// </summary>
    static int RemainingInitializationAttempts 
      = Settings.InitializationAttempts;

    /// <summary>
    /// How many finalization attempts are remaining
    /// </summary>
    static int RemainingFinalizationAttempts 
      = Settings.FinalizationAttempts;

    /// <summary>
    /// Try to load all archetypes, using the Settings
    /// </summary>
    public static void TryToLoadAllArchetypes() {
      Initialize();

      while(RemainingInitializationAttempts-- > 0 && UninitializedTypes.Count > 0) {
        TryToCompleteInitialization();
      }

      while(RemainingFinalizationAttempts-- > 0 && InitializedTypes.Count > 0) {
        TryToFinishAllInitalizedTypes();
      }

      IsSealed = true;
    }

    /// <summary>
    /// Make sure the loader isn't sealed.
    /// </summary>
    public static bool VerifyIsNotSealed(string errorMessage) {
      if (IsSealed) {
        throw new ArchetypeDataIsSealedException($"ARCHETYPE LOAD ERROR: {errorMessage}. \nArchetype Loading is Sealed!!!");
      }

      return true;
    }

    /// <summary>
    /// Iinitialize all singleton types:
    /// </summary>
    static void Initialize() {
      // Setup newtonsoft json.net
      JsonConvert.DefaultSettings = () => new JsonSerializerSettings() {
        ContractResolver = new DefaultContractResolver() {
          IgnoreSerializableAttribute = false
        }
      };

      Serializer.Settings.ComponentJsonSerializerSettings = new JsonSerializerSettings() { 
        ContractResolver = new ShouldSerializeTypeInComponentsContractResolver() {
          IgnoreSerializableAttribute = false
        }
      };

      // pre-load
      Settings.PreLoadAssemblies.Count();
      Model.Builders.Count();

      // get all models and archetypes to init-load:
      IEnumerable<System.Type> applicableSystemTypes = AppDomain.CurrentDomain.GetAssemblies()
        // get all assemblies that begin with the specified prefix
        .Where(assembly => !assembly.IsDynamic && assembly.GetName().FullName.StartsWith(Settings.ArchetypeAssembliesPrefix))
        // get all types from those assemblies
        .SelectMany(assembly => assembly.GetExportedTypes())
        // if those type are archetypes, or models:
        .Where(
          type => !type.IsAbstract 
            && (typeof(Archetype).IsAssignableFrom(type)
              || (typeof(IModel).IsAssignableFrom(type) && !type.IsInterface))
            && !Attribute.IsDefined(type, typeof(DoNotBuildArchetypeInitiallyAttribute))
            && !Attribute.IsDefined(type, typeof(DoNotBuildArchetypeOrChildrenInitiallyAttribute))
        )
        // load components, then models, then component archetypes, then any other archetypes:
        .OrderBy(
          type => typeof(IComponentArchetype).IsAssignableFrom(type)
            ? 2
            : typeof(Archetype).IsAssignableFrom(type)
              ? 3
              : typeof(IComponent).IsAssignableFrom(type)
                ? 0
                : 1 // models are all that's left
        );

      // Attempt 0:
      foreach (System.Type systemType in applicableSystemTypes) {
        if (typeof(Archetype).IsAssignableFrom(systemType)) {
          System.Type archetypeSystemType = systemType;
          try {
            ConstructArchetypeFromSystemType(archetypeSystemType);
          } catch (FailedToConfigureNewArchetypeException fe) {
            UninitializedTypes.Add(archetypeSystemType);
          } catch (CannotInitializeArchetypeException ce) {
            if (Settings.FatalOnCannotInitializeArchetype) {
              throw ce;
            }
          }
        } else {
          RegisterModelType(systemType);
        }
      }
    }

    /// <summary>
    /// Register info about model types
    /// </summary>
    static void RegisterModelType(Type systemType) {
      Type baseClass = null;
      IEnumerable<Type> bases = null;
      bool isBuilderBased = false;

      // if it's recorded it's easy
      if (systemType.IsAssignableToGeneric(typeof(IModel<,,>))) {
        isBuilderBased = systemType.IsAssignableToGeneric(typeof(Model<>));

        bases = systemType.GetInheritedGenericTypes(typeof(IModel<,,>));
        baseClass ??= systemType.GetInheritedGenericTypes(typeof(IModel<,,>)).First();
      } // if not, we suffer and must loop
     
      if (baseClass == null) {
        Type currentType = systemType;
        while (currentType.BaseType != typeof(Model) && currentType.BaseType != typeof(object) && !currentType.IsValueType) {
          currentType = currentType.BaseType;
        }
        baseClass = currentType;
      }

      // builder based ones need their builder to be initated:
      if (isBuilderBased) {
        Type builderType = bases.Skip(1).First();
        ConstructArchetypeFromSystemType(builderType);
      }

      IExtensionModelMethods._modelBaseTypes.Add(
        systemType,
        baseClass
      );
      Serializer.Cache.GetDataModelInfo(systemType);
    }

    /// <summary>
    /// Try to initialize any archetypes that failed:
    /// </summary>
    static void TryToCompleteInitialization() {
      UninitializedTypes.RemoveAll(archetypeSystemType => {
        try {
          ConstructArchetypeFromSystemType(archetypeSystemType);
          return true;
        } catch (FailedToConfigureNewArchetypeException fe) {
          return false;
        } catch (CannotInitializeArchetypeException ce) {
          if (Settings.FatalOnCannotInitializeArchetype) {
            throw ce;
          }

          return true;
        }
      });
    }

    /// <summary>
    /// Try to finish all remaining initialized archetypes:
    /// </summary>
    static void TryToFinishAllInitalizedTypes() {
      InitializedTypes.RemoveAll(archetype => {
        try {
          archetype._finalize();

          return true;
        } // attempt failure: 
        catch (FailedToConfigureNewArchetypeException e) {
          Debugger.LogError($"Failed on attempt #{Settings.FinalizationAttempts - RemainingFinalizationAttempts} to construct new Archetype of type: {archetype} due to unknown internal error. \n ---------- \n Will retry \n ---------- \n. \nINTERNAL ERROR: {e}");

          return false;
        } catch (CannotInitializeArchetypeException e) {
          Debugger.LogError($"Cannot finish archetype of type: {archetype} due to CannotInitializeArchetypeException. \n ---------- \n Will Not Retry \n ---------- \n INNER EXCEPTION:\n {e}" + $"\n{e}");
          if (Settings.FatalOnCannotInitializeArchetype) {
            throw e;
          }

          return true;
        } catch (Exception e) {
          Debugger.LogError($"Cannot finish archetype of type: {archetype} Due to unknown inner exception. \n ---------- \n Will Not Retry \n ---------- \n." + $"\n{e}");
          if (Settings.FatalOnCannotInitializeArchetype) {
            throw e;
          }

          return true;
        }
      });
    }

    /// <summary>
    /// Try to construct the archetype, which will register it with it's collections:
    /// </summary>
    static void ConstructArchetypeFromSystemType(System.Type archetypeSystemType) {
      // Get ctor
      ConstructorInfo archetypeConstructor = archetypeSystemType.GetConstructor(
          BindingFlags.Instance | BindingFlags.NonPublic,
          null,
          new Type[0],
          null
        );
      object[] ctorArgs = new object[0];

      // We first look for a private parameterless ctor, then for a protected ctor with one argumen which inherits from ArchetypeId.
      if (archetypeConstructor == null) {
        archetypeConstructor = archetypeSystemType.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic)
          .Where(ctor => ctor.GetParameters().Count() == 1 && typeof(Archetype.Identity).IsAssignableFrom(ctor.GetParameters()[0].ParameterType)).FirstOrDefault();
        ctorArgs = new object[] { null };

        if (archetypeConstructor == null) {
          throw new CannotInitializeArchetypeException($"Cannot initialize type: {archetypeSystemType?.FullName ?? "ERRORNULLTYPE"},\n  it does not impliment either:\n\t\t a private or protected parameterless constructor that takes no arguments,\n\t\t or a protected/private ctor that takes one argument that inherits from ArchetypeId that accepts the default of Null for singleton initialization.");
        }
      }

      /// Try to construct it
      try {
        object archetype = archetypeConstructor.Invoke(ctorArgs);
        // success:
        InitializedTypes.Add(archetype as Archetype);
      } // attempt failure: 
      catch (FailedToConfigureNewArchetypeException e) {
        string failureMessage;
        try {
          failureMessage = $"Failed on attempt #{Settings.InitializationAttempts - RemainingInitializationAttempts} to construct new Archetype of type: {archetypeSystemType.FullName} due to unknown internal error. \n ---------- \n Will retry \n ---------- \n.";
          Debugger.LogError(failureMessage + $"\nINNER EXCEPTION:\n {e}");
        } // fatal:
        catch (Exception ex) {
          string fatalMessage = $"Cannot initialize archetype of type: {archetypeSystemType?.FullName ?? "NULLTYPE"} for unknown reasons. \n ---------- \n Will Not Retry \n ---------- \n.";
          Debugger.LogError(fatalMessage + $"\nINNER EXCEPTION:\n {ex}");
          throw new CannotInitializeArchetypeException(fatalMessage, ex);
        }
        throw new FailedToConfigureNewArchetypeException(failureMessage, e);
      } catch (Exception e) {
        string fatalMessage = $"Cannot initialize archetype of type: {archetypeSystemType?.FullName ?? "NULLTYPE"} Due to unknown inner exception. \n ---------- \n Will Not Retry \n ---------- \n.";
        Debugger.LogError(fatalMessage + $"\nINNER EXCEPTION:\n {e}");
        throw new CannotInitializeArchetypeException(fatalMessage, e);
      }
    }

    /// <summary>
    /// Exeption thrown when you tried to register an archetype after the archetype loader has finished and has been sealed.
    /// </summary>
    public class AttemptedRegistryWhileSealedException : ArchetypeDataIsSealedException {
      public AttemptedRegistryWhileSealedException(string message) : base(message) { }
      public AttemptedRegistryWhileSealedException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// Exeption thrown when you tried to update an archtetypes data after the loader was sealed.
    /// </summary>
    public class AttemptedUpdateWhileSealedException : ArchetypeDataIsSealedException {
      public AttemptedUpdateWhileSealedException(string message) : base(message) { }
      public AttemptedUpdateWhileSealedException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// Exeption thrown when you tried to update an archtetypes data after the loader was sealed.
    /// </summary>
    public class AttemptedCreationWhileSealedException : ArchetypeDataIsSealedException {
      public AttemptedCreationWhileSealedException(string message) : base(message) { }
      public AttemptedCreationWhileSealedException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// Exeption thrown when you tried to do something you can only do while loading archetypes after the archetype loader has finished and has been sealed.
    /// </summary>
    public class ArchetypeDataIsSealedException : InvalidOperationException {
      public ArchetypeDataIsSealedException(string message) : base(message) { }
      public ArchetypeDataIsSealedException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// Exeption thrown when you fail to initialize or finalize an archetype. This will cause the loader to retry for things like missing dependencies that haven't loaded yet:
    /// </summary>
    public class FailedToConfigureNewArchetypeException : InvalidOperationException {
      public FailedToConfigureNewArchetypeException(string message) : base(message) { }
      public FailedToConfigureNewArchetypeException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// Exeption thrown when you fail to initialize an archetype. This will cause the loader to retry for things like missing dependencies that haven't loaded yet:
    /// </summary>
    public class MissingArchetypeDependencyException : FailedToConfigureNewArchetypeException {
      public MissingArchetypeDependencyException(string message) : base(message) { }
      public MissingArchetypeDependencyException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// Exeption thrown when you cannot to initialize an archetype. This will cause the loader to stop trying for this archetype and mark it as failed completely:
    /// </summary>
    public class CannotInitializeArchetypeException : InvalidOperationException {
      public CannotInitializeArchetypeException(string message) : base(message) { }
      public CannotInitializeArchetypeException(string message, Exception innerException) : base(message, innerException) { }
    }
  }
}
