﻿using Meep.Tech.Data.Static;
using System;
using System.Collections.Generic;
using Meep.Tech.Data.Base;

namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// Indicates this has data stores in Model Data 
  /// Can be used to create a struct model with struct based components if you want I guess.
  /// </summary>
  public interface IModelDataComponentStorage : IModel, IEnumerable<IModelDataComponent> {

    /// <summary>
    /// Equality helper
    /// </summary>
    public static bool Equals(IModelDataComponentStorage model, IModelDataComponentStorage other) {
      if (other?.GetType() == model.GetType()) {
        foreach (IModelDataComponent dataComponent in model.dataComponents.Values) {
          // check each child component that we need to:
          if (((IModelDataComponentType)dataComponent.archetypeId.Archetype).IncludeInParentModelEqualityChecks) {
            // if the other item doesn't have any components, is missing this component, or the other component doesn't equal the one from this model, it's not ==
            if (!other.hasComponent(dataComponent.archetypeId)
              || !dataComponent.Equals(other.getComponent(dataComponent.archetypeId))
            ) {
              return false;
            }
          }
        }

        return true;
      } else return false;
    }

    #region Component Access 

    /// <summary>
    /// External, readonly list of data components on the model:
    /// </summary>
    public virtual IReadOnlyDictionary<Model.DataComponent.Id, IModelDataComponent> dataComponents
      => _dataComponentsByArchetypeId;

    /// <summary>
    /// Internal access to modify the data components
    /// </summary>
    internal Dictionary<Model.DataComponent.Id, IModelDataComponent> _dataComponentsByArchetypeId
      => new Dictionary<Model.DataComponent.Id, IModelDataComponent>();

    /// <summary>
    /// Internal access to modify the data components
    /// </summary>
    internal Dictionary<System.Type, Model.DataComponent.Id> _dataComponentIdsByBaseType
      => new Dictionary<System.Type, Model.DataComponent.Id>();

    /// <summary>
    /// Check if this model has the given component.
    /// </summary>
    bool hasComponent(Model.DataComponent.Id componentType)
      => IModelDataComponentStorageMethods.hasComponent(this, componentType);

    /// <summary>
    /// Check if this model has the given component.
    /// </summary>
    bool hasComponent(IModelDataComponent component)
      => IModelDataComponentStorageMethods.hasComponent(this, component.archetypeId);

    /// <summary>
    /// Helper to try to get a component
    /// </summary>
    /// <returns>false if this model doesn't have the given component</returns>
    bool tryToGetComponent(Model.DataComponent.Id componentType, out IModelDataComponent component)
      => IModelDataComponentStorageMethods.tryToGetComponent(this, componentType, out component);

    /// <summary>
    /// Helper to try to get a component
    /// </summary>
    /// <returns>false if this model doesn't have the given component</returns>
    IModelDataComponent getComponent(Model.DataComponent.Id componentType)
      => IModelDataComponentStorageMethods.getComponent(this, componentType);

    #endregion

    #region Component Modification

    /// <summary>
    /// Add a component to this model
    /// </summary>
    void addComponent(IModelDataComponent component)
      => IModelDataComponentStorageMethods.addComponent(this, component);

    /// <summary>
    /// Update a component of the given type with an entirely new one
    /// </summary>
    void updateComponent(IModelDataComponent updatedComponent)
      => IModelDataComponentStorageMethods.updateComponent(this, updatedComponent);

    /// <summary>
    /// Update a component of the given type with an entirely new one
    /// </summary>
    void updateComponent(Model.DataComponent.Id componentType, Func<IModelDataComponent, IModelDataComponent> update)
      => IModelDataComponentStorageMethods.updateComponent(this, componentType, update);

    #endregion
  }

  public static class IModelDataComponentStorageMethods {

    #region Component Access 

    /// <summary>
    /// Check if this model has the given component.
    /// </summary>
    public static bool hasComponent(this IModelDataComponentStorage model, Model.DataComponent.Id componentType)
      => model.dataComponents.ContainsKey(componentType);

    /// <summary>
    /// Check if this model has the given component.
    /// </summary>
    public static bool hasComponent<TComponent>(
      this IModelDataComponentStorage model
    ) where TComponent : IModelDataComponent
      => model._dataComponentIdsByBaseType.ContainsKey(typeof(TComponent).GetBaseModelType());

    /// <summary>
    /// Helper to try to get a component
    /// </summary>
    /// <returns>false if this model doesn't have the given component</returns>
    public static bool tryToGetComponent(this IModelDataComponentStorage model, Model.DataComponent.Id componentType, out IModelDataComponent component)
      => model.dataComponents.TryGetValue(componentType, out component);

    /// <summary>
    /// Helper to try to get a component
    /// </summary>
    /// <returns>false if this model doesn't have the given component</returns>
    public static IModelDataComponent tryToGetComponent(this IModelDataComponentStorage model, Model.DataComponent.Id componentType)
      => model.dataComponents.TryGetValue(componentType, out IModelDataComponent component) ? component : default;

    /// <summary>
    /// Helper to try to get a component
    /// </summary>
    /// <returns>false if this model doesn't have the given component</returns>
    public static IModelDataComponent getComponent(this IModelDataComponentStorage model, Model.DataComponent.Id componentType)
      => model.dataComponents[componentType];

    /// <summary>
    /// Helper to try to get a component
    /// </summary>
    /// <returns>false if this model doesn't have the given component</returns>
    public static bool tryToGetComponent<TComponent>(this IModelDataComponentStorage model, out TComponent component)
      where TComponent : IModelDataComponent
    {
      if (model._dataComponentIdsByBaseType.TryGetValue(typeof(TComponent).GetBaseModelType(), out Model.DataComponent.Id dataComponent)) {
        component = (TComponent)(model.dataComponents[dataComponent]);
        return true;
      }

      component = default;
      return false;
    }

    /// <summary>
    /// Helper to try to get a component
    /// </summary>
    /// <returns>false if this model doesn't have the given component</returns>
    public static TComponent tryToGetComponent<TComponent>(this IModelDataComponentStorage model)
      where TComponent : IModelDataComponent
    {
      if (model._dataComponentIdsByBaseType.TryGetValue(typeof(TComponent).GetBaseModelType(), out Model.DataComponent.Id dataComponent)) {
        return (TComponent)(model.dataComponents[dataComponent]);
      }

      return default;
    }

    /// <summary>
    /// Helper to try to get a component
    /// </summary>
    /// <returns>false if this model doesn't have the given component</returns>
    public static TComponent getComponent<TComponent>(this IModelDataComponentStorage model)
      where TComponent : IModelDataComponent
        => (TComponent)
          (model.dataComponents[model._dataComponentIdsByBaseType[typeof(TComponent).GetBaseModelType()]]);

    #endregion

    #region Component Modification

    /// <summary>
    /// Add a component to this model
    /// </summary>
    public static void addComponent(this IModelDataComponentStorage model, IModelDataComponent component) {
      try {
        if (component is IOwned ownedComponent && model is IUnique uniqueParent) {
          ownedComponent.setOwner(uniqueParent.id);
        }
        model._dataComponentsByArchetypeId.Add(component.archetypeId, component);
        model._dataComponentIdsByBaseType.Add(component.getBaseModelType(), component.archetypeId);
      } catch (Exception e) {
        if (component == null) {
          throw new NullReferenceException($"Cannot add a null component.");
        }
        if (component.archetypeId == null) {
          throw new NullReferenceException($"Component is missing an archetype id, cannot add it to the model.");
        }
        throw new ArgumentException($"Could not add the component of type: {component.archetypeId.ExternalId} to the Model: {model?.ToString() ?? "!!NULLMODEL!!"}. The model already has the given Component. \nINTERNAL EXCEPTION:\n{e}");
      }
    }

    /// <summary>
    /// Add a component to this model
    /// </summary>
    public static void addOrUpdateComponent(this IModelDataComponentStorage model, IModelDataComponent component) {
      if (model.hasComponent(component)) {
        model.updateComponent(component);
      } else model.addComponent(component);
    }

    /// <summary>
    /// Update a component of the given type with an entirely new one
    /// </summary>
    public static void updateComponent(this IModelDataComponentStorage model, IModelDataComponent updatedComponent) {
      Model.DataComponent.Id componentId = updatedComponent.archetypeId;
      if (model.hasComponent(componentId)) {
        model._dataComponentsByArchetypeId[componentId] = updatedComponent;
        model._dataComponentIdsByBaseType[updatedComponent.getBaseModelType()] = updatedComponent.archetypeId;
      } else {
        throw new ArgumentException($"Could not update the component of type: {updatedComponent.archetypeId} to the Model: {model}, of type: {model.GetType()}. The model does not have a component of this type yet");
      }
    }

    /// <summary>
    /// Update a component of the given type with an entirely new one
    /// </summary>
    public static void updateComponent(this IModelDataComponentStorage model, Model.DataComponent.Id componentType, Func<IModelDataComponent, IModelDataComponent> update) {
      if (model.dataComponents.TryGetValue(componentType, out IModelDataComponent component)) {
        IModelDataComponent updatedComponent = update(component);
        model._dataComponentsByArchetypeId[componentType] = updatedComponent;
        model._dataComponentIdsByBaseType[componentType.GetType().GetBaseModelType()] = updatedComponent.archetypeId;
      } else {
        throw new ArgumentException($"Could not update the component of type: {componentType} to the Model: {model}, of type: {model.GetType()}. The model does not have a component of this type yet");
      }
    }

    #region System Execution

    /// <summary>
    /// try to a link system logic function if all the components exist.
    /// </summary>
    public static bool tryToExecute<
      TReturn,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent.Type<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.AdvancedLogic<TReturn> systemLogicFunction,
      out TReturn @return
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            @return = systemLogicFunction.Invoke(
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent,
              model,
              model.type
            );
            return true;
          }
        }
      }

      @return = default;
      return false;
    }

    /// <summary>
    /// try to a link system logic function if all the components exist.
    /// </summary>
    public static bool tryToExecute<
      TInput,
      TReturn,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent.Type<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.AdvancedFunction<TInput, TReturn> systemLogicFunction,
      out TReturn @return,
      TInput input = default
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            @return = systemLogicFunction.Invoke(
              input,
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent,
              model,
              model.type
            );
            return true;
          }
        }
      }

      @return = default;
      return false;
    }

    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static bool tryToExecute<
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent.Type<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.SimpleLogic systemLogicFunction
    ) 
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            systemLogicFunction.Invoke(
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent
            );
            return true;
          }
        }
      }

      return false;
    }

    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static bool tryToExecute<
      TInput,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent.Type<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.SimpleFunction<TInput> systemLogicFunction,
      TInput input = default
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            systemLogicFunction.Invoke(
              input,
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent
            );
            return true;
          }
        }
      }

      return false;
    }
    
    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static bool tryToExecute<
      TReturn,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent.Type<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.SimpleLogic<TReturn> systemLogicFunction,
      out TReturn @return
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            @return = systemLogicFunction.Invoke(
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent
            );
            return true;
          }
        }
      }

      @return = default;
      return false;
    }
    
    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static bool tryToExecute<
      TInput,
      TReturn,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent.Type<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.SimpleFunction<TInput, TReturn> systemLogicFunction,
      out TReturn @return,
      TInput input = default
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
        ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            @return = systemLogicFunction.Invoke(
              input,
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent
            );
            return true;
          }
        }
      }

      @return = default;
      return false;
    }

    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static bool tryToExecute<
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent.Type<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.AdvancedLogic systemLogicFunction
    ) 
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            systemLogicFunction.Invoke(
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent,
              model,
              model.type
            );
            return true;
          }
        }
      }

      return false;
    }

    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static bool tryToExecute<
      TInput,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent.Type<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.AdvancedFunction<TInput> systemLogicFunction,
      TInput input = default
    ) 
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            systemLogicFunction.Invoke(
              input,
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent,
              model,
              model.type
            );
            return true;
          }
        }
      }

      return false;
    }
   
    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static TReturn execute<
      TReturn,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent.Type<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.AdvancedLogic<TReturn> systemLogicFunction
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        Archetype modelComponentArchetype = typeof(TLinkedModelComponentArchetype).AsArchetype();
        IModelDataComponent modelComponent = storage.getComponent(modelComponentArchetype.Id as Model.DataComponent.Id);
        Archetype archetypeComponentArchetype = typeof(TLinkedArchetypeComponentArchetype).AsArchetype();
        IArchetypeDataComponent archetypeComponent = model.type.DataComponents[archetypeComponentArchetype.Id as Archetype.DataComponent.Id];

        return systemLogicFunction.Invoke(
          (TLinkedModelComponent)modelComponent,
          (TLinkedArchetypeComponent)archetypeComponent,
          model,
          model.type
        );
      } else throw new NotImplementedException("Cannot call tryToExecute on a type without .type implimented");
    }

    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static void execute<
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent.Type<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.SimpleLogic systemLogicFunction
    ) 
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        Archetype modelComponentArchetype = typeof(TLinkedModelComponentArchetype).AsArchetype();
        IModelDataComponent modelComponent = storage.getComponent(modelComponentArchetype.Id as Model.DataComponent.Id);
        Archetype archetypeComponentArchetype = typeof(TLinkedArchetypeComponentArchetype).AsArchetype();
        IArchetypeDataComponent archetypeComponent = model.type.DataComponents[archetypeComponentArchetype.Id as Archetype.DataComponent.Id];

        systemLogicFunction.Invoke((TLinkedModelComponent)modelComponent, (TLinkedArchetypeComponent)archetypeComponent);
      } else throw new NotImplementedException("Cannot call tryToExecute on a type without .type implimented");
    }
    
    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static TReturn execute<
      TReturn,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent.Type<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.SimpleLogic<TReturn> systemLogicFunction
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        Archetype modelComponentArchetype = typeof(TLinkedModelComponentArchetype).AsArchetype();
        IModelDataComponent modelComponent = storage.getComponent(modelComponentArchetype.Id as Model.DataComponent.Id);
        Archetype archetypeComponentArchetype = typeof(TLinkedArchetypeComponentArchetype).AsArchetype();
        IArchetypeDataComponent archetypeComponent = model.type.DataComponents[archetypeComponentArchetype.Id as Archetype.DataComponent.Id];

        return systemLogicFunction.Invoke((TLinkedModelComponent)modelComponent, (TLinkedArchetypeComponent)archetypeComponent);
      } else throw new NotImplementedException("Cannot call tryToExecute on a type without .type implimented");
    }

    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static void execute<
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent.Type<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.AdvancedLogic systemLogicFunction
    ) 
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        Archetype modelComponentArchetype = typeof(TLinkedModelComponentArchetype).AsArchetype();
        IModelDataComponent modelComponent = storage.getComponent(modelComponentArchetype.Id as Model.DataComponent.Id);
        Archetype archetypeComponentArchetype = typeof(TLinkedArchetypeComponentArchetype).AsArchetype();
        IArchetypeDataComponent archetypeComponent = model.type.DataComponents[archetypeComponentArchetype.Id as Archetype.DataComponent.Id];

        systemLogicFunction.Invoke(
          (TLinkedModelComponent)modelComponent,
          (TLinkedArchetypeComponent)archetypeComponent,
          model,
          model.type
        );
      } else throw new NotImplementedException("Cannot call tryToExecute on a type without .type implimented");
    }

    /// <summary>
    /// try to a link system logic function if all the components exist.
    /// </summary>
    public static bool tryToExecute<
      TReturn,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.AdvancedLogic<TReturn> systemLogicFunction,
      out TReturn @return
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            @return = systemLogicFunction.Invoke(
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent,
              model,
              model.type
            );
            return true;
          }
        }
      }

      @return = default;
      return false;
    }

    /// <summary>
    /// try to a link system logic function if all the components exist.
    /// </summary>
    public static bool tryToExecute<
      TInput,
      TReturn,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.AdvancedFunction<TInput, TReturn> systemLogicFunction,
      out TReturn @return,
      TInput input = default
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            @return = systemLogicFunction.Invoke(
              input,
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent,
              model,
              model.type
            );
            return true;
          }
        }
      }

      @return = default;
      return false;
    }

    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static bool tryToExecute<
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.SimpleLogic systemLogicFunction
    ) 
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            systemLogicFunction.Invoke(
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent
            );
            return true;
          }
        }
      }

      return false;
    }

    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static bool tryToExecute<
      TInput,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.SimpleFunction<TInput> systemLogicFunction,
      TInput input = default
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            systemLogicFunction.Invoke(
              input,
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent
            );
            return true;
          }
        }
      }

      return false;
    }
    
    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static bool tryToExecute<
      TReturn,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.SimpleLogic<TReturn> systemLogicFunction,
      out TReturn @return
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            @return = systemLogicFunction.Invoke(
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent
            );
            return true;
          }
        }
      }

      @return = default;
      return false;
    }
    
    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static bool tryToExecute<
      TInput,
      TReturn,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.SimpleFunction<TInput, TReturn> systemLogicFunction,
      out TReturn @return,
      TInput input = default
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
        ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            @return = systemLogicFunction.Invoke(
              input,
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent
            );
            return true;
          }
        }
      }

      @return = default;
      return false;
    }

    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static bool tryToExecute<
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.AdvancedLogic systemLogicFunction
    ) 
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            systemLogicFunction.Invoke(
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent,
              model,
              model.type
            );
            return true;
          }
        }
      }

      return false;
    }

    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static bool tryToExecute<
      TInput,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.AdvancedFunction<TInput> systemLogicFunction,
      TInput input = default
    ) 
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent
    {
      if (storage is IModel model) {
        if (storage.tryToGetComponent(
          typeof(TLinkedModelComponentArchetype).AsArchetype().Id as Model.DataComponent.Id,
          out IModelDataComponent modelComponent
        )) {
          if (model.type.DataComponents.TryGetValue(
            typeof(TLinkedArchetypeComponentArchetype).AsArchetype().Id as Archetype.DataComponent.Id,
            out IArchetypeDataComponent archetypeComponent
          )) {
            systemLogicFunction.Invoke(
              input,
              (TLinkedModelComponent)modelComponent,
              (TLinkedArchetypeComponent)archetypeComponent,
              model,
              model.type
            );
            return true;
          }
        }
      }

      return false;
    }
   
    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static TReturn execute<
      TReturn,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.AdvancedLogic<TReturn> systemLogicFunction
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        Archetype modelComponentArchetype = typeof(TLinkedModelComponentArchetype).AsArchetype();
        IModelDataComponent modelComponent = storage.getComponent(modelComponentArchetype.Id as Model.DataComponent.Id);
        Archetype archetypeComponentArchetype = typeof(TLinkedArchetypeComponentArchetype).AsArchetype();
        IArchetypeDataComponent archetypeComponent = model.type.DataComponents[archetypeComponentArchetype.Id as Archetype.DataComponent.Id];

        return systemLogicFunction.Invoke(
          (TLinkedModelComponent)modelComponent,
          (TLinkedArchetypeComponent)archetypeComponent,
          model,
          model.type
        );
      } else throw new NotImplementedException("Cannot call tryToExecute on a type without .type implimented");
    }

    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static void execute<
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.SimpleLogic systemLogicFunction
    ) 
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        Archetype modelComponentArchetype = typeof(TLinkedModelComponentArchetype).AsArchetype();
        IModelDataComponent modelComponent = storage.getComponent(modelComponentArchetype.Id as Model.DataComponent.Id);
        Archetype archetypeComponentArchetype = typeof(TLinkedArchetypeComponentArchetype).AsArchetype();
        IArchetypeDataComponent archetypeComponent = model.type.DataComponents[archetypeComponentArchetype.Id as Archetype.DataComponent.Id];

        systemLogicFunction.Invoke((TLinkedModelComponent)modelComponent, (TLinkedArchetypeComponent)archetypeComponent);
      } else throw new NotImplementedException("Cannot call tryToExecute on a type without .type implimented");
    }
    
    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static TReturn execute<
      TReturn,
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.SimpleLogic<TReturn> systemLogicFunction
    )
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        Archetype modelComponentArchetype = typeof(TLinkedModelComponentArchetype).AsArchetype();
        IModelDataComponent modelComponent = storage.getComponent(modelComponentArchetype.Id as Model.DataComponent.Id);
        Archetype archetypeComponentArchetype = typeof(TLinkedArchetypeComponentArchetype).AsArchetype();
        IArchetypeDataComponent archetypeComponent = model.type.DataComponents[archetypeComponentArchetype.Id as Archetype.DataComponent.Id];

        return systemLogicFunction.Invoke((TLinkedModelComponent)modelComponent, (TLinkedArchetypeComponent)archetypeComponent);
      } else throw new NotImplementedException("Cannot call tryToExecute on a type without .type implimented");
    }

    /// <summary>
    /// Execute a link system logic function
    /// </summary>
    public static void execute<
      TLinkSystemComponentModel,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >(
      this IModelDataComponentStorage storage,
      Archetype.LinkSystemComponent<
        TLinkSystemComponentModel,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >.AdvancedLogic systemLogicFunction
    ) 
      where TLinkSystemComponentModel
        : ILinkSystemComponent<
          TLinkSystemComponentModel,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
          ISystemComponent<TLinkSystemComponentModel>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent 
    {
      if (storage is IModel model) {
        Archetype modelComponentArchetype = typeof(TLinkedModelComponentArchetype).AsArchetype();
        IModelDataComponent modelComponent = storage.getComponent(modelComponentArchetype.Id as Model.DataComponent.Id);
        Archetype archetypeComponentArchetype = typeof(TLinkedArchetypeComponentArchetype).AsArchetype();
        IArchetypeDataComponent archetypeComponent = model.type.DataComponents[archetypeComponentArchetype.Id as Archetype.DataComponent.Id];

        systemLogicFunction.Invoke(
          (TLinkedModelComponent)modelComponent,
          (TLinkedArchetypeComponent)archetypeComponent,
          model,
          model.type
        );
      } else throw new NotImplementedException("Cannot call tryToExecute on a type without .type implimented");
    }

    #endregion

    #endregion
  }
}
