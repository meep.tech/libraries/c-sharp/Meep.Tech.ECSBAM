﻿namespace Meep.Tech.Data.Attributes.Models {
  public class DataModelWithExistingTableAttribute : DataModelAttribute {

    /// <summary>
    /// Link a model to an existing table by name.
    /// </summary>
    /// <param name="ModelTypeName"></param>
    public DataModelWithExistingTableAttribute(string ModelTypeName) 
      : base(ModelTypeName, null, null, null) {
    }
  }
}
