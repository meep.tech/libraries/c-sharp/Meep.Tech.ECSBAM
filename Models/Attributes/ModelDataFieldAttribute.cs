﻿using Newtonsoft.Json.Linq;
using Meep.Tech.Data.Serialization;
using System;
using Meep.Tech.Data.Base;

namespace Meep.Tech.Data.Attributes.Models {

  /// <summary>
  /// Attribute used to dictate a basic field or property on a model we should serialize
  /// </summary>
  [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = true)]
  public class ModelDataFieldAttribute : Attribute {

    /// <summary>
    /// The name of the field
    /// </summary>
    public virtual string Name {
      get;
    } = null;

    /// <summary>
    /// If this is light-level data. AKA small data needed to load the model and it's doc from the DB.
    /// EX: just a type and ID
    /// </summary>
    public bool IsASQLField {
      get;
    } = false;

    /// <summary>
    /// If we should skip deserializing this field onto it's model
    /// </summary>
    public bool SkipDuringDeserialization {
      get;
    } = false;

    /// <summary>
    /// If this field represents child models that may need to be fetched from their own docs 
    /// </summary>
    public bool IsChildModelField {
      get;
      internal set;
    } = false;

    /// <summary>
    /// Custom type to return for what this serializes to, if it changes from the type of the field
    /// </summary>
    public virtual Type CustomSerializeToType
      => _customSerializeToType;
    Type _customSerializeToType
        = null;

    /// <summary>
    /// If this has custom serialization
    /// </summary>
    public virtual bool HasCustomSerialization
      => false;

    /// <summary>
    /// If this has custom serialization
    /// </summary>
    public virtual bool HasCustomDeserialization
      => false;

    /// <summary>
    /// If this uses an advanced serialization function
    /// </summary>
    public virtual bool HasAdvancedSerialization
      => false;

    /// <summary>
    /// If this has custom serialization
    /// </summary>
    public virtual bool HasAdvancedJSONFieldDeserialization
      => false;

    /// <summary>
    /// If this has custom serialization
    /// </summary>
    public virtual bool HasAdvancedSqlFieldDeserialization
      => false;

    /// <summary>
    /// Optional, ovveride function for how this field is serialized
    /// </summary>
    public virtual Func<object, object> SerializationFunction { 
      get;
      protected set;
    }

    /// <summary>
    /// Optional, ovveride function for how this field is serialized
    /// </summary>
    public virtual Func<object, IUnique, object> AdvancedSerializationFunction { 
      get;
      protected set;
    }

    /// <summary>
    /// Optional, ovveride function for how this field is serialized
    /// </summary>
    public virtual Func<object, IUnique, JObject> AdvancedJSONSerializationFunction { 
      get;
      protected set;
    }

    /// <summary>
    /// Optional, ovveride function for how this field is deserialized
    /// Takes the serialized field json token, and the current model.
    /// Returns the updated model with the deserialized field.
    /// </summary>
    public virtual Func<JToken, ISerializeable, ISerializeable> AdvancedJSONDeserializationFunction { 
      get;
    }

    /// <summary>
    /// Optional, ovveride function for how this field is deserialized
    /// Takes the serialized field json token, and the current model.
    /// Returns the updated model with the deserialized field.
    /// </summary>
    public virtual Func<object, ISerializeable, ISerializeable> AdvancedSqlObjectDeserializationFunction { 
      get;
    }

    /// <summary>
    /// Override for deserialzing from any object. Usually for what sql returns.
    /// </summary>
    internal protected virtual Func<object, object> DeserializeObject {
      get;
      protected set;
    }

    /// <summary>
    /// override for deserializing from json
    /// </summary>
    internal protected virtual Func<JToken, object> DeserializeJSON {
      get;
      protected set;
    }

    /// <summary>
    /// Public version for making a simple model data field
    /// </summary>
    /// <param name="FieldName"></param>
    public ModelDataFieldAttribute(string FieldName = null, bool IsASQLField = false, bool SkipDuringDeserialization = false, Type CustomSerializeToType = null) 
      : this(FieldName, false, IsASQLField, SkipDuringDeserialization, CustomSerializeToType) { }

    /// Internal version for making more complex custom fields
    /// <param name="FieldName">Override name for the field</param>
    /// <param name="serializationFunction">Optional, ovveride function for how this field is serialized</param>
    /// <param name="deserializationFunction">Optional, ovveride function for how this field is deserialized</param>
    internal ModelDataFieldAttribute(
      string FieldName,
      bool IsChildModelField,
      bool IsASQLField,
      bool SkipDuringDeserialization,
      Type CustomSerializeToType = null
    ) {
      Name ??= FieldName ?? Name;
      this.IsASQLField = IsASQLField;
      this.IsChildModelField = IsChildModelField;
      this.SkipDuringDeserialization = SkipDuringDeserialization;
      _customSerializeToType ??= CustomSerializeToType ?? _customSerializeToType;
    }

    public override bool Equals(object obj)
      => obj is ModelDataFieldAttribute other
        && other.GetType() == GetType()
        && other.Name == Name
        && other.IsASQLField == IsASQLField
        && other.IsChildModelField == IsChildModelField
        && other.CustomSerializeToType == CustomSerializeToType;
  }
}
