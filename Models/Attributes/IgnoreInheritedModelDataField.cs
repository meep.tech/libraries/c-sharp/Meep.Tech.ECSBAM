﻿using System;

namespace Meep.Tech.Data.Attributes.Models {

  /// <summary>
  /// Makes an field ignored by serialization even if a parent class had it set to serialize
  /// </summary>
  [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = true)]
  public class IgnoreInheritedModelDataField : Attribute { }
}
