﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Serialization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.Data.Attributes.Models {

  /// <summary>
  /// Represents a field with different serialization and deserialization depending on light loading vs full loading, as it's a field that contains other models.
  /// </summary>
  public class ChildModelFieldAttribute : CustomModelDataFieldAttribute {

    /// <summary>
    /// If we want to prevent this child field from ever light-loading
    /// </summary>
    public bool IsUnique {
      get;
    } = false;

    /// <summary>
    /// Model Type
    /// </summary>
    public Type ModelType {
      get;
    }

    /// <summary>
    /// The type this serializes to
    /// </summary>
    public override Type CustomSerializeToType 
      => ModelType;

    /// <summary>
    /// If we shouldn't try to set the child model data to the field value after deserializing.
    /// This is helpful if you used advanced deserialization to set the child model onto the parent already
    /// </summary>
    public bool SkipSettingValueToFieldAfterDeserialzation
      => skipSettingValueToFieldAfterDeserialzation || HasAdvancedChildDeserialization;
    bool skipSettingValueToFieldAfterDeserialzation
      = false;

    /// <summary>
    /// If this uses advanced child deserialization.
    /// This will make it so we skip setting the value to the model, as we assume advanced deserialization does this for us
    /// </summary>
    public virtual bool HasAdvancedChildDeserialization
      => ChildModelAdvancedDeserializationFunction != null;

    /// <summary>
    /// If this attribute uses advanced child serialization
    /// </summary>
    public bool HasAdvancedChildSerialization
      => ChildModelAdvancedSerializationFunction != null;

    /// <summary>
    /// Table name for the model type
    /// </summary>
    public string TableName
      => Serializer.Cache.GetDataModelInfo(ModelType).TableName;

    /// <summary>
    /// Helper for making the child function match the returns you need.
    /// </summary>
    public virtual Func<object, ICollection<Serializer.SerializedData>> ChildModelSerializationFunction
      => modelFieldValue => SingleChildModelSerializationFunction(modelFieldValue).AsEnumerable().ToList();

    /// <summary>
    /// Accessor for the single child model serialization function
    /// </summary>
    public virtual Func<object, Serializer.SerializedData> SingleChildModelSerializationFunction
      => modelFieldValue => ((ISerializeable)modelFieldValue).serialize(fullySerializeLinkedModels: false);

    /// <summary>
    /// Accessor for the single child model serialization function. included the index of the item in it's enumerable
    /// PARAMS: 
    ///  childModelDataObj,
    ///  index
    /// RETURNS:
    ///   The serialized data for the model object
    /// </summary>
    /// </summary>
    public virtual Func<object, int, Serializer.SerializedData> SingleChildModelSerializationFunctionWithIndex
      => null;

    /// <summary>
    /// Default for full deserialization of any model from it's id.
    /// PARAMS: 
    ///  childModelUniqueId,
    ///  childModelArchetypeId,
    ///  serializedChildModelData
    /// RETURNS:
    ///   The deserialized model
    /// </summary>
    public virtual Func<string, string, Serializer.SerializedData, ISerializeable> SingleChildModelDeserializationFunction
      => (uniqueModelId, archetypeExternalId, serializedModel) =>
        ISerializeable.Make(serializedModel);
      
    /// <summary>
    /// Full deserialization w/ parent model
    /// PARAMS: 
    ///  childModelUniqueId,
    ///  childModelArchetypeId,
    ///  serializedChildModelData,
    ///  parentModel
    /// RETURNS:
    ///   The deserialized model
    /// </summary>
    public virtual Func<string, string, Serializer.SerializedData, IUnique, ISerializeable> ChildModelAdvancedDeserializationFunction
      => null;

    /// <summary>
    /// An advanced serialization option for child models that includes the parent
    /// </summary>
    public virtual Func<object, ISerializeable, ICollection<Serializer.SerializedData>> ChildModelAdvancedSerializationFunction
      => null;

    /// <summary>
    /// Serialization override to point to the ChildModelSerializationFunction.
    /// Override that in children instead
    /// </summary>
    public override Func<object, object> SerializationFunction
      => (x) => ChildModelSerializationFunction(x);

    internal protected override Func<JToken, object> DeserializeJSON
      => (serializedData) => throw new NotImplementedException("You cannot normally de-serialize child model fields");

    internal protected override Func<object, object> DeserializeObject
      => (serializedData) => throw new NotImplementedException("You cannot normally de-serialize child model fields");

    /// <summary>
    /// Make a child field with a single model
    /// </summary>
    protected ChildModelFieldAttribute(
      Type ModelType,
      string FieldName = null,
      bool SkipSettingValueToFieldAfterDeserialzation = false,
      bool SkipDuringDeserialization = false, 
      bool skipOwnerCheck = false
    ) : base(FieldName, SkipDuringDeserialization) {
      this.ModelType = ModelType;
      IsChildModelField = true;
      skipSettingValueToFieldAfterDeserialzation = SkipSettingValueToFieldAfterDeserialzation;
      IsUnique = typeof(IUnique).IsAssignableFrom(ModelType);
      if(IsUnique && ModelType.IsValueType) {
        throw new NotImplementedException($"Struct based child models should not be unique! This can cause issues with loading children of children.");
      }
      if (!skipOwnerCheck && !typeof(IOwned).IsAssignableFrom(ModelType)) {
        throw new NotImplementedException($"Cannot set a model as a child model type unless it impliments IOwned. {ModelType?.ToString() ?? "null"} does not impliment IOwned");
      }
    }

    /// <summary>
    /// Make a child field with a single model
    /// </summary>
    public ChildModelFieldAttribute(Type ModelType, string FieldName = null, bool SkipSettingValueToFieldAfterDeserialzation = false, bool SkipDuringDeserialization = false) 
      : this(ModelType, FieldName, SkipSettingValueToFieldAfterDeserialzation, SkipDuringDeserialization, false) { }

    public override bool Equals(object obj) {
      return base.Equals(obj) 
        && (obj as ChildModelFieldAttribute).ModelType == ModelType;
    }
  }
}
