﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Meep.Tech.Data.Attributes.Models {

  /// <summary>
  /// Default for advanced deserialization methods, in case you need the parent model data to fetch from cache
  /// </summary>
  public class LinkedModelCollectionDataField : LinkedModelDataFieldAttribute {

    /// <summary>
    /// Serialize the entire enumerable as opposed to one model at a time:
    /// </summary>
    public override Func<object, ICollection<Serializer.SerializedData>> ChildModelSerializationFunction
      => modelList => ChildModelsCollectionFieldAttribute.MultiModelSerializationLogic(this, modelList);

    /// <summary>
    /// Used to set the collection in your own way if it's been found to be cached.
    /// </summary>
    public virtual Func<IUnique, IEnumerable<IUnique>, IUnique> AdvancedSetCollectionOnParent { get; }
      = null;

    public LinkedModelCollectionDataField(
      Type LinkedModelType, 
      string FieldName = null,
      bool SkipSettingValueToFieldAfterDeserialzation = false, 
      bool SkipDuringDeserialization = false,
      bool DeleteAllExistingChildLinksBeforeSaving = true
    ) : base(LinkedModelType, FieldName, SkipSettingValueToFieldAfterDeserialzation, SkipDuringDeserialization, DeleteAllExistingChildLinksBeforeSaving) {}
  }
}
