﻿using Meep.Tech.Data.Extensions;
using Newtonsoft.Json.Linq;
using Meep.Tech.Data.Serialization;
using System;
using System.Collections.Generic;

namespace Meep.Tech.Data.Attributes.Models {

  /// <summary>
  /// Special attribute for indicating the data component collection field.
  /// Saves them as a json object in a sql field, with each component indexed by it's type:
  /// </summary>
  public class ModelDataComponentCollectionFieldAttribute : ModelDataFieldAttribute {

    public override bool HasCustomSerialization 
      => true;

    public override bool HasAdvancedSqlFieldDeserialization 
      => true;

    /// <summary>
    /// Save all the components from the model as a json collection
    /// </summary>
    public override Func<object, object> SerializationFunction 
      => componentCollection => {
        JObject serializedComponents = new JObject();
        List<Serializer.SerializedData> fullySerializedComponents = null;
        foreach (IModelDataComponent component in componentCollection as IEnumerable<IModelDataComponent>) {
          if (component is IFullySerializeableComponent fullySerializeableComponent) {
            fullySerializedComponents ??= new List<Serializer.SerializedData>();
            Serializer.SerializedData data = fullySerializeableComponent.serialize();
            //data.setParentModelFieldIfEmpty(Serializer.ComponentKeyNameAndTablePlaceholder);
            data.sqlData[Serializer.SQLTypeColumnName] = fullySerializeableComponent.type.ExternalId;
            fullySerializedComponents.Add(data);
          } else {
            JToken componentJson = component.toJObject();
            serializedComponents.Add(component.type.Id.ExternalId, componentJson);
          }
        }

        return (serializedComponents.ToString(), fullySerializedComponents);
      };

    /// <summary>
    /// Add all the components back to the model
    /// </summary>
    public override Func<object, ISerializeable, ISerializeable> AdvancedSqlObjectDeserializationFunction 
      => (serializedComponents, currentModel) => {
        (object serializedComponentsJSONString, List<Serializer.SerializedData> fullySerializedComponents)
          = ((object, List<Serializer.SerializedData>))serializedComponents;
        IModelDataComponentStorage model = currentModel as IModelDataComponentStorage;
        foreach ((string componentType, JToken serializedComponent) in JObject.Parse(serializedComponentsJSONString as string ?? "{}")) {
          IModelDataComponent component = (IModelDataComponent)Static.Components.ModelDatas.ByExternalId[componentType].MakeI();
          component = component.fromJson(serializedComponent.ToString());
          model.addOrUpdateComponent(component);
        }

        // Make any fully serialized components from their data:
        fullySerializedComponents?.ForEach(serializedComponent => model.addOrUpdateComponent(
          Static.Components.ModelDatas.Make(serializedComponent))
        );

        return model as ISerializeable;
      };

    public ModelDataComponentCollectionFieldAttribute() : base(
      Serializer.SQLComponentsDataColumnName, 
      true,
      false,
      typeof(string)
    ) {}
  }
}
