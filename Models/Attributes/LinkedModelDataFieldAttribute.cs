﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Serialization;
using System;
using System.Collections.Generic;

namespace Meep.Tech.Data.Attributes.Models {

  /// <summary>
  /// A linked model, one that's not owned but is saved in it's own table under it's own UUID.
  /// By default this will load the model as a new model with the desired data from the DB. If you want this to find an existing cached model by id you need to overload it.
  /// 
  /// TODO: Make LinkedModelDataField that allows you to pre-emptively search the cache for an id to avoid a DB hit alltogeter. This one can only check the cache after db data has been grabbed which isn't too helpful. Maybe just refactor this one lol.
  /// </summary>
  public class LinkedModelDataFieldAttribute : ChildModelFieldAttribute {

  /// <summary>
  /// Images table name
  /// </summary>
  public const string LinksTableName = "_Links";
    /// <summary>
    /// Table columns for the image table
    /// </summary>
    public static Dictionary<string, Type> LinksTableColumns {
      get;
    } = new Dictionary<string, Type> {
      {
        Serializer.SQLOwnerColumnName,
        typeof(string)
      },
      {
        Serializer.SQLIDColumnName,
        typeof(string)
      },
      {
        Serializer.SQLOwnerModelFieldNameColumnName,
        typeof(string)
      },
      {
        "parentModelTable",
        typeof(string)
      },
      {
        "childModelTable",
        typeof(string)
      },
    };

    /// <summary>
    /// If all of the child links for this model field should be deleted before the current links are saved
    // TODO: impliment
    /// </summary>
    public bool DeleteAllExistingChildLinksBeforeSaving {
      get;
    }

    /// <summary>
    /// Overrideable function to search a cache for the item based on:
    /// it's uniqueId, archetype externalId, json data, and (if using advances deserialization) the parent model
    /// </summary>
    public virtual Func<string, IUnique> FetchModelFromCache { 
      get;
    } = null;

    /// <summary>
    /// Default for full deserialization
    /// </summary>
    public override Func<string, string, Serializer.SerializedData, ISerializeable> SingleChildModelDeserializationFunction
      => (uniqueModelId, modelTypeExternalId, serializedModel) => {
        ISerializeable childModel;
        // try to fetch from cache
        if ((childModel = FetchModelFromCache?.Invoke(uniqueModelId)) != null) {
          return childModel;
        // TODO: does this call the custom finalizeDeserialization function in icached?
        } else return base.SingleChildModelDeserializationFunction(uniqueModelId, modelTypeExternalId, serializedModel) 
            as IUnique;
      };


    public LinkedModelDataFieldAttribute(
      Type LinkedModelType,
      string FieldName = null,
      bool SkipSettingValueToFieldAfterDeserialzation = false,
      bool SkipDuringDeserialization = false,
      bool DeleteAllExistingChildLinksBeforeSaving = true
    ) : base(
      LinkedModelType,
      FieldName,
      SkipSettingValueToFieldAfterDeserialzation,
      SkipDuringDeserialization,
      true
    ) {
      this.DeleteAllExistingChildLinksBeforeSaving = DeleteAllExistingChildLinksBeforeSaving;
      if (!IsUnique) {
        throw new Exception($"LinkedModelDataFieldAttribute must be a IUnique model type so we can find it via ID");
      }
      if (typeof(ICached).IsAssignableFrom(LinkedModelType)) {
        FetchModelFromCache ??= id => ICached.FromCache(id);
      } else throw new NotImplementedException($"Type {LinkedModelType} does not impliment ICached<self>");
    }
  }
}
