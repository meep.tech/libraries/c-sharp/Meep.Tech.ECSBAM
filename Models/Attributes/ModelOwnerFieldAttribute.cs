﻿using Meep.Tech.Data.Serialization;

namespace Meep.Tech.Data.Attributes.Models {

  /// <summary>
  /// Used to represent the unique ID of the owner/parent model of this child model.
  /// </summary>
  public class ModelOwnerFieldAttribute : ModelDataFieldAttribute {
    public ModelOwnerFieldAttribute(bool SkipDuringDeserialization = false) : base(Serializer.SQLOwnerColumnName, IsASQLField: true, SkipDuringDeserialization) { }
  }
}
