﻿using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Serialization;
using System;

namespace Meep.Tech.Data.Attributes.Models {

  /// <summary>
  /// Used to indicate the Type (Archetype) field on a model.
  /// </summary>
  public class ModelTypeFieldAttribute : ModelDataFieldAttribute {

    public override bool HasCustomSerialization 
      => true;

    public override bool HasCustomDeserialization 
      => true;

    public override Type CustomSerializeToType
      => typeof(string);

    /// <summary>
    /// Make using the field type
    /// </summary>
    public ModelTypeFieldAttribute() 
      : base(
          Serializer.SQLTypeColumnName,
          IsASQLField: true,
          SkipDuringDeserialization: true
        ) {}

    /// <summary>
    /// Set the value with the external id
    /// </summary>
    public override Func<object, object> SerializationFunction
      => archetype
        => (archetype as IArchetypeId).ExternalId;

    /// <summary>
    /// Get the enum from an external id
    /// </summary>
    internal protected override Func<object, object> DeserializeObject
      => archetypeExternalId
        => archetypeExternalId != null
          ? Static.Archetypes.Ids[archetypeExternalId as string].Archetype
          : null;
  }
}
