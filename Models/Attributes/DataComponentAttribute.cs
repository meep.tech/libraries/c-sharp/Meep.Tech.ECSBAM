﻿using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Serialization;
using System;

namespace Meep.Tech.Data.Attributes.Models {

  /// <summary>
  /// Represents a component that can be saved to json
  /// </summary>
  public class DataComponentAttribute : DataModelAttribute {
    public DataComponentAttribute(Type componentArchetypeType = null) 
      : base(
          Serializer.ComponentKeyNameAndTablePlaceholder,
          componentArchetypeType ?? typeof(IComponentArchetype),
          null,
          null
        ) {}
  }
}
