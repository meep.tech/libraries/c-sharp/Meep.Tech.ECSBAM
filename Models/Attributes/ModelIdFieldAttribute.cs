﻿using Meep.Tech.Data.Serialization;

namespace Meep.Tech.Data.Attributes.Models {
  /// <summary>
  /// Used to represent the unique ID of a model.
  /// </summary>
  public class ModelIdFieldAttribute : ModelDataFieldAttribute {
    public ModelIdFieldAttribute() : base(Serializer.SQLIDColumnName, IsASQLField: true) {}
  }
}
