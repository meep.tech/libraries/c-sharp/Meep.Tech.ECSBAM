﻿using System;
using System.Collections;
using Meep.Tech.Data.Serialization;
using System.Collections.Generic;
using Meep.Tech.Data.Base;

namespace Meep.Tech.Data.Attributes.Models {

  /// <summary>
  /// Used to serialize and deserialize a collection(IEnumeration) of child models in one field
  /// </summary>
  public class ChildModelsCollectionFieldAttribute : ChildModelFieldAttribute {

    /// <summary>
    /// Deserealize as a collection obect instead of as one object in the collection at a time.
    /// </summary>
    public bool DeserializeCollectionAsAWholeObject
      => ChildModelsCollectionAdvancedDeserializationFunction != null;

    /// <summary>
    /// Deserialize the object as a big collection of serialized datas instead of one at a time:
    /// Params: (serializedChildModels, currentParentModel)
    /// Returns: The deserialized models.
    /// 
    /// Adding owner field name to each, and deserializing the children of each can be auto handled.
    /// </summary>
    public virtual Func<ICollection<Serializer.SerializedData>, IUnique, Dictionary<string, ICollection<ISerializeable>>> ChildModelsCollectionAdvancedDeserializationFunction
      => null;

    /// <summary>
    /// If ChildModelsCollectionAdvancedDeserializationFunction != null, this will automatically deserialize children of each child model onto them.
    /// </summary>
    public virtual bool AutoHandleChildrenOfChildrenCollection
      => true;

    /// <summary>
    /// If the serializer should delete all children of this type from the DB for this model before saving them again
    /// </summary>
    public bool DeleteAllOldChildrenBeforeSavingNewChildrenOfThisType {
      get;
    } = false;

    /// <summary>
    /// Base type as an enum
    /// </summary>
    public override Type CustomSerializeToType
      => typeof(IEnumerable<>).MakeGenericType(base.CustomSerializeToType);

    /// <summary>
    /// Serialize the entire enumerable as opposed to one model at a time:
    /// </summary>
    public override Func<object, ICollection<Serializer.SerializedData>> ChildModelSerializationFunction
      => modelList => MultiModelSerializationLogic(this, modelList);

    public ChildModelsCollectionFieldAttribute(Type ModelType, string FieldName = null, bool SkipDuringDeserialization = false, bool SkipSettingValueToFieldAfterDeserialzation = false)
      : base(ModelType, FieldName, SkipSettingValueToFieldAfterDeserialzation, SkipDuringDeserialization) { }

    protected ChildModelsCollectionFieldAttribute(string FieldName, Type ModelType, bool SkipOwnerCheck = false, bool SkipSettingValueToFieldAfterDeserialzation = false, bool SkipDuringDeserialization = false, bool DeleteAllOldChildrenBeforeSavingNewChildrenOfThisType = false)
      : base(
          ModelType,
          FieldName,
          SkipSettingValueToFieldAfterDeserialzation,
          SkipDuringDeserialization,
          SkipOwnerCheck
    ) {
      this.DeleteAllOldChildrenBeforeSavingNewChildrenOfThisType = DeleteAllOldChildrenBeforeSavingNewChildrenOfThisType;
    }

    /// <summary>
    /// Logic to help making multi-item field attributes.
    /// </summary>
    public static Func<ChildModelFieldAttribute, object, ICollection<Serializer.SerializedData>> MultiModelSerializationLogic {
      get => (thisCollectionField, modelList) => {
        ICollection<Serializer.SerializedData> serializedObjects
          = new List<Serializer.SerializedData>();

        int index = 0;
        Func<object, int, Serializer.SerializedData> serializer = (model, index) =>
            thisCollectionField.SingleChildModelSerializationFunctionWithIndex != null
            ? thisCollectionField.SingleChildModelSerializationFunctionWithIndex(model, index)
            : thisCollectionField.SingleChildModelSerializationFunction(model);

        Select(modelList, serializer)
          .ForEach(serializedObjects.Add);

        return serializedObjects;
      };
    }

    public static IEnumerable<T> Select<T>(object collectionObject, Func<object, int, T> select) {
      // if it's a dictonary, by default we assume the values are the models
      if (collectionObject is IDictionary modelDictionary) {
        collectionObject = modelDictionary.Values;
      }

      if (collectionObject is System.Collections.IEnumerable collection) {
        int index = 0;
        foreach (object model in collection) {
          yield return select(model, index++);
        }
      }
    }
  }
}
