﻿using System;
using System.Collections.Generic;

namespace Meep.Tech.Data.Base {
  public abstract partial class Model {

    /// <summary>
    /// A collection for model creation parameters.
    /// </summary>
    public class Params : Dictionary<Param, object> {
      public Params() : base() { }
      public Params(Dictionary<Param, object> original) : base(original) { }

      public Model.Params Append(Param param, object value) {
        this[param] = value;
        return this;
      }

      public Model.Params AppendIf(Func<bool> @if, Param param, object value) {
        if (@if()) {
          this[param] = value;
        }

        return this;
      }

      public Model.Params AppendIf(Func<bool> @if, Model.Params values) {
        if (@if()) {
          values.ForEach(x => Append(x.Key, x.Value));
        }

        return this;
      }
    }
  }
}
