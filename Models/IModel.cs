﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Serialization;
using System.Collections.Generic;
using System;
using Meep.Tech.Data;

namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// A general model made of data components, holds instance data and is built from an archetype
  /// </summary>
  public interface IModel : ISerializeable {

    /// <summary>
    /// The archetype this model represents
    /// </summary>
    Archetype type
      => null;

    /// <summary>
    /// Return an effective clone/deep copy of this model.
    /// Implimented in the Generic interface below
    /// </summary>
    IModel clone(bool newId = true, string uniqueIdToken = null)
      => null;
  }

  /// <summary>
  /// A Model that has all of it's archetypes based on one kind of parent base Archetype
  /// </summary>
  public interface IModel<TModelBase, TArchetypeBase, TIdBase>
    : IModel
    where TModelBase : IModel<TModelBase, TArchetypeBase, TIdBase>
    where TArchetypeBase : Archetype<TModelBase, TArchetypeBase, TIdBase>
    where TIdBase : IArchetypeId 
  {

    /// <summary>
    /// The Archetype type this model was made for:
    /// </summary>
    new TArchetypeBase type {
      get;
    }

    /// <summary>
    /// The archetype this model represents
    /// </summary>
    Archetype IModel.type
      => type;

    /// <summary>
    /// make a copy but keep the base type
    /// </summary>
    new TModelBase clone(bool newId = true, string uniqueIdToken = null)
      => IBaseModelMethods.clone(this);
  }

  public static class IExtensionModelMethods {

    /// <summary>
    /// Model Base Type's System.Types indexed by each Model Type
    /// </summary>
    internal static Dictionary<System.Type, System.Type> _modelBaseTypes { get; private set; }
      = new Dictionary<System.Type, System.Type>();

    /// <summary>
    /// Get the base model type from a given model type:
    /// </summary>
    public static System.Type GetBaseModelType(this System.Type modelType) {
      if (_modelBaseTypes.TryGetValue(modelType, out System.Type modelBaseType)) {
        return modelBaseType;
      } else throw new NotImplementedException($"Type: {modelType?.FullName ?? "ERRORNOTYPE"}, does not impliment IModel<ModelBaseType...");
    }

    /// <summary>
    /// Get the base model type of a model.
    /// </summary>
    public static System.Type getBaseModelType(this IModel model)
      => model.GetType().GetBaseModelType();

  }
}

public static class IBaseModelMethods {

  /// <summary>
  /// Check if this model has the given component.
  /// </summary>
  public static bool hasComponent(this IModel model, Model.DataComponent.Id componentType)
    => (model as IModelDataComponentStorage)?.dataComponents.ContainsKey(componentType)
      ?? false;

  /// <summary>
  /// Check if this model has the given component.
  /// </summary>
  public static bool hasComponent<TComponent>(
    this IModel model
  ) where TComponent : IModelDataComponent
    => (model as IModelDataComponentStorage)?._dataComponentIdsByBaseType.ContainsKey(typeof(TComponent).GetBaseModelType())
      ?? false;

  /// <summary>
  /// Helper to try to get a component
  /// </summary>
  /// <returns>false if this model doesn't have the given component</returns>
  public static bool tryToGetComponent<TComponent>(this IModel model, out TComponent component)
    where TComponent : IModelDataComponent {
    Model.DataComponent.Id dataComponent = null;
    if ((model as IModelDataComponentStorage)?._dataComponentIdsByBaseType.TryGetValue(typeof(TComponent).GetBaseModelType(), out dataComponent) ?? false) {
      component = (TComponent)((model as IModelDataComponentStorage).dataComponents[dataComponent]);
      return true;
    }

    component = default;
    return false;
  }

  /// <summary>
  /// Helper to try to get a component, returns null/default on failure
  /// </summary>
  public static TComponent tryToGetComponent<TComponent>(this IModel model)
    where TComponent : IModelDataComponent {
    Model.DataComponent.Id dataComponent = null;
    if ((model as IModelDataComponentStorage)?._dataComponentIdsByBaseType.TryGetValue(typeof(TComponent).GetBaseModelType(), out dataComponent) ?? false) {
      return (TComponent)((model as IModelDataComponentStorage).dataComponents[dataComponent]);
    }

    return default;
  }

  /// <summary>
  /// Helper to try to get a component, returns null/default on failure
  /// </summary>
  public static TComponent getComponent<TComponent>(this IModel model)
    where TComponent : IModelDataComponent {
    Model.DataComponent.Id dataComponent = (model as IModelDataComponentStorage)?._dataComponentIdsByBaseType[typeof(TComponent).GetBaseModelType()];
    return (TComponent)((model as IModelDataComponentStorage).dataComponents[dataComponent]);
  }

  /// <summary>
  /// Helper to try to get a component
  /// </summary>
  /// <returns>false if this model doesn't have the given component</returns>
  public static bool tryToGetComponent(this IModel model, Model.DataComponent.Id componentType, out IModelDataComponent component) {
    component = null;
    return (model as IModelDataComponentStorage)?.dataComponents.TryGetValue(componentType, out component) ?? false;
  }

  /// <summary>
  /// Helper to try to get a component
  /// </summary>
  /// <returns>false if this model doesn't have the given component</returns>
  public static IModelDataComponent tryToGetComponent(this IModel model, Model.DataComponent.Id componentType) {
    IModelDataComponent component = null;
    return ((model as IModelDataComponentStorage)?.dataComponents.TryGetValue(componentType, out component) ?? false) ? component : null;
  }

  /// <summary>
  /// Copy helper
  /// </summary>
  public static IModel clone<TModelBase, TArchetypeBase, TIdBase>(this IModel model, bool newId = true, string uniqueIdToken = null)
    where TModelBase : IModel<TModelBase, TArchetypeBase, TIdBase>
    where TArchetypeBase : Archetype<TModelBase, TArchetypeBase, TIdBase>
    where TIdBase : IArchetypeId {
    ISerializeable serializeable = (ISerializeable)model;
    Serializer.SerializedData serializedData = serializeable.serialize(true);
    if (newId) {
      serializedData.sqlData[Serializer.SQLIDColumnName] = RNG.GetNextUniqueIdFromToken(uniqueIdToken);
    }
    return ((IModel<TModelBase, TArchetypeBase, TIdBase>)model).type.Make(serializedData);
  }

  /// <summary>
  /// Make a model copy
  /// </summary>
  /// <returns></returns>
  public static TModelBase clone<TModelBase, TArchetypeBase, TIdBase>(this IModel<TModelBase, TArchetypeBase, TIdBase> model, bool newId = true, string uniqueIdToken = null)
    where TModelBase : IModel<TModelBase, TArchetypeBase, TIdBase>
    where TArchetypeBase : Archetype<TModelBase, TArchetypeBase, TIdBase>
    where TIdBase : IArchetypeId {
    Serializer.SerializedData serializedData = model.serialize(true);
    if (newId) {
      serializedData.sqlData[Serializer.SQLIDColumnName] = RNG.GetNextUniqueIdFromToken(uniqueIdToken);
    }
    return model.type.Make(serializedData);
  }

  /// <summary>
  /// Try to get a logic system attached to this model's type.
  /// </summary>
  public static bool tryToGetSystem<TSystemComponent, TModelBase, TArchetypeBase, TIdBase>(
    this IModel<TModelBase, TArchetypeBase, TIdBase> model,
    out TSystemComponent systemComponent
  )
    where TModelBase : IModel<TModelBase, TArchetypeBase, TIdBase>
    where TArchetypeBase : Archetype<TModelBase, TArchetypeBase, TIdBase>
    where TIdBase : IArchetypeId
    where TSystemComponent : ISystemComponent
      => model.type.TryGetSystem(out systemComponent);
}
