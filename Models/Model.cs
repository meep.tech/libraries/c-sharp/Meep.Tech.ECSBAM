﻿using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Attributes.Models;
using Meep.Tech.Data.Static;
using Meep.Tech.Data.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using KellermanSoftware.CompareNetObjects;
using System.Runtime.CompilerServices;
using Meep.Tech.Data.Base;

namespace Meep.Tech.Data.Base {

  /// <summary>
  /// Base abstract class for Generic Model interactions.
  /// Most of the time you should extend the Generic Model<,,>, or Interface IModel instead.
  /// </summary>
  public abstract partial class Model
    : IModelDataComponentStorage,
      ISerializeable {

    /// <summary>
    /// Overriden comparers
    /// </summary>
    internal static Dictionary<System.Type, CompareLogic> Comparers
      = new Dictionary<Type, CompareLogic>();

    /// <summary>
    /// Collection for model builders
    /// </summary>
    public static Archetype.Collection<Model, IArchetype, IArchetypeId> Builders
      = new Archetype.Collection<Model, IArchetype, IArchetypeId>();

    #region IModelDataComponentStorage

    /// <summary>
    /// External, readonly list of data components on the model:
    /// </summary>
    public IReadOnlyDictionary<Model.DataComponent.Id, IModelDataComponent> dataComponents {
      get => ((IModelDataComponentStorage)this)._dataComponentsByArchetypeId;
    }

    /// <summary>
    /// This makes sure components get saved via serialization:
    /// </summary>
    [ModelDataComponentCollectionField]
    protected virtual IEnumerable<IModelDataComponent> componentsToSerialize {
      get => dataComponents.Values;
      set => ((IModelDataComponentStorage)this)._dataComponentsByArchetypeId.Merge(value.ToDictionary(
        component => component.type.Id,
        component => component
      ));
    }

    /// <summary>
    /// Initialize the data components
    /// </summary>
    Dictionary<Model.DataComponent.Id, IModelDataComponent> IModelDataComponentStorage._dataComponentsByArchetypeId {
      get;
    } = new Dictionary<Model.DataComponent.Id, IModelDataComponent>();

    /// <summary>
    /// Initialize the data components
    /// </summary>
    Dictionary<System.Type, Model.DataComponent.Id> IModelDataComponentStorage._dataComponentIdsByBaseType {
      get;
    } = new Dictionary<System.Type, Model.DataComponent.Id>();

    #endregion

    #region ISerializeable;

    protected internal virtual void finishDeserialization() { }

    #endregion

    #region IEnumerator

    IEnumerator<IModelDataComponent> IEnumerable<IModelDataComponent>.GetEnumerator()
      => dataComponents.Values.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator()
      => dataComponents.Values.GetEnumerator();

    #endregion

    /// <summary>
    /// Easy access to copying.
    /// You can just override clone in future models and it should override copy too
    /// </summary>
    public abstract Model copy(bool newUniqueId = true, string newUniqueIdToken = null);

    #region Eqiality

    /// <summary>
    /// For comparing model data components:
    /// </summary>
    public override bool Equals(object obj) {
      return obj is Model
        && IModelDataComponentStorage.Equals(this, obj as IModelDataComponentStorage);
    }

    // ==
    public static bool operator ==(Model a, Model b)
      => (a?.Equals(b) ?? false) || (a is null && b is null);

    // !=
    public static bool operator !=(Model a, Model b)
      => !(a == b);

    public override int GetHashCode()
      => this is IUnique uniqueModel
        ? uniqueModel.getUniqueId().GetHashCode()
        : base.GetHashCode();

    /// <summary>
    /// Default
    /// </summary>
    public override string ToString()
      => $"{{{GetType().Name}}}";


    #endregion
  }

  /// <summary>
  /// Mini model without extensible types. Has a built in bulder.
  /// </summary>
  /// <typeparam name="TModelBase"></typeparam>
  public abstract class Model<TModelBase>
  : Model<Model<TModelBase>, Model<TModelBase>.Builder, Model<TModelBase>.Builder.Id>
  where TModelBase : Model<TModelBase>, new() {

    /// <summary>
    /// Invoke the static initalzer
    /// </summary>
    static Model() {
      Static.Model<TModelBase>.StaticInitalization?.Invoke();
    }

    /// <summary>
    /// Can be used to set an initializer function for base models:
    /// </summary>
    protected virtual TModelBase Configure(TModelBase model, Model.Params @params)
      => model;

    protected Model()
      : base(null) { type = Builder.Instance; }

    /// <summary>
    /// A builder is initalized for each Model<> class.
    /// </summary>
    [Attributes.Archetypes.DoNotBuildArchetypeInitially]
    public class Builder : Archetype<Model<TModelBase>, Builder, Builder.Id> {

      public static Builder Instance
        => Model.Builders.Get<Builder>();

      /// <summary>
      /// Auto generated ids.
      /// </summary>
      public new class Id : Archetype.Identity {
        internal Id(string name, string externalIdPrefix = null) 
          : base(name, $"Builder.{externalIdPrefix ?? ""}") {}
      }

      protected internal override bool AllowSubtypeRuntimeRegistrations
        => true;

      internal Builder() 
        : base(new Id(typeof(TModelBase).FullName), Model.Builders) {}

      protected override Model<TModelBase> InitializeModel(Params @params = null)
        => new TModelBase() as Model<TModelBase>;

      protected override Model<TModelBase> ConfigureModel(Model<TModelBase> model, Params @params = null) {
        model = base.ConfigureModel(model, @params);
        model = model.Configure(model as TModelBase, @params);

        return model;
      }

      /// <summary>
      /// Build a new instance of the model from the builder.
      /// Shortcut for the Archetype.Make
      /// </summary>
      public new TModelBase Make(Model.Params @params = null)
        => Make<TModelBase>(@params);
    }
  }

  /// <summary>
  /// Just a helper base class that can be used to build the base of an object based model.
  /// For structs based models, see IModel, and you'll need to copy paste most of the code in "#region Basic IModel Implimentation", and possibly want to add IModelDataComponentStorage
  /// </summary>
  public abstract class Model<TModelBase, TArchetypeBase, TIdBase>
    : Model,
      IModel<TModelBase, TArchetypeBase, TIdBase>,
      System.IEquatable<TModelBase>
    where TArchetypeBase : Archetype<TModelBase, TArchetypeBase, TIdBase>
    where TModelBase : IModel<TModelBase, TArchetypeBase, TIdBase>
    where TIdBase : IArchetypeId 
  {

    /// <summary>
    /// The archetype of this model
    /// </summary>
    [ModelTypeField]
    public TArchetypeBase type {
      get;
      internal set;
    }

    /// <summary>
    /// Basic CTOR
    /// </summary>
    protected Model(TArchetypeBase archetype) {
      type = archetype;
    }

    /// <summary>
    /// Easy access to copying.
    /// This uses serialization, so overwrite if you want a quicker way.
    /// </summary>
    public override Model copy(bool newUniqueId = true, string newUniqueIdToken = null)
      => clone(newUniqueId, newUniqueIdToken) as Model;

    /// <summary>
    /// Overrideable clone
    /// </summary>
    /// <returns></returns>
    public virtual TModelBase clone(bool newUniqueId = true, string newUniqueIdToken = null)
      => IBaseModelMethods.clone(this, newUniqueId, newUniqueIdToken);

    /// <summary>
    /// Convert this model's data to a serialized state.
    /// </summary>
    public static TModelBase FromJson(string jsonString) {
      Serializer.SerializedData data = Serializer.SerializedData.FromJson(jsonString);
      return (TModelBase)Archetypes.Collections[typeof(TArchetypeBase)]
        .GetI(data.sqlData[Serializer.SQLTypeColumnName] as string)
        .MakeI(data);
    }

    #region Eqiality

    /// <summary>
    /// Compare all model fields to check for equality.
    /// This runs alongside baseEqualityCheck by default, which compares all components.
    /// This shouldn't check the id.
    /// You can also edit the compare through Static.Model<Type>.Comparer
    /// </summary>
    public virtual bool compare(TModelBase other)
      => compareAs(GetType(), other);

    /// <summary>
    /// Compare as the given type:
    /// </summary>
    /// <typeparam name="TCompareAs"></typeparam>
    /// <param name="other"></param>
    /// <returns></returns>
    public virtual bool compare<TCompareAs>(TCompareAs other)
      where TCompareAs : TModelBase
        => Static.Model<TCompareAs>.Comparer.Compare(this, other).AreEqual;

    /// <summary>
    /// Compare two types using the first given type
    /// </summary>
    internal virtual bool compareAs(System.Type typeToCompareAs, TModelBase other)
      => Model.Comparers.TryGetValue(typeToCompareAs, out CompareLogic compareLogic)
        ? compareLogic.Compare(this, other).AreEqual
        : compare(other);

    /// <summary>
    /// The base check equality for a model.
    /// This checks to make sure the types and id match first off, then checks components in the base call
    /// then compares all fields.
    /// </summary>
    /*protected bool baseEqualityCheck(object obj) 
      => obj is Model<TModelBase, TArchetypeBase, TIdBase> otherModel
        // id
        && (this is IUnique uniqueThis && uniqueThis.id == (otherModel as IUnique)?.getUniqueId())
        // type value
        && otherModel.type.Equals(type)
        // base equality
        && base.Equals(obj)
        // base comparison
        && compare((TModelBase)obj);*/
    protected bool baseEqualityCheck(object obj) {
      if (obj is Model<TModelBase, TArchetypeBase, TIdBase> otherModel)
        if (this is IUnique uniqueThis && uniqueThis.id == (otherModel as IUnique)?.getUniqueId())
          if (otherModel.type.Equals(type))
            if (base.Equals(obj))
             if (compare((TModelBase)obj))
                return true;
      return false;
    }

    /// <summary>
    /// Base equals for models:
    /// </summary>
    public override bool Equals(object obj)
      => baseEqualityCheck(obj);

    /// <summary>
    /// Equals for IEquality.
    /// </summary>
    bool System.IEquatable<TModelBase>.Equals(TModelBase other)
      => baseEqualityCheck(other);
    #endregion
  }
}

namespace Meep.Tech.Data.Static {

  /// <summary>
  /// Mini model without extensible types. Has a built in bulder.
  /// </summary>
  /// <typeparam name="TModelBase"></typeparam>
  public static class Model<TModelBase> where TModelBase : IModel {

    /// <summary>
    /// Can be used to set up stuff for this model before a model of this type if ever made
    /// </summary>
    public static Action StaticInitalization
      = null;

    /// <summary>
    /// Fields for the comparer to ignore by default
    /// Editing this does not guarenteed updates. This is just foe help
    /// </summary>
    public static IReadOnlyList<string> DefaultFieldsToIgnoreDuringComparison {
      get;
    } = new List<string> {
      "dataComponents",
      "id",
      "_id",
      "type",
      "componentsToSerialize",
      "*k__BackingField*",
      "<type>k__BackingField",
      "<Meep.Tech.Data.Base.IUnique.id>k__BackingField",
      "<Meep.Tech.Data.Extensions.IModelDataComponentStorage._dataComponentsByArchetypeId>k__BackingField",
      "<Meep.Tech.Data.Extensions.IModelDataComponentStorage._dataComponentIdsByBaseType>k__BackingField"
    };

    /// <summary>
    /// Compare logic to use for this class
    /// </summary>
    public static CompareLogic Comparer {
      get;
    } = Model.Comparers.AddAndReturnValue(
      typeof(TModelBase),
      new CompareLogic() {
        Config = new ComparisonConfig { 
          ComparePrivateFields = true,
          ComparePrivateProperties = true,
          CompareProperties = true,
          CompareChildren = true,
          MembersToIgnore = DefaultFieldsToIgnoreDuringComparison.ToList(),
          IgnoreObjectTypes = true,
          UseHashCodeIdentifier = true,
          CompareStaticFields = false,
          CompareStaticProperties = false
        }
      }
    );
  }
}