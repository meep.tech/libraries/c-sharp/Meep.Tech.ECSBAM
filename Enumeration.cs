﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;

namespace Meep.Tech.Data {

  /// <summary>
  /// Base for a simple Enumerable with some extra data.
  /// </summary>
  public abstract class Enumeration 
    : IEquatable<Enumeration> 
  {

    /// <summary>
    /// The current number of enums. Used for internal indexing.
    /// </summary>
    static int CurrentMaxInternalEnumId 
      = 0;

    /// <summary>
    /// The name of this archetype. Names must be unique by ArchetypeBaseType
    /// </summary>
    public string Name {
      get;
    }

    /// <summary>
    /// The assigned internal id of this archetype. This is only consistend within the current runtime and execution.
    /// </summary>
    public int InternalId {
      get;
    }

    /// <summary>
    /// The perminant and unique external id
    /// </summary>
    public string ExternalId {
      get;
    }

    /// <summary>
    /// Make an archetype ID
    /// </summary>
    internal Enumeration(string name, string externalIdPrefix) {
      Name = name;
      // Remove any spaces:
      ExternalId = Regex.Replace($"{externalIdPrefix}{name}", @"\s+", "");
      InternalId = Interlocked.Increment(ref CurrentMaxInternalEnumId) - 1;
    }

    /// <summary>
    /// ==
    /// </summary>
    public abstract bool Equals(Enumeration other);
  }

  /// <summary>
  /// Base for a general Enum
  /// </summary>
  /// <typeparam name="TEnumBase"></typeparam>
  public abstract class Enumeration<TEnumBase>
    : Enumeration,
      IComparable,
      IEquatable<Enumeration<TEnumBase>>,
      IEnumerable<Enumeration<TEnumBase>>
    where TEnumBase : Enumeration<TEnumBase> {

    /// <summary>
    /// Readonly list of all items
    /// </summary>
    public static IReadOnlyList<Enumeration<TEnumBase>> All
      => _all;

    /// <summary>
    /// Intenral list of all items
    /// </summary>
    readonly static List<Enumeration<TEnumBase>> _all
      = new List<Enumeration<TEnumBase>>();

    /// <summary>
    /// Ctor add to all.
    /// </summary>
    protected Enumeration(string name, string externalIdPrefix) 
      : base(name, externalIdPrefix) {
      _all.Add(this);
    }

    #region Equality, Comparison, and Conversion

    /// <summary>
    /// ==
    /// </summary>
    /// 
    public override bool Equals(object obj)
      => Equals(obj as Enumeration<TEnumBase>);

    public int CompareTo(object other)
      => ExternalId.CompareTo((other as Enumeration<TEnumBase>)?.ExternalId);

    public bool Equals(Enumeration<TEnumBase> other) {
      return other != null && other.ExternalId == ExternalId;
    }

    public override bool Equals(Enumeration other)
      => other is Enumeration<TEnumBase> otherEnum && Equals(otherEnum);

    /// <summary>
    /// #
    /// </summary>
    public override int GetHashCode() {
      // TODO: test using internal id here instead for more speeeed in indexing:
      return ExternalId.GetHashCode();
    }

    public override string ToString() {
      return $"_{InternalId}/{ExternalId}";
    }

    #endregion

    public IEnumerator<Enumeration<TEnumBase>> GetEnumerator()
      => All.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator()
      => GetEnumerator();
  }
}
