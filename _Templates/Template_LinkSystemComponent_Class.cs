﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Static;

namespace $rootnamespace$.Components {

  public class $itemname$LinkSystemComponent : LinkSystemComponent<
    $itemname$LinkSystemComponent,
    $itemname$LinkSystemComponent.ModelData,
    $itemname$LinkSystemComponent.ArchetypeData,
    $itemname$LinkSystemComponent.ModelData.Type,
    $itemname$LinkSystemComponent.ArchetypeData.Type
  > {

    #region Child Components

    /// <summary>
    /// The model's data component
    /// </summary>
    public class ModelData : ModelDataComponent<ModelData> {

      public static Components.ModelDataId Id {
        get;
      } = new Components.ModelDataId("$itemname$");

      protected ModelData()
        : base((ModelDataComponentType<ModelData>)Id.Archetype) { }

      #region Type

      public class Type : ModelDataComponentType<ModelData> {

        #region Initialization

        public Type() : base(ModelData.Id) { }

        #region Model Construction

        protected override ModelData InitializeModel()
          => new ModelData(this);

        #endregion

        #endregion
      }

      #endregion
    }

    /// <summary>
    /// The archetype's data component
    /// </summary>
    public class ArchetypeData : ArchetypeDataComponent<ArchetypeData> {

      public static Components.ArchetypeDataId Id {
        get;
      } = new Components.ArchetypeDataId("$itemname$");

      public ArchetypeData()
        : base((ArchetypeDataComponentType<ArchetypeData>)Id.Archetype) { }

      #region Type

      public class Type : ArchetypeDataComponentType<ArchetypeData> {

        #region Initialization

        public Type() : base(ArchetypeData.Id) { }

        #region Model Construction

        protected override ArchetypeData InitializeModel()
          => new ArchetypeData(this);

        #endregion

        #endregion
      }

      #endregion
    }

    #endregion

    public static Components.LinkSystemId Id {
      get;
    } = new Components.LinkSystemId("$itemname$");

    /// <summary>
    /// For adding this to archetypes:
    /// </summary>
    /// <param name="type"></param>
    /// <param name="archetypeComponent"></param>
    public $itemname$LinkSystemComponent(IArchetypeDataComponent archetypeComponent)
      : base(
          Id.Archetype as LinkSystemComponentType<
            $itemname$LinkSystemComponent,
            ModelData,
            ArchetypeData,
            ModelData.Type,
            ArchetypeData.Type
          >,
          archetypeComponent
        ) { }

    #region Type

    public class Type : LinkSystemComponentType<
      $itemname$LinkSystemComponent,
      ModelData,
      ArchetypeData,
      ModelData.Type,
      ArchetypeData.Type
    > {

      #region Initialization

      public Type()
        : base($itemname$LinkSystemComponent.Id) { }

      #region Model Construction

      protected override $itemname$LinkSystemComponent InitializeModel()
        => new $itemname$LinkSystemComponent(null);

      #endregion

      #endregion
    }

    #endregion
  }
}
