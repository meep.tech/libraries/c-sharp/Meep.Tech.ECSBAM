﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Static;

namespace $rootnamespace$.Components {

  public class $itemname$ : ModelDataComponent<$itemname$> {

    public static Components.ModelDataId Id {
      get;
    } = new Components.ModelDataId("$itemname$");

    protected $itemname$(ModelDataComponentType<$itemname$> modelDataComponentArcheype)
      : base(modelDataComponentArcheype) { }

    #region Type

    public class Type : ModelDataComponentType<$itemname$> {
  
      #region Initialization

      public Type() : base($itemname$.Id) { }

      #region Model Construction

      protected override $itemname$ InitializeModel()
        => new $itemname$(this);

      #endregion

      #endregion
    }

    #endregion
  }  
}
