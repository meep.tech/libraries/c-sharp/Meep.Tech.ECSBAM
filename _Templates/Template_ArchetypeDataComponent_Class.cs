﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Static;

namespace $rootnamespace$.Components {

  public class $itemname$ : ArchetypeDataComponent<$itemname$> {

    public static Components.ArchetypeDataId Id {
      get;
    } = new Components.ArchetypeDataId("$itemname$");

    protected $itemname$(ArchetypeDataComponentType<$itemname$> archetypeDataComponentArcheype)
      : base(archetypeDataComponentArcheype) { }

    #region Type

    public class Type : ArchetypeDataComponentType<$itemname$> {
  
      #region Initialization

      public Type() : base($itemname$.Id) { }

      #region Archetype Construction

      protected override $itemname$ InitializeArchetype()
        => new $itemname$(this);

      #endregion

      #endregion
    }

    #endregion
  }  
}
