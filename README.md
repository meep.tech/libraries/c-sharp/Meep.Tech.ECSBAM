﻿# ECSBAM (Pronounced X-BAM)
## Enumerable Component and System Based Archetype Models
A Modular and Extensible system for easy access to Model producing Enumerable Singleton Archetypes.

* Id: A unique ID, just a name and unique id string usually.
* Archetype: An immutable singleton representing a type of object, they are used to create models. Each archetype has an Id to index it.
* Archetype Collection: An enumerable and extendable collection of archetypes of the same ArchetypeBaseType.
* Archetype Base Type: The parent type for a branch of archetypes, like Item, Creature, Channel, Species, etc.
* Model: A mutable struct or object of data based on an archetype, with a specific archetype specifying it's 'type'. Models can be serialized.
* Param: A type of unique ID used to specify parameters to build models from archetypes.
* Component Based Model: A Model that has a system to add components to it. All archetypes can have components added.
* Component: An extendible structure or object that can be added to and querried off of a model or an archetype. Components are based on models and can be serialized and built similarly.
* Component Archetype: An archetype used to identity the type of component.
* Data Component: Data only component
* System Component: logic only component containing staticly accessible methods, only availble on archetypes.
* Link System Component: A system component that connects a model data component to an archetype data component for initialization etc.