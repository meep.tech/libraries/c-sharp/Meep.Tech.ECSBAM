﻿namespace Meep.Tech.Data.Extensions {
  /// <summary>
  /// A Component of a model or achetype that holds only data
  /// </summary>
  public interface IDataComponent 
    : IComponent
  {}
}
