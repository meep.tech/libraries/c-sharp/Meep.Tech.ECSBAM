﻿using Meep.Tech.Data.Extensions;

namespace Meep.Tech.Data.Base {
  public partial class Archetype {

    /// <summary>
    /// Base abstract component class for "object"/"class" based components
    /// </summary>
    public abstract partial class DataComponent 
      : Component 
    {
      /// <summary>
      /// Base Ids for any type of component:
      /// </summary>
      public class Id : Component.Type.Id {
        public Id(string name, string externalIdPrefixEnding = "")
          : base(name, $"ArchetypeDatas.{externalIdPrefixEnding}") { }
      }
    }

    /// <summary>
    /// An object based data component for archetypes:
    /// </summary>
    public abstract class DataComponent
      <TComponentModelBase>
      : DataComponent,
        IArchetypeDataComponent<TComponentModelBase>
      where TComponentModelBase : IArchetypeDataComponent<TComponentModelBase>
    {

      public Archetype.DataComponent.Type<TComponentModelBase> type {
        get;
      }

      protected DataComponent(Archetype.DataComponent.Type<TComponentModelBase> archetypeDataComponentArcheype) {
        type = archetypeDataComponentArcheype;
      }
    }
  }
}
