﻿namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// A component of a model or achetype
  /// </summary>
  public interface IComponent {}

  /// <summary>
  /// A specific type of model or archetype component.
  /// </summary>
  public interface IComponent<TComponentBase, TArchetypeBase, TIdBase> 
    : IComponent,
      IModel<TComponentBase, TArchetypeBase, TIdBase>
    where TComponentBase 
      : IComponent,
        IModel<TComponentBase, TArchetypeBase, TIdBase> 
    where TIdBase 
      : Component.Type.Id 
    where TArchetypeBase : Component.Type<TComponentBase, TArchetypeBase, TIdBase> 
  {}
}
