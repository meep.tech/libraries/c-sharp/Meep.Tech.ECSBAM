﻿using Meep.Tech.Data.Base;

namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// An archetype component for static logic systems
  /// </summary>
  public interface ISystemComponent<TComponentModelBase>
    : ISystemComponent,
      IComponent<TComponentModelBase, Archetype.SystemComponent.Type<TComponentModelBase>, Archetype.SystemComponent.Id>,
      IModel<TComponentModelBase, Archetype.SystemComponent.Type<TComponentModelBase>, Archetype.SystemComponent.Id>
    where TComponentModelBase : ISystemComponent<TComponentModelBase> 
  {

    Archetype.SystemComponent.Id ISystemComponentTypeId
      => type.ArchetypeId;
  }

  /// <summary>
  /// Generic for access, use the above instead for inheritance.
  /// </summary>
  public interface ISystemComponent
    : IComponent,
      IModel {

    Archetype.SystemComponent.Id archetypeId
      => null;
  }
}
