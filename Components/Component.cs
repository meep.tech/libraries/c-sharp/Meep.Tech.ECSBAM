﻿namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// Base abstract component class for "object"/"class" based components
  /// </summary>
  public abstract partial class Component :
    IComponent
  {}
}
