﻿using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Serialization;
using System.Collections.Generic;

namespace Meep.Tech.Data.Base {
  public partial class Archetype {

    public abstract partial class SystemComponent : Component {

      /// <summary>
      /// Base Ids for any type of component:
      /// </summary>
      public class Id : Component.Type.Id {
        public Id(string name, string externalIdPrefixEnding = "")
          : base(name, $"ArchetypeSystems.{externalIdPrefixEnding}") { }
      }
    }

    public abstract class SystemComponent<TComponentModelBase>
      : SystemComponent,
        ISystemComponent<TComponentModelBase>
      where TComponentModelBase : ISystemComponent<TComponentModelBase> 
    {

      public Archetype.SystemComponent.Type<TComponentModelBase> type {
        get;
      }

      protected SystemComponent(Archetype.SystemComponent.Type<TComponentModelBase> archetypeSystemComponentArcheype) {
        type = archetypeSystemComponentArcheype;
      }
    }
  }
}
