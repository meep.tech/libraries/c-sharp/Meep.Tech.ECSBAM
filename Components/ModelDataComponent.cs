﻿using Meep.Tech.Data.Extensions;
using System.Collections.Generic;

namespace Meep.Tech.Data.Base {
  public partial class Model {

    /// <summary>
    /// A Model data component
    /// </summary>
    public abstract partial class DataComponent : Component {

      /// <summary>
      /// Base Ids for any type of component:
      /// </summary>
      public class Id : Component.Type.Id {
        public Id(string name, string externalIdPrefixEnding = "")
          : base(name, $"ModelDatas.{externalIdPrefixEnding}") { }

        /// <summary>
        /// For components where you want to link an ID but don't want to make a Type class to make it extensible.
        /// </summary>
        public Id(string name, System.Type componentType)
          : base(name, componentType) 
        {}
      }
    }

    /// <summary>
    /// A Model data component
    /// </summary>
    public abstract class DataComponent<TComponentModelBase>
    : DataComponent,
      IModelDataComponent<TComponentModelBase>
    where TComponentModelBase : IModelDataComponent<TComponentModelBase> {

      Model.DataComponent.Type<TComponentModelBase> IModelDataComponent<TComponentModelBase>.type {
        get => type;
        set => type = value;
      }

      public Model.DataComponent.Type<TComponentModelBase> type {
        get;
        private set;
      }

      protected DataComponent(Model.DataComponent.Type<TComponentModelBase> modelDataComponentArcheype) {
        type = modelDataComponentArcheype;
      }

      public abstract bool Equals(TComponentModelBase other);
    }
  }
}