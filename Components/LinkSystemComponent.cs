﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;

namespace Meep.Tech.Data.Base {

  public partial class Archetype {

    /// <summary>
    /// Static single accessors for the generic LinkSystemComponent:
    /// </summary>
    public static partial class LinkSystemComponent {

      /// <summary>
      /// Base Ids for any type of component:
      /// </summary>
      public class Id : Archetype.SystemComponent.Id {
        public Id(string name, string externalIdPrefixEnding = "")
          : base(name, $"Links.{externalIdPrefixEnding}") { }
      }
    }

    /// <summary>
    /// Base class for an object based link system component
    /// </summary>
    public abstract class LinkSystemComponent<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >
      : Archetype.SystemComponent<TComponentModelBase>,
        ILinkSystemComponent<
          TComponentModelBase,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >
      where TComponentModelBase
        : ILinkSystemComponent<
          TComponentModelBase,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
        ISystemComponent<TComponentModelBase>
      where TLinkedModelComponentArchetype : IModelDataComponentType
      where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
      where TLinkedModelComponent : IModelDataComponent
      where TLinkedArchetypeComponent : IArchetypeDataComponent {

      /// <summary>
      /// The pre-constructed arechetype data component associated with this component. This should be initialized and passed to the ctor
      /// </summary>
      public IArchetypeDataComponent constructedArchetypeDataComponent {
        get;
        protected set;
      }

      /// <summary>
      /// The pre-constructed arechetype data component associated with this component.
      /// This should be initialized and passed to the ctor.
      /// Same as constructedArchetypeDataComponent but more type specific.
      /// </summary>
      public TLinkedArchetypeComponent currentArchetypeDataComponent {
        get;
        protected set;
      }

      #region IModel/IComponent

      Archetype IModel.type
        => type;

      /// <summary>
      /// Correct type for link system components
      /// </summary>   
      public new Archetype.LinkSystemComponent.Type<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      > type {
        get;
      }
      ILinkSystemComponentType ILinkSystemComponent.type
        => type;
      Archetype.LinkSystemComponent.Type<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      > IModel<
        TComponentModelBase,
        Archetype.LinkSystemComponent.Type<
          TComponentModelBase,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
        LinkSystemComponent.Id
      >.type
        => type;

      IModel IModel.clone(bool newId = true, string uniqueIdToken = null)
        => ((IModel<
          TComponentModelBase,
          Archetype.LinkSystemComponent.Type<
            TComponentModelBase,
            TLinkedModelComponent,
            TLinkedArchetypeComponent,
            TLinkedModelComponentArchetype,
            TLinkedArchetypeComponentArchetype
          >,
          LinkSystemComponent.Id
        >)this).clone();

      #endregion

      #region Initialization

      /// <summary>
      /// for making child classes
      /// </summary>
      protected LinkSystemComponent(
        Archetype.LinkSystemComponent.Type<
          TComponentModelBase,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        > type,
        TLinkedArchetypeComponent archetypeComponent
      ) : base(type as Archetype.SystemComponent.Type<TComponentModelBase>) {
        this.type = type;
        constructedArchetypeDataComponent
        = currentArchetypeDataComponent
          = (TLinkedArchetypeComponent)
              (archetypeComponent
                ?? ((ILinkSystemComponent)this).type.GetDefaultArchetypeComponent());
      }

      #endregion

      #region System Logic Functions

      ///// Prefix:
      /// Simple: This only effects/takes the components as parameters.
      /// Advanced: This takes both components, the parent model, and archetype of the parent model as params

      ///// Suffix:
      /// Function means it takes custom Input params
      /// Logic means it only effects the model and components, taking no input params

      /// <summary>
      /// Used to build system logic functions
      /// </summary>
      public delegate TOut SimpleLogic<TOut>(
        TLinkedModelComponent modelData,
        TLinkedArchetypeComponent archetypeData
      );

      /// <summary>
      /// Used to build basic system logic functions
      /// </summary>
      public delegate void SimpleLogic(
        TLinkedModelComponent modelData,
        TLinkedArchetypeComponent archetypeData
      );

      /// <summary>
      /// Used to build advanced logic functions
      /// </summary>
      public delegate TOut AdvancedLogic<TOut>(
        TLinkedModelComponent modelComponentData,
        TLinkedArchetypeComponent archetypeComponentData,
        IModel model,
        Archetype archetype
      );

      /// <summary>
      /// Used to build advanced system logic functions
      /// </summary>
      public delegate void AdvancedLogic(
        TLinkedModelComponent modelComponentData,
        TLinkedArchetypeComponent archetypeComponentData,
        IModel model,
        Archetype archetype
      );

      /// <summary>
      /// Used to build system logic functions
      /// </summary>
      public delegate TOut SimpleFunction<TIn, TOut>(
        TIn input,
        TLinkedModelComponent modelData,
        TLinkedArchetypeComponent archetypeData
      );

      /// <summary>
      /// Used to build system logic functions
      /// </summary>
      public delegate void SimpleFunction<TIn>(
        TIn input,
        TLinkedModelComponent modelData,
        TLinkedArchetypeComponent archetypeData
      );

      /// <summary>
      /// Used to build advanced system logic functions
      /// </summary>
      public delegate TOut AdvancedFunction<TIn, TOut>(
        TIn input,
        TLinkedModelComponent modelComponentData,
        TLinkedArchetypeComponent archetypeComponentData,
        IModel model,
        Archetype archetype
      );

      /// <summary>
      /// Used to build advanced logic functions
      /// </summary>
      public delegate void AdvancedFunction<TIn>(
        TIn input,
        TLinkedModelComponent modelComponentData,
        TLinkedArchetypeComponent archetypeComponentData,
        IModel model,
        Archetype archetype
      );

      #endregion
    }

  }
}

public static class LinkSystemExtensions {

  /// <summary>
  /// Execute the system For this model
  /// </summary>
  public static void ExecuteFor<
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.SimpleLogic simpleLogic,
    TModel model
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    simpleLogic(
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>()
    );
  }

  /// <summary>
  /// Execute the system For this model
  /// </summary>
  public static TReturn ExecuteFor<
    TReturn,
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.SimpleLogic<TReturn> simpleLogic,
    TModel model
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    return simpleLogic(
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>()
    );
  }

  /// <summary>
  /// Execute the system ON this model, possibly editing the model
  /// </summary>
  public static void ExecuteOn<
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.AdvancedLogic simpleLogic,
    TModel model
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    simpleLogic(
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>(),
      model,
      model.type
    );
  }

  /// <summary>
  /// Execute the system ON this model, possibly editing the model
  /// </summary>
  public static TReturn ExecuteOn<
    TReturn,
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.AdvancedLogic<TReturn> simpleLogic,
    TModel model
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    return simpleLogic(
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>(),
      model,
      model.type
    );
  }

  /// <summary>
  /// Execute the system For this model
  /// </summary>
  public static void ExecuteFor<
    TInput,
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.SimpleFunction<TInput> simpleLogic,
    TModel model,
    TInput input = default
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    simpleLogic(
      input,
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>()
    );
  }

  /// <summary>
  /// Execute the system For this model
  /// </summary>
  public static TReturn ExecuteFor<
    TInput,
    TReturn,
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.SimpleFunction<TInput, TReturn> simpleLogic,
    TModel model,
    TInput input = default
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    return simpleLogic(
      input,
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>()
    );
  }

  /// <summary>
  /// Execute the system ON this model, possibly editing the model
  /// </summary>
  public static void ExecuteOn<
    TInput,
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.AdvancedFunction<TInput> simpleLogic,
    TModel model,
    TInput input = default
  ) where TComponentModelBase
      : ILinkSystemComponent<
        TComponentModelBase,
        TLinkedModelComponent,
        TLinkedArchetypeComponent,
        TLinkedModelComponentArchetype,
        TLinkedArchetypeComponentArchetype
      >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel : IModel, IModelDataComponentStorage {
    simpleLogic(
      input,
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>(),
      model,
      model.type
    );
  }

  /// <summary>
  /// Execute the system ON this model, possibly editing the model
  /// </summary>
  public static TReturn ExecuteOn<
    TInput,
    TReturn,
    TModel,
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >(
    this Archetype.LinkSystemComponent<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >.AdvancedFunction<TInput, TReturn> simpleLogic,
    TModel model,
    TInput input = default
  ) where TComponentModelBase
      : ILinkSystemComponent<
          TComponentModelBase,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
        ISystemComponent<TComponentModelBase>
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent
    where TModel 
      : IModel,
        IModelDataComponentStorage
  {
    return simpleLogic(
      input,
      model.getComponent<TLinkedModelComponent>(),
      model.type.GetData<TLinkedArchetypeComponent>(),
      model,
      model.type
    );
  }
}