﻿using Meep.Tech.Data.Attributes.Models;
using System;

namespace Meep.Tech.Data.Attributes.Components {

  /// <summary>
  /// Used to mark fields we want to serialize to json on fully serializeable components.
  /// </summary>
  public class ComponentDataFieldAttribute : ModelDataFieldAttribute, IComponentModelDataAttribute {
    public ComponentDataFieldAttribute(
      string FieldName = null, bool SkipDuringDeserialization = false, Type CustomSerializeToType = null) 
        : base(FieldName, false, SkipDuringDeserialization, CustomSerializeToType) {}
  }
}
