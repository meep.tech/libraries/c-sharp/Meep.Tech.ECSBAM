﻿namespace Meep.Tech.Data.Attributes.Components {

  /// <summary>
  /// Used to make the owner field (required) on a fully serializeable component.
  /// </summary>
  public class ComponentOwnerFieldAttribute : Attributes.Models.ModelOwnerFieldAttribute, IComponentModelDataAttribute {
    public ComponentOwnerFieldAttribute(bool SkipDuringDeserialization = false) 
      : base(SkipDuringDeserialization) {}
  }
}
