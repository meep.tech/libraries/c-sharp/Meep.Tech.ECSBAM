﻿using Meep.Tech.Data.Attributes.Models;
using System;

namespace Meep.Tech.Data.Attributes.Components {

  /// <summary>
  /// Used to mark generic fields on components that can be used in sql.
  /// </summary>
  public abstract class ComponentSqlDataFieldAttribute : ModelDataFieldAttribute, IComponentModelDataAttribute {

    protected abstract string FieldNamePrefix {
      get;
    }

    public readonly static string FieldNameProfix
      = "_data_";

    protected abstract int _index {
      get;
    }

    public override string Name {
      get => _name ??= FieldNamePrefix + FieldNameProfix + _index.ToString();
    } string _name;

    protected ComponentSqlDataFieldAttribute(bool skipDuringDeserialization) 
      : base(null, true, skipDuringDeserialization, null) {}

    protected ComponentSqlDataFieldAttribute() 
      : base(null, true, false, null) {}
  }

  /// <summary>
  /// Used to mark generic fields on components that can be used in sql.
  /// </summary>
  public abstract class ComponentStringSqlDataFieldAttribute : ComponentSqlDataFieldAttribute {

    public override Type CustomSerializeToType
      => SerializeToType;

    public readonly static Type SerializeToType
      = typeof(string);

    protected override string FieldNamePrefix
      => "string";
  }

  /// <summary>
  /// Used to mark generic fields on components that can be used in sql.
  /// </summary>
  public abstract class ComponentNumberSqlDataFieldAttribute : ComponentSqlDataFieldAttribute {

    public override Type CustomSerializeToType
      => SerializeToType;

    public readonly static Type SerializeToType
      = typeof(double);

    protected override string FieldNamePrefix
      => "number";
  }

  public class ComponentNumberSqlDataFieldOneAttribute : ComponentNumberSqlDataFieldAttribute {
    protected override int _index 
      => 1;
  }

  public class ComponentNumberSqlDataFieldTwoAttribute : ComponentNumberSqlDataFieldAttribute {
    protected override int _index 
      => 2;
  }

  public class ComponentNumberSqlDataFieldThreeAttribute : ComponentNumberSqlDataFieldAttribute {
    protected override int _index 
      => 3;
  }

  public class ComponentStringSqlDataFieldOneAttribute : ComponentStringSqlDataFieldAttribute {
    protected override int _index 
      => 1;
  }

  public class ComponentStringSqlDataFieldTwoAttribute : ComponentStringSqlDataFieldAttribute {
    protected override int _index 
      => 2;
  }

  public class ComponentStringSqlDataFieldThreeAttribute : ComponentStringSqlDataFieldAttribute {
    protected override int _index 
      => 3;
  }
}