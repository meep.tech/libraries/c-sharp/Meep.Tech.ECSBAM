﻿using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Attributes.Models;
using Meep.Tech.Data.Serialization;
using System;

namespace Meep.Tech.Data.Attributes.Components {

  /// <summary>
  /// Used to mark fully serializeable components.
  /// </summary>
  public class ComponentDataModelAttribute : DataModelAttribute {

    static string StringOneFieldName {
      get => _stringOneFieldName ??= (new ComponentStringSqlDataFieldOneAttribute()).Name;
    } static string _stringOneFieldName;

    static string StringTwoFieldName {
      get => _stringTwoFieldName ??= (new ComponentStringSqlDataFieldTwoAttribute()).Name;
    } static string _stringTwoFieldName;

    static string StringThreeFieldName {
      get => _stringThreeFieldName ??= (new ComponentStringSqlDataFieldThreeAttribute()).Name;
    } static string _stringThreeFieldName;

    static string NumberOneFieldName {
      get => _NumberOneFieldName ??= (new ComponentNumberSqlDataFieldOneAttribute()).Name;
    } static string _NumberOneFieldName;

    static string NumberTwoFieldName {
      get => _NumberTwoFieldName ??= (new ComponentNumberSqlDataFieldTwoAttribute()).Name;
    } static string _NumberTwoFieldName;

    static string NumberThreeFieldName {
      get => _NumberThreeFieldName ??= (new ComponentNumberSqlDataFieldThreeAttribute()).Name;
    } static string _NumberThreeFieldName;

    public ComponentDataModelAttribute() : base(
        Serializer.ComponentKeyNameAndTablePlaceholder,
        typeof(IModelDataComponentType),
        new string[] {
          StringOneFieldName,
          StringTwoFieldName,
          StringThreeFieldName,
          NumberOneFieldName,
          NumberTwoFieldName,
          NumberThreeFieldName
        },
        new Type[] {
          ComponentStringSqlDataFieldOneAttribute.SerializeToType,
          ComponentStringSqlDataFieldTwoAttribute.SerializeToType,
          ComponentStringSqlDataFieldThreeAttribute.SerializeToType,
          ComponentNumberSqlDataFieldOneAttribute.SerializeToType,
          ComponentNumberSqlDataFieldTwoAttribute.SerializeToType,
          ComponentNumberSqlDataFieldThreeAttribute.SerializeToType
        }
      ) {}
  }
}
