﻿using Meep.Tech.Data.Base;

namespace Meep.Tech.Data.Extensions {

  public interface ILinkSystemComponent<
    TComponentModelBase,
    TLinkedModelComponent,
    TLinkedArchetypeComponent,
    TLinkedModelComponentArchetype,
    TLinkedArchetypeComponentArchetype
  >
    : ISystemComponent,
      ILinkSystemComponent,
      IComponent<TComponentModelBase,
        Archetype.LinkSystemComponent.Type<
          TComponentModelBase,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
        Archetype.LinkSystemComponent.Id
      >,
      IModel<
        TComponentModelBase,
        Archetype.LinkSystemComponent.Type<
          TComponentModelBase,
          TLinkedModelComponent,
          TLinkedArchetypeComponent,
          TLinkedModelComponentArchetype,
          TLinkedArchetypeComponentArchetype
        >,
        Archetype.LinkSystemComponent.Id
      >
    where TLinkedModelComponentArchetype : IModelDataComponentType
    where TLinkedArchetypeComponentArchetype : IArchetypeDataComponentType
    where TComponentModelBase : ILinkSystemComponent<
      TComponentModelBase,
      TLinkedModelComponent,
      TLinkedArchetypeComponent,
      TLinkedModelComponentArchetype,
      TLinkedArchetypeComponentArchetype
    >
    where TLinkedModelComponent : IModelDataComponent
    where TLinkedArchetypeComponent : IArchetypeDataComponent 
  {

    /// <summary>
    /// The pre-constructed arechetype data component associated with this component. Usually this should be initialized in the ctor
    /// </summary>
    TLinkedArchetypeComponent currentArchetypeDataComponent {
      get;
    }

    /// <summary>
    /// The pre-constructed arechetype data component associated with this component. Usually this should be initialized in the ctor
    /// </summary>
    IArchetypeDataComponent ILinkSystemComponent.constructedArchetypeDataComponent
      => currentArchetypeDataComponent;
  }
  
  public interface ILinkSystemComponent : ISystemComponent {

    /// <summary>
    /// Type accessor
    /// </summary>
    new ILinkSystemComponentType type {
      get;
    }

    /// <summary>
    /// The pre-constructed arechetype data component associated with this component. Usually this should be initialized in the ctor
    /// </summary>
    IArchetypeDataComponent constructedArchetypeDataComponent
      => null;
  }
}
