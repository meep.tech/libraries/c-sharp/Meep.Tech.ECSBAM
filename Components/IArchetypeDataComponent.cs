﻿using Meep.Tech.Data.Base;

namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// An archetype component for just data
  /// </summary>
  public interface IArchetypeDataComponent<TComponentModelBase>
    : IArchetypeDataComponent,
      IComponent<TComponentModelBase, Archetype.DataComponent.Type<TComponentModelBase>, Archetype.DataComponent.Id>,
      IModel<TComponentModelBase, Archetype.DataComponent.Type<TComponentModelBase>, Archetype.DataComponent.Id>
    where TComponentModelBase : IArchetypeDataComponent<TComponentModelBase>
  {

    Archetype.DataComponent.Id IArchetypeDataComponent.archetypeId
      => type.ArchetypeId;
  }

  /// <summary>
  /// Generic for access, use the above instead for inheritance.
  /// </summary>
  public interface IArchetypeDataComponent
    : IDataComponent,
      IComponent,
      IModel 
  {
    /// <summary>
    /// Helper to get the archetype ID of this component
    /// </summary>
    public virtual Archetype.DataComponent.Id archetypeId
      => null;
  }
}
