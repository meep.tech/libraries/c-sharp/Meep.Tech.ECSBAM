﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;

namespace Meep.Tech.Data.Static {

  /// <summary>
  /// Basic class for Component constants access
  /// </summary>
  public static partial class Components {

    /// <summary>
    /// All Model Data Components
    /// </summary>
    public static Archetype.Collection<IModelDataComponent, IModelDataComponentType, Model.DataComponent.Id> ModelDatas { get; }
      = new Archetype.Collection<IModelDataComponent, IModelDataComponentType, Model.DataComponent.Id>();

    /// <summary>
    /// All Archetype Data Components
    /// </summary>
    public static Archetype.Collection<IArchetypeDataComponent, IArchetypeDataComponentType, Archetype.DataComponent.Id> ArchetypeDataComponents { get; }
      = new Archetype.Collection<IArchetypeDataComponent, IArchetypeDataComponentType, Archetype.DataComponent.Id>();

    /// <summary>
    /// All Archetype System Components
    /// </summary>
    public static Archetype.Collection<ISystemComponent, IArchetypeSystemComponentType, Archetype.SystemComponent.Id> ArchetypeSystemComponents { get; }
      = new Archetype.Collection<ISystemComponent, IArchetypeSystemComponentType, Archetype.SystemComponent.Id>();
  }
}