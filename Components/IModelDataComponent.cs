﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Reflection;

namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// A Model component for just data
  /// </summary>
  public interface IModelDataComponent<TComponentModelBase>
    : IModelDataComponent,
      IComponent<TComponentModelBase, Model.DataComponent.Type<TComponentModelBase>, Model.DataComponent.Id>,
      IModel<TComponentModelBase, Model.DataComponent.Type<TComponentModelBase>, Model.DataComponent.Id>
    where TComponentModelBase : IModelDataComponent<TComponentModelBase> {

    new Model.DataComponent.Type<TComponentModelBase> type {
      get;
      protected internal set;
    }

    Model.DataComponent.Type<TComponentModelBase> IModel<TComponentModelBase, Model.DataComponent.Type<TComponentModelBase>, Model.DataComponent.Id>.type
      => type;

    /// <summary>
    /// Helper for archetype id
    /// </summary>
    Model.DataComponent.Id IModelDataComponent.archetypeId
      => type?.ArchetypeId 
        ?? (Archetype.Identity._defaultArchetypelessTypeIdsByModelBaseTypeForDefaultCtorTypes[GetType().GetBaseModelType()] as Model.DataComponent.Id);

    /// <summary>
    /// Overrideable conversion of this type from a jobject
    /// </summary>
    IModelDataComponent IModelDataComponent.fromJson(string serializedDataString) {
      IModelDataComponent deserialized = JsonConvert.DeserializeObject(serializedDataString, GetType(), Serializer.Settings.ComponentJsonSerializerSettings) as IModelDataComponent;
      ((IModelDataComponent<TComponentModelBase>)deserialized).setType(type);

      return deserialized;
    }

    /// <summary>
    /// Need to tell how just this type equals another of this type. basic things are already covered.
    /// </summary>
    bool Equals(TComponentModelBase other);

    /// <summary>
    /// Override the equality base components use
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    bool IEquatable<IModelDataComponent>.Equals(IModelDataComponent other)
      => Equals(
        (TComponentModelBase)other
      );
  }

  /// <summary>
  /// Generic for access, use the above instead for inheritance.
  /// </summary>
  public interface IModelDataComponent
    : IDataComponent,
      ISerializeable,
      IComponent,
      IModel,
      IEquatable<IModelDataComponent> {

    /// <summary>
    /// Helper for archetype id
    /// </summary>
    public virtual Model.DataComponent.Id archetypeId
      => null;

    /// <summary>
    /// Convert this model's data to a serialized collection.
    /// </summary>
    Serializer.SerializedData ISerializeable.serialize(bool andAllChildrenRecursively, bool fullySerializeLinkedModels)
      => IModelDataComponentMethods.serialize(this);

    /// <summary>
    /// Convert this model's data to a serialized collection.
    /// </summary>
    /*Serializer.SerializedData ISerializeable.serialize()
      => IModelDataComponentMethods.serialize(this);*/

    /// <summary>
    /// Deserialize this model from the given data.
    /// This does not set itself!
    /// </summary>
    ISerializeable ISerializeable.deserialize(Serializer.SerializedData serializedData, int? depthLimit)
       => IModelDataComponentMethods.deserialize(this, serializedData.dataString);

    /// <summary>
    /// Get the table name used by the model
    /// </summary>
    /// <returns></returns>
    string ISerializeable.GetTableName()
      => IModelDataComponentMethods.GetTableName(this);

    /// <summary>
    /// Overrideable conversion to a jobject
    /// </summary>
    string ISerializeable.toJson()
      => JsonConvert.SerializeObject(this, Serializer.Settings.ComponentJsonSerializerSettings);

    /// <summary>
    /// Get the table name used by the model type
    /// </summary>
    JObject ISerializeable.toJObject()
      => JObject.FromObject(this, Serializer.Settings.ComponentJsonSerializer);

    /// <summary>
    /// Get this from a data string:
    /// </summary>
    ISerializeable ISerializeable.fromJson(string serializedDataString)
      => fromJson(serializedDataString);

    /// <summary>
    /// Get this from a data string:
    /// </summary>
    new IModelDataComponent fromJson(string serializedDataString);
  }

  /// <summary>
  /// Prevent serialization of the type:
  /// </summary>
  public class ShouldSerializeTypeInComponentsContractResolver : DefaultContractResolver {

    /// <summary>
    /// Instance
    /// </summary>
    public static readonly ShouldSerializeTypeInComponentsContractResolver Instance
      = new ShouldSerializeTypeInComponentsContractResolver();

    protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization) {
      JsonProperty property = base.CreateProperty(member, memberSerialization);

      if ((property.PropertyName == "type" || property.PropertyName == "<type>k__BackingField")) {
        property.ShouldSerialize = instance => false;
      }

      return property;
    }
  }
}

public static class IModelDataComponentMethods {

  internal static void setType<TComponentModelBase>(this IModelDataComponent<TComponentModelBase> component, IModelDataComponentType type)
    where TComponentModelBase : IModelDataComponent<TComponentModelBase> {
    component.type = (Model.DataComponent.Type<TComponentModelBase>)type;
  }

  /// <summary>
  /// Convert this model's data to a serialized collection.
  /// </summary>
  public static Serializer.SerializedData serialize(this IModelDataComponent component, bool andAllChildrenRecursively = false, bool fullySerializeLinkedModels = false)
    => Serializer.SerializedData.MakeForComponent(component.toJson());

  /// <summary>
  /// Deserialize this model from the given data.
  /// This does not set itself!
  /// </summary>
  public static ISerializeable deserialize(
    this IModelDataComponent component,
    string serializedDataString
  ) {
    return component.fromJson(serializedDataString);
  }

  /// <summary>
  /// Deserialize this model from the given data.
  /// This does not set itself!
  /// </summary>
  public static ISerializeable deserialize(
    this IModelDataComponent component,
    Serializer.SerializedData serializedData
  ) {
    if (serializedData.tableName != Serializer.ComponentKeyNameAndTablePlaceholder) {
      throw new System.Exception("Cannot deserialize a component from a non component source");
    }
    return component.fromJson(
      serializedData.dataString
    );
  }

  /// <summary>
  /// Get the table name used by the model
  /// </summary>
  /// <returns></returns>
  public static string GetTableName(this IModelDataComponent component)
    => Serializer.ComponentKeyNameAndTablePlaceholder;
}
