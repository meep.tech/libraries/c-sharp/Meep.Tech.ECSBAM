﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Serialization;
using Newtonsoft.Json.Linq;

namespace Meep.Tech.Data.Extensions {

  /// <summary>
  /// A component that doesn't use the quick json serialization, and has it's own table relations.
  /// </summary>
  public interface IFullySerializeableComponent : IModelDataComponent, IOwned {

    /// <summary>
    /// Owner is set when the component is added.
    /// </summary>
    bool IOwned.OwnerIsRequiredOnCreation 
      => false;

    string ISerializeable.GetTableName()
      => IFullySerializeableMethods.GetTableName(this);

    string ISerializeable.toJson()
      => IFullySerializeableMethods.toJson(this);

    JObject ISerializeable.toJObject()
      => IFullySerializeableMethods.toJObject(this);

    ISerializeable ISerializeable.fromJson(string jsonString)
      => ISerializeableMethods.fromJson(this, jsonString);

    Serializer.SerializedData ISerializeable.serialize(bool andAllChildrenRecursively, bool fullySerializeLinkedModels)
      => IFullySerializeableMethods.serialize(this, andAllChildrenRecursively, fullySerializeLinkedModels);

    /*Serializer.SerializedData ISerializeable.serialize()
      => IFullySerializeableMethods.serialize(this, true);*/

    ISerializeable ISerializeable.deserialize(
      Serializer.SerializedData serializedData,
      int? depthLimit
    ) => IFullySerializeableMethods.deserialize(this, serializedData, depthLimit);
  }
}


public static class IFullySerializeableMethods {

  /// <summary>
  /// Convert this model's data to a serialized state.
  /// </summary>
  public static Serializer.SerializedData serialize(this IFullySerializeableComponent model, bool andAllChildrenRecursively = true, bool fullySerializeLinkedModels = true)
    => ISerializeableMethods.serialize(model, andAllChildrenRecursively, fullySerializeLinkedModels);

  /// <summary>
  /// Convert this model's data to a serialized state.
  /// </summary>
  public static Serializer.SerializedData serialize(this IFullySerializeableComponent model)
    => ISerializeableMethods.serialize(model);

  /// <summary>
  /// Convert this model's data to a serialized state.
  /// </summary>
  public static string toJson(this IFullySerializeableComponent model)
    => ISerializeableMethods.toJson(model);

  /// <summary>
  /// Convert this model's data to a serialized state.
  /// </summary>
  public static JObject toJObject(this IFullySerializeableComponent model)
    => ISerializeableMethods.toJObject(model);

  /// <summary>
  /// Deserialize this model from the given data.
  /// </summary>
  public static ISerializeable deserialize(
    this IFullySerializeableComponent model,
    Serializer.SerializedData serializedData,
    int? depthLimit = null
  ) => ISerializeableMethods.deserialize(model, serializedData, depthLimit);

  /// <summary>
  /// Get the table name used by the model type
  /// </summary>
  public static string GetTableName(this IFullySerializeableComponent serializeable)
    => ISerializeableMethods.GetTableName(serializeable);
}
